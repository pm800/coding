#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

int n, arr[205];
int ans[205];

void f(){
    for(int i=0, j=n-1; i<j; i++, j--)
		swap(arr[i], arr[j]);
	stack<int> stk;
	fill(ans, ans+n, 1);
	stk.push(0);
	for(int i=1; i<n; i++){
		if(arr[stk.top()] >= arr[i] )
			stk.push(i);
		else{
			while(!stk.empty() && arr[stk.top()] < arr[i]){
				int j = stk.top();
				stk.pop();
				ans[j] = i-j;
			}
			stk.push(i);
		}
	}
	while(!stk.empty()){
		int j = stk.top();
		stk.pop();
		ans[j] = n - j;
	}

	for(int i=n-1; i>=0; i--)
		cout<<ans[i]<<" ";
	cout<<endl;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		f();
	}
	return 0;
}


