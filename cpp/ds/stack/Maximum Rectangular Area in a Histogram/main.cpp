#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

int n, arr[105];

int f(){
    stack<int> stk;
    int mx = INT_MIN;
    int i;
    for(i=0; i<n; i++){
		if(stk.empty()){
			stk.push(i);
		}
		else if(arr[stk.top()] <= arr[i]){
			stk.push(i);
		}
		else{
            while(!stk.empty() && arr[stk.top()] > arr[i]){
				int val = arr[stk.top()];
				stk.pop();
				if(stk.empty())
					val *= i;
				else
					val *= (i-stk.top()-1);
				if(val > mx)
					mx = val;
            }
            stk.push(i);
		}
    }
    while(!stk.empty()){
		int val = arr[stk.top()];
		stk.pop();
		if(stk.empty())
			val *= i;
		else
			val *= (i-stk.top()-1);
		if(val > mx)
			mx = val;
    }
    return mx;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		cout<<f()<<endl;
	}
	return 0;
}


