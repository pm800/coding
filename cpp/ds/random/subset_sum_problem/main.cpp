#include <iostream>
#include <bits/stdc++.h>
using namespace std;


int bss(int arr[], int n, int f){
    if(n==1){
        if(arr[0] <= f) return 0;
        else return -1;
    }
    else{
        int l = 0, r = n-1,m;
        while(r != l+1){
            m = l + (r-l)/2;
            if(arr[m] == f) return m;
            else if(arr[m] > f) r = m;
            else l = m;
        }
        if(r <= f) return r;
        else if(l <= f) return l;
        else return -1;
    }
    return -1;
}

void show(bool** mat, int r, int c){
    for(int i=0; i<r; i++){
        for(int j= 0; j<c; j++){
            cout<< mat[i][j]<<" ";
        }cout<<endl;
    }cout<<endl;
}

void f(){
        int n;
        cin >> n;

//        int* arr = new int[n];
        int arr[n] = {0};

        for(int i=0; i<n; i++) cin >> arr[i];
        sort(arr, arr+n);
        int sum = 0;
        for(int i=0; i<n; i++) sum += arr[i];
        if(sum%2 != 0) {cout<<"NO"<<endl; return;};
        int p = bss(arr, n, sum/2); //position
//        cout<<p<<endl;
        if(p == -1) {cout<<"NO2"<<endl; return;};
        p += 1; //elements
        sum /= 2;

        bool mat[p][sum+1] = {false};
//        bool** mat;
//        mat = new bool* [p];
//        for(int i=0; i<p; i++){
//            mat[i] = new bool [sum+1];
//        }

        for(int i=0; i<sum+1; i++) mat[0][i] = false;
        mat[0][arr[0]] = true;
        for(int i=0; i<p; i++) mat[i][0] = true;

        for(int r=1; r<p; r++){
            int a = arr[r];
            mat[r][a] = true;
            for(int c=a+1; c<sum+1; c++){
                if(mat[r-1][c]) {mat[r][c] = true; continue;};
                if(mat[r-1][c-a]) {mat[r][c] = true; continue; };
            }
        }
        if(mat[p-1][sum]) cout<<"YES"<<endl;
        else cout<<"NO"<<endl;


//        delete [] arr;
//        for(int i=0; i<p; i++){
//            delete [] mat[i];
//        }
//        delete [] mat;
}
int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        f();
    }
    return 0;
}
