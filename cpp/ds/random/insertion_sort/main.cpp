#include <iostream>

using namespace std;
void f(int arr[], int n){
    for(int i=n-2; i>=0; i--){
        int a = i+1, b = n-1;
        int temp = arr[i];
        int j;
        for(j=a; j<=b; j++){
            if(arr[j] >= temp) break;
            arr[j-1] = arr[j];
        }
        arr[j-1] = temp;
    }
}

void f2(int arr[], int n){
    for(int i=1; i<n; i++){
        int a=0, b=i-1;
        int temp = arr[i];
        int j;
        for(j=b; j>=a; j--){
            if(arr[j]>temp) arr[j+1] = arr[j];
            else break;
        };
        arr[j+1] = temp;
    }
}

void show(int arr[], int n){
    for(int i=0; i<n; i++) cout<< arr[i]<< " ";
    cout <<endl;
}

void shellSort(int arr[], int n){
    int k = n;
    while(k>0){
        k /= 2;
        for(int p=0; p<k; p++){
            for(int i=k+p; i<n; i+=k){
                int a = p, b = i - k;
                int temp = arr[i];
                int j;
                for(j=b; j>=a; j-=k){
                    if(arr[j]>temp) arr[j+k] = arr[j];
                    else break;
                };
                arr[j+k] = temp;
            }
        }
    }
}
int main()
{
    int tcs, n;
    cin >> tcs;
    while(tcs--){
        cin >> n;
        int arr[n]={};
        for(int i=0; i<n; i++) cin >> arr[i];
        shellSort(arr, n);
        show(arr,n);
    }
    return 0;
}
