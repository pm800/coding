#include <iostream>

using namespace std;

void shellSort(int arr[], int n){
    int k = n;
    while(k>0){
        k /= 2;
        for(int p=0; p<k; p++){
            for(int i=k+p; i<n; i+=k){
                int a = p, b = i - k;
                int temp = arr[i];
                int j;
                for(j=b; j>=a; j-=k){
                    if(arr[j]>temp) arr[j+k] = arr[j];
                    else break;
                };
                arr[j+k] = temp;
            }
        }
    }
}


int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
