#include <iostream>
#include <numeric>
#include <climits>
#define INF INT_MAX

using namespace std;

void util(int sum1, int sum2, int n1, int n2, int i, int& bestdiff, int* arr, int n, bool* set1, bool* sol_set){
    if(n1 == n/2 || n2 == n/2){
        int z = sum1 - sum2;
        if(z<0) z = -z;
        if(z<bestdiff){
            bestdiff = z;
            for(int i=0; i<n; i++)
                sol_set[i] = set1[i];
        }
    }
    if(i>=n) return;
    if( (n-i) + n1 < n/2 ) return;
    util(sum1, sum2, n1, n2, i+1, bestdiff, arr, n, set1, sol_set);
    set1[i] = true;
    util(sum1+arr[i], sum2-arr[i], n1+1, n2-1, i+1, bestdiff, arr, n, set1, sol_set);
    set1[i] = false;
}


void tugOfWar(int* arr, int n){
    bool set1[n] = {};
    bool sol_set[n] = {};
    int sum1 = 0, sum2 = accumulate(arr, arr+n, 0), n1 = 0, n2 = n, bestdiff = INF, i = 0;
    util(sum1, sum2, n1, n2, i, bestdiff, arr, n, set1, sol_set);
    cout<<bestdiff<<endl;
    for(int i=0; i<n; i++){
        if(sol_set[i]==true)
            cout<<arr[i]<<" ";
    }cout<<endl;
    for(int i=0; i<n; i++){
        if(sol_set[i]==false)
            cout<<arr[i]<<" ";
    }cout<<endl;
}
int main()
{
    int arr[] = {1,2,3,4,1,2,7};
    int n = sizeof(arr)/sizeof(arr[0]);
    tugOfWar(arr, n);
    return 0;
}
