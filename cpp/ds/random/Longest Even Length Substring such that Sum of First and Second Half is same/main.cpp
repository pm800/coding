#include <iostream>

using namespace std;

int f(string arr, int n){
    int mlen = 0;
    for(int i=0, j=1; j<n; i++, j++){
        int clen = 0;
        int lsum = 0, rsum = 0;
        for(int p=i, q=j; p>=0&&q<n; p--, q++){
            lsum += arr[p];
            rsum += arr[q];
            if(lsum == rsum){
                clen = q-p+1;
            }
        }
        if(mlen < clen){
            mlen = clen;
        }
    }
    return mlen;
}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n;
        string s;
        cin >> s;
        n = s.length();
        int ans = f(s, n);
        cout << ans << endl;
    }
    return 0;
}
