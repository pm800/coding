    #include <iostream>
    #include <algorithm>

    using namespace std;


    int g(int arr[], int l , int r, int x, int kind){
        if(l>=r) cout <<"error 18 "<<endl;
        switch(kind){
        case 0: // for exact match
            if(arr[l] == x) return l;
            else if(arr[r] == x) return r;
            else return -1;
            break;
        case 1: // for just greater than x
            if(arr[l]>=x) return l;
            else if(arr[r]>=x) return r;
            else return -1;
            break;
        case -1: // for just smaller than x
            if(arr[r]<=x) return r;
            else if(arr[l] <= x) return l;
            else return -1;
            break;
        default:
            cout <<"error";
        }
    }

    int f(int arr[], int n, int l, int r, int x, int kind){
        if(l==r) return l;
        if(l>r) return -1;
        int m = l+(r-l)/2;
        while(m>l){
            if(arr[m] == x) return m;
            if(arr[m] > x) r = m;
            if(arr[m] < x) l = m;
            m = l+(r-l)/2;
        }
        int pos = g(arr, l, r, x, kind);
        return pos;
    }

    int main()
    {
        int arr[] = {1,2,3,5,8,14, 22, 44, 55};
        int n = sizeof(arr)/sizeof(arr[0]);
        sort(arr, arr+n);
        int tcs;
        cin >> tcs;
        while(tcs--){
            int l = 0, r = n-1, x = 88, kind = -1;
            cin >> x;
            int pos = f(arr, n, l, r, x, kind);
            // kind =0: exact match, kind=1: just grater than x, kind=-1: just smaller than x;
            cout << pos << " -- ";
            if(pos >= 0) cout << arr[pos];
            cout << endl;
        }
        return 0;
    }
