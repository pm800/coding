#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

struct Job{
    int st, ft;
};

bool compareJob(Job j1, Job j2){
    return j1.ft <= j2.ft;
}

void f(int n, int* st_a, int* ft_a){
    Job j_a[n];
    for(int i=0; i<n; i++){
        j_a[i] = {st_a[i], ft_a[i]};
    }
    sort(j_a, j_a+n, compareJob);

    vector<int> s_v;
    for(int i=0; i<n; i++){
        int yst = j_a[i].st, yft = j_a[i].ft;
        int flag = 0;
        for(vector<int>::iterator it=s_v.begin(); it!=s_v.end(); it++){
            int x = *it;
            int xst = j_a[x].st;
            int xft = j_a[x].ft;
            if((xst<=xft && xft<= yst && yst<=yft) || (xft<=yst && yst<=yft && yft<=xst) ){
                *it = i;
                flag = 1;
                break;
            }
        }
        if(flag == 0) s_v.push_back(i);
    }

    cout<<s_v.size()<<endl;
}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n;
        cin >> n;
        int st_a[n], ft_a[n];
        for(int i=0; i<n; i++) cin >> st_a[i];
        for(int i=0; i<n; i++) cin >> ft_a[i];
        f(n, st_a, ft_a);
    }
    return 0;
}

