#include <iostream>
#include <algorithm>
#include <climits>
#include <vector>
using namespace std;

struct Job{
    int id, dl, prof;
};

bool compareJob(Job j1, Job j2){
    return j1.prof >= j2.prof;
}

void f(int* arr, int n){
    Job ja[n+1];
    for(int i=0; i<3*n; i+=3){
        ja[i/3].id = arr[i];
        ja[i/3].dl = arr[i+1];
        ja[i/3].prof = arr[i+2];
    }
    ja[n].id = -1; ja[n].dl = -1; ja[n].prof = INT_MAX;
    sort(ja, ja+n+1, compareJob);

    if(ja[1].prof>=INT_MAX) cout<<"error"<<endl;

    int slots[n+1] = {};
//    vector<int> ans_jobs;
//    ans_jobs.push_back(ja[1].id);
    slots[min(ja[1].dl,n)] = 1; slots[0] = 1;
    int cnt = 1, mprof = ja[1].prof; //assuming 1 or more jobs.

    for(int i=2; i<=n; i++){
        for(int j=min(ja[i].dl,n); j>=1; j--){
            if(slots[j]==0){
                slots[j] = 1;   cnt++;
//                ans_jobs.push_back(ja[i].id);
                mprof += ja[i].prof;
                break;
            }
        }
    }

    cout<<cnt<<" "<<mprof<<endl;
}

int main()
{
    int tcs;
    cin >> tcs;
//    tcs = 1;
    while(tcs--){
        int n;
        cin >> n;
//        n = 10;
        int arr[3*n];
        for(int i=0; i<3*n; i++) cin >> arr[i];
//        int arr[] = {1,9,50,2,7,40,3,4,30,4,3,30,5,2,20,6,5,15,7,3,10,8,2,9,9,2,8,10,1,7};


        f(arr, n);
    }

    return 0;
}
