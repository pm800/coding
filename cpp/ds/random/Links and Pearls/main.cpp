#include <iostream>
#include <string>
using namespace std;

int main()
{
    string s ;
    cin>>s;
    int p=0, l=0;
    for(char c: s){
        if('o'==c)
            p++;
        else
            l++;
    }
    if(l==0 || l%p==0)
        cout<<"YES";
    else
        cout<<"NO";

    return 0;
}
