#include <iostream>
using namespace std;
int** mat;
int** mat2;
int m,n;
int f(int i,int j){
//    cout<<i<<" "<<j<<" "<<cl<<" ";
    if (i>=m || j>=n || i<0 || j<0){
         return 0;
    }
    if(mat[i][j] == 0 || mat2[i][j] == 1) return 0;
    else{
        mat2[i][j] = 1;
        return 1 + f(i+1,j)+ f(i,j+1)+ f(i+1, j+1) + f(i-1, j) + f(i-1,j-1) + f(i, j-1) + f(i+1, j-1) + f(i-1, j+1);
    }
};
void show(){
    for(int i=0; i<m; i++){
        for(int j=0; j<n; j++){
            cout << mat[i][j]<<" ";
        }cout<<endl;
    }
    cout<<endl;
}
int main(){
    int tcs;
    cin >> tcs;
    while(tcs--){
        cin >> m >> n;
        int maxlen = 0;
        int lent = 0;

        mat = new int*[m];
        for(int i=0; i<m; i++) mat[i] = new int[n];
        mat2 = new int*[m];
        for(int i=0; i<m; i++) mat2[i] = new int[n];

        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                cin >> mat[i][j];
            }
        }

//        show();

        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                mat2[i][j] = 0;
            }
        }
        for(int i=0;i<m;i++){
            for(int j=0; j<n; j++){
                if (mat2[i][j] == 0){
                    int lent = f(i,j);
                    if (maxlen < lent) {maxlen = lent;};
                }
            }
        }
        cout<< maxlen<<endl;

        for(int i=0; i<m; i++) delete [] mat[i];
        delete [] mat;
        for(int i=0; i<m; i++) delete [] mat2[i];
        delete [] mat2;

    }
    return 0;
}
