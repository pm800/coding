#include <iostream>
// here answer is longest prefix suffix of whole string not of any substring.
using namespace std;

int f(string s){
    int n = s.size();
    int lps[n] = {};
    lps[0] = 0;
    int i=0, j=1;
    while(j<n){
        if(s[i] == s[j]){
            lps[j] = i+1;
            i++; j++;
        }
        else{
            if(i==0){
                lps[j] = 0;
                j++; continue;
            }
            i = lps[i-1];
        }
    }
    return lps[n-1];
}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        string s;
        cin >> s;
        int ans = f(s);
        cout << ans << endl;
    }
    return 0;
}










