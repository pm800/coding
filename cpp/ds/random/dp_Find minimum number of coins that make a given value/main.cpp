#include <iostream>
#include <algorithm>
using namespace std;

void show(int arr[], int n){
    for(int i=0; i<n; i++){
        cout << arr[i] << " ";
    }cout << endl;
}

void f(int sum, int coins[], int n){
    int arr[sum+1];
    fill_n(arr, sum+1, -1);
    arr[0] = 0;
    sort(coins, coins+n);
    for(int i=1; i<= sum; i++){
        int mm = 1234567890, flag = 0;
        for(int j=0; j<n; j++){
            int x = i - coins[j];
            if(x<0)
                break;
            if(arr[x] == -1)
                continue;
            if(mm > arr[x]){
                mm = arr[x];
                flag = 1;
            }
        }
        if(flag == 0)
            continue;
        arr[i] = mm + 1;
    }
    cout << arr[sum] << endl;

}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n, sum;
        cin >> sum >> n;
        int coins[n];
        for(int i=0; i<n; i++)
            cin >> coins[i];

        f(sum, coins, n);

    }
    return 0;
}
