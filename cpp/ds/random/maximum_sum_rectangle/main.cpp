#include <iostream>

using namespace std;
void show(int arr[], int n){
    for(int i=0; i<n; i++) cout<< arr[i]<< " ";
    cout <<endl;
}
void showmat(int** mat, int m, int n){
        for(int i=0; i<m; i++){
            for(int j = 0; j<n; j++)
                cout << mat[i][j] << " ";
            cout <<endl;
        }
        cout << endl;
}
void f(int arr[], int n, int rf[3]){

    int csum = arr[n-1], msum = arr[n-1], ml = n-1, mr = n-1, cr = n-1;
    for(int i=n-2; i>=0; i--){
        int temp = csum, tempr = cr;

        csum = arr[i];
        cr = i;
        if(csum > msum){
            msum = csum;
            ml = mr = i;
        }

        if(csum < arr[i] + temp){
            csum = arr[i] + temp;
            cr = tempr;
            if(csum > msum){
                msum = csum;
                ml = i; mr = cr;
            }
        }
    }
    rf[0] = msum; rf[1]=ml; rf[2] = mr;
}

void g(int** mat, int m, int n){
    int msum = -1234567890, ml = -1, mr = -1, mu = -1, md = -1;
    for(int r=0; r<m; r++){
        int arr[n] = {};
        for(int j= r; j<m; j++){
            for(int p=0; p<n; p++)
                arr[p] += mat[j][p];
            int fr[3] = {};
            f(arr, n, fr);
            int sum = fr[0], L = fr[1], R = fr[2];
            if(sum > msum){
                msum = sum;
                ml = L; mr = R; mu = r; md = j;
            };
        }
    }
    cout << msum << endl;
}


int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int m, n;
        cin >> m >> n;

        int** mat;
        mat = new int* [m];
        for(int i = 0; i<m; i++) mat[i] = new int [n];

        for(int i=0; i<m; i++)
            for(int j = 0; j<n; j++)
                cin >> mat[i][j];

        g(mat, m, n);

        for(int i = 0; i<m; i++) delete [] mat[i];
        delete [] mat;


    }
    return 0;
}
