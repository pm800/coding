#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

struct Activity{
    int st;
    int ft;
    int pos;
};

bool compareActivity(Activity a1, Activity a2){
    return a1.ft<a2.ft;
}

void f(int n, int* sta, int* fta){
    Activity aa[n];
    for(int i=0; i<n; i++){
        aa[i].st = sta[i];
        aa[i].ft = fta[i];
        aa[i].pos = i+1;
    }
    sort(aa, aa+n, compareActivity);
    int lft = aa[0].ft, cnt = 1;
    vector<int> ansV;
    ansV.push_back(aa[0].pos);

    for(int i=1; i<n; i++){
        if(aa[i].st >= lft){
            cnt++;
            lft = aa[i].ft;
            ansV.push_back(aa[i].pos);
        }
    }

    for(vector<int>::iterator it = ansV.begin(); it != ansV.end(); it++){
        cout << *it<<" ";
    }cout<<endl;

}


int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n;
        cin >> n;
        int sta[n], fta[n];
        for(int i=0; i<n; i++) cin >> sta[i];
        for(int i=0; i<n; i++) cin >> fta[i];

        f(n, sta, fta);
    }
    return 0;
}
