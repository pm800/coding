#include <iostream>
#include <vector>

using namespace std;

void f(vector<long long int> v1, vector<long long int> v2, vector<long long int> &parr, vector<long long int> &carr){
    long long int sum = 0;
    for(long long int j=0; j<v2.size(); j++){
        sum += parr[v2[j]];
    }
    for(long long int i=0; i<v1.size(); i++){
        carr[v1[i]] = sum;
    }
}
void showVec(vector<long long int> mv){
    for(long long int i=0; i<mv.size(); i++)
        cout<<mv[i]<<" ";
    cout<<endl;
}

int main()
{
    long long int tcs;
    cin >> tcs;
    while(tcs--){
        long long int n;
        cin >>n;
        vector<long long int> parr(10,1), carr(10,1);
        n=n-1;
        while(n--){
            vector<long long int> v1 , v2;
            v1 = {1,3}; v2 = {1,2,4};
            f(v1,v2,parr, carr);
            v1 = {4,6}; v2 = {4,1,5,7};
            f(v1,v2,parr, carr);
            v1 = {7,9}; v2 = {7,4,8};
            f(v1,v2,parr, carr);
            v1 = {0}; v2 = {0,8};
            f(v1,v2,parr, carr);
            v1 = {8}; v2 = {8,5,7,9,0};
            f(v1,v2,parr, carr);
            v1 = {5}; v2 = {5,2,4,6,8};
            f(v1,v2,parr, carr);
            v1 = {2}; v2 = {2,1,3,5};
            f(v1, v2,parr,carr);

            for(long long int i=0; i<10; i++)
                parr[i] = carr[i];
//            showVec(parr);
        }

        long long int sum = 0;
        for(long long int i=0; i<10; i++)
            sum += carr[i];
        cout << sum <<endl;
    }
}
