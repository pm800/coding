#include <iostream>
#include <string>
#include <cstring>
using namespace std;

int mat[101][101];


void f(string s1, string s2, int m, int n){
    memset(mat, 0, sizeof(mat));
    int ans = 0;
    for(int i=0; i<n;  i++){
        if(s1[0]==s2[i]){
            ans = 1; mat[0][i] = 1;
        }
    }
    for(int i=0; i<m;  i++){
        if(s2[0]==s1[i]){
            ans = 1; mat[i][0] = 1;
        }
    }
    for(int i=1; i<m; i++){
        for(int j=1; j<n; j++){
            if(s1[i] == s2[j]){
                mat[i][j] = mat[i-1][j-1]+1;
                ans = max(ans, mat[i][j]);
            }
        }
    };
    cout<<ans<<endl;
}

int main()
{
    int tcs;
    cin >>tcs;
    while(tcs--){
        int m, n;
        cin >>m>>n;
        string s1, s2;
        cin>>s1>>s2;
        f(s1, s2, m, n);
    }
    return 0;
}


