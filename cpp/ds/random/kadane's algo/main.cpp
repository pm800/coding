#include <iostream>

using namespace std;

int main()
{
    int tcs;
    cin <<tcs;
    while(tcs--){
        int n;
        cin << n;
        int arr[n] = {};
        int ansarr[n] = {};
        for(int i=0; i<n; i++) cin << arr[i];

        ansarr[n-1] = arr[n-1];
        for(int i=n-2; i>=0; i--) ansarr[i] = max(arr[i], arr[i] + ansarr[i+1]);
        int maxm = 0;
        for(int i=0; i<n; i++) if(maxm < ansarr[i]) maxm = ansarr[i];
    };
    return 0;
}
