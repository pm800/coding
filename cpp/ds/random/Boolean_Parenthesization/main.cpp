#include <iostream>
#include <vector>
#include <utility>

typedef long long int lli;

using namespace std;

void calculate(int a, int b, vector<vector<pair<lli,lli>>> & mat, vector<char> arr){
    int zw=0, ow = 0;
    for(int i=a; i<=b-2; i+=2){
        int m, n, x, y;
        pair<int,int> p1, p2;
        p1 = mat[a][i]; p2 = mat[i+2][b];
        m = p1.first; n = p1.second;
        x = p2.first; y = p2.second;
        char op = arr[i+1];
        switch(op){
            case '|':
                zw += m*x;
                ow += m*y + n*x + n*y;
                break;
            case '^':
                zw += n*y + m*x;
                ow += m*y + n*x;
                break;
            case '&':
                zw += m*y + n*x + m*x;
                ow += n*y;
                break;
        };
    }
    mat[a][b] = make_pair(zw%1003, ow%1003);
}

void show(vector<char> arr){
    for(int i=0; i<arr.size(); i++)
        cout << arr[i] << " ";
    cout <<endl;
}

void showmat(vector<vector<pair<int, int>>> mat){
        int n = mat.size();
        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){
                cout <<"("<< (mat[i][j]).first<<","<< (mat[i][j]).second<<"), ";
            }cout<<endl;
        }cout<<endl;
}
int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n;
        cin >> n;

        vector<char> arr(n,'a');

        for(int i=0; i<n; i++)
            cin >> arr[i];


        vector<vector<pair<lli,lli>>> mat(n, vector<pair<lli, lli>>(n, pair<lli,lli>(-1,-1) ) );



        for(int i=0; i<n; i+=2){
            int x=0,y=0;
            if(arr[i] == 'T') y=1;
            else x = 1;
            mat[i][i] = make_pair(x,y);
        }

        for(int s=2; s<=n-1; s+=2){
            int b = s, a = 0;
            while(b <= n-1){
                calculate(a,b,mat, arr);
                a += 2; b+= 2;
            }
        }
        cout << ((mat[0][n-1]).second)%1003<<endl;

    }
    return 0;
}



















