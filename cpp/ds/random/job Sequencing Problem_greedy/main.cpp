#include <iostream>
#include <algorithm>
#include <climits>

using namespace std;

struct Job{
    int pos, dl, prof;
};

bool compareJob(Job j1, Job j2){
    return j1.prof >= j2.prof;
}

void f(int* arr, int n){
    Job ja[n+1];
    for(int i=0; i<3*n; i+=3){
        ja[i/3].id = arr[i];
        ja[i/3].dl = arr[i+1];
        ja[i/3].prof = arr[i+2];
    }
    ja[n].id = -1; ja[n].dl = -1; ja[n].prof = INT_MAX;
    sort(ja, ja+n+1, compareJob);
    for(int i=1; i<=n; i++){
        int x = ja[i].dl;
        if(x<i){
            for(int j=x; j>=1; j--){
                if(ja[j].dl >= i){
                    Job temp = ja[j];
                    ja[j] = ja[i];
                    ja[i] = temp;
                    break;
                }
            }
        }
    }




    int cnt = 0, mprof = 0;
    for(int i=1; i<=n; i++){
        if(ja[i].dl >= i){
            cnt++;
            mprof += ja[i].prof;
        }
//        else{
////            cout<<"here  "<<ja[i].prof<<" "<<ja[i].dl<<" "<<i<<endl;
//        }
    }

//    cout <<endl<<endl;
//    for(int i=1; i<=cnt; i++){
//        cout<<"kkkk  "<<ja[i].prof<<" "<<ja[i].dl<<" "<<i<<endl;
//    }cout<<endl<<endl;

    cout<<cnt<<" "<<mprof<<endl;
}

int main()
{
    int tcs;
    cin >> tcs;
//    tcs = 1;
    while(tcs--){
        int n;
        cin >> n;
//        n = 10;
        int arr[3*n];
        for(int i=0; i<3*n; i++) cin >> arr[i];
//        int arr[] = {1,9,50,2,7,40,3,4,30,4,3,30,5,2,20,6,5,15,7,3,10,8,2,9,9,2,8,10,1,7};


        f(arr, n);
    }

    return 0;
}
