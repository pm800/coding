#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

void f(int n, int* arr, int* dep){
    sort(arr, arr+n);
    sort(dep, dep+n);
    int pr = 0, pe = 0, a = 0, d = 0;
    while(a<n && d<n){
        if(dep[d] == arr[a] && pe == pr){//arrival
            a++;
            if(pe>0){//any platform is empty
                pe--;
            }
            else{
                pr++;
            }
        }
        else if(dep[d] <= arr[a]){//departure case
            d++;
            pe++;
            if(pe>pr){
                pr = pe;
            }
        }
        else if(dep[d] > arr[a]){//arrival
            a++;
            if(pe>0){
                pe--;
            }
            else{
                pr++;
            }
        }

    }
    cout<<pr<<endl;
}




int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n;
        cin >> n;
        int st_a[n], ft_a[n];
        for(int i=0; i<n; i++) cin >> st_a[i];
        for(int i=0; i<n; i++) cin >> ft_a[i];
        f(n, st_a, ft_a);
    }
    return 0;
}

