#include <iostream>

using namespace std;

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n;
        cin >> n;
        if(n==1){
            cout<<1<<endl; continue;
        }
        if(n==2){
            cout<<2<<endl; continue;
        }
        if(n==3){
            cout<<3<<endl; continue;
        }
        int x = n-3;
        cout << n + x*(x+1)/2<<endl;
    }
    return 0;
}
