#include <iostream>
#include <vector>

using namespace std;

int aminsum = -1234567890;
int m, n;

void show(vector<vector<int>> mat){
    for(int i=0; i<mat.size(); i++){
        for(int j=0; j<mat[0].size(); j++){
            cout << mat[i][j] << " ";
        }cout <<endl;
    } cout << endl;
}

void f(int i, int j, int sum, int minsum, vector<vector<int>> mat){
    if(i>=m || j>=n)
        return;
    sum += mat[i][j];
    if(minsum > sum)
        minsum = sum;
    if(i == m-1 && j == n-1){
        if(aminsum < minsum){
            aminsum = minsum;
            return;
        }
    }
    if(i<m && j<n){
        f(i+1, j, sum, minsum, mat);
        f(i, j+1, sum, minsum, mat);
    }
}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        aminsum = -1234567890;
        cin >> m >> n;
        vector<vector<int>> mat(m, vector<int>(n,0));
        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                cin >> mat[i][j];
            }
        }
        f(0, 0, 0, 1234567890, mat);
        if(aminsum >= 0){
            cout << 1 << endl;
            continue;
        }
        else{
            cout << -(aminsum-1) << endl;
        }

    }
    return 0;
}
