#include <iostream>

using namespace std;

int f(string pat, string text){
    int m = pat.size();
    int lps[m] = {};
    int i=0, j=1;
    while(j<m){
        if(pat[i] == pat[j]){
            lps[j] = i+1;
            i++; j++;
        }
        else{
            if(i==0){
                lps[j] = 0;
                j++; continue;
            }
            i = lps[i-1];
        }
    }

    int n = text.size();
    i = 0;
    j = 0;
    while(i < n){
        if(pat[j] == text[i]){
            if(j == m-1){
                cout << "from "<<i-m+1<<" to "<<i<<endl;
                if(j==0){
                    i++; continue;
                }
                else{
                    j = lps[j-1]; i++; continue;
                }
            }
            j++; i++; continue;
        }
        else{
            if(j==0){
                i++; continue;
            }
            else{
                j = lps[j-1]; continue;
            }
        }
    }

}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        string pat, text;
        cin >> pat >> text;
        f(pat, text);
    }
    return 0;
}










