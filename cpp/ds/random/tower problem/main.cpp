#include <iostream>

using namespace std;
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wuninitialized"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#include <vector>
#include <string>
#include<utility> //pair
#include <algorithm> //sorting
void sf(int, int);
int m,n, ans_s, ans_dest;
int** arr;
int stepn=0;
int last[4] = {0}; // postion of last element.
int f(int a, int b, int dest, int src){
    int s = src, d = dest;
    int t = 6-s-d;
    for(int i = 1; i<= b-a+1; i++){
        int x = f(1, b-i, t, s);
        if(x) return 1;
        sf(s, dest);
        stepn++;
        if(stepn > m-1){
            ans_s = s; ans_dest = dest;
            return 1;
        }
        swap(t, s);
    }
    return 0;
}
void show(int**, int r, int c);
void sf(int s, int dest){
    arr[dest][last[dest]+1] = arr[s][last[s]];
    arr[s][last[s]] = 0;
    last[s]--;
    last[dest]++;
//    show(arr, 4, n+1);
}
void show(int**, int r, int c){
    for(int i=0; i<r; i++){
        for(int j=0; j<c; j++){
            cout<<arr[i][j] << " ";
        }
        cout << endl;
    }
    cout<<endl;
}
int main()
{
    int tcs;
    pair<int, int> p;
    cin >>tcs;
    while(tcs--){
        stepn = 0;
        cin>>n>>m;
        arr = new int*[4];
        for(int i = 0; i<4; i++){
            arr[i] = new int[n+1];
        };
        for(int i=1; i<=n; i++) arr[1][i] = n-i+1;
        last[1] = n; last[2] = 0; last[3] = 0;
        f(1, n, 3, 1);
        cout << ans_s << " " << ans_dest<<endl;
        for(int i = 0; i<4; i++){
            delete [] arr[i];
        }
        delete [] arr;
    }
    return 0;
}
