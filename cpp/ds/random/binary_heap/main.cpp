#include <iostream>

using namespace std;

int show(int* arr, int n){
    for(int i=0; i<n; i++)
        cout<<arr[i]<<" ";
    cout<<endl;
}

class MinHeap{
public:
    int capacity, hsize;
    int* harr;
    MinHeap(int _capacity);

    int parent(int i) { return (i-1)/2; }
    int left(int i) { return (2*i + 1); }
    int right(int i) { return (2*i + 2); }

    int extractMin();
    int getMin() { return harr[0]; }
    void deleteKey(int i);
    void insertKey(int k);
    void decreaseKey(int i, int new_val);
    void increseKey(int i, int new_val);

};

MinHeap::MinHeap(int _capacity){
    capacity = _capacity;
    hsize = 0;
    harr = new int[capacity];
}

void MinHeap::insertKey(int k){
    if(hsize >= capacity)
        return throw invalid_argument("hsize >= capacity");
    hsize++;
    int p, c;
    c = hsize-1;
    harr[c] = k;
    while(c>=1){
        p = parent(c);
        if(harr[p] >= harr[c]){
            swap(harr[p], harr[c]);
            c = p;
        }
        else break;
    }
}

void MinHeap::decreaseKey(int i, int val){
    if(i>=hsize)
        return throw invalid_argument("index > hsize error");
    harr[i] = val;
    int p,c;
    c = i;
    while(c>=1){
        p = parent(c);
        if(harr[p] >= harr[c]){
            swap(harr[p], harr[c]);
            c = p;
        }
        else break;
    }
}

void MinHeap::increseKey(int i, int val){
    if(i>=hsize)
        return throw invalid_argument("index > hsize error");
    harr[i] = val;
    int c, l, r;
    c = i;
    while(c<hsize){
        l = left(c);
        r = right(c);
        int s = c;
        if(l<hsize && harr[l] < harr[s])
            s = l;
        if(r<hsize && harr[r] < harr[s])
            s = r;
        if(s == c)
            break;
        swap(harr[s], harr[c]);
        c = s;
    }
}

void MinHeap::deleteKey(int i){
    increseKey(i, harr[hsize-1]);
    hsize--;
}

int MinHeap::extractMin(){
    int i = getMin();
    deleteKey(0);
    return i;
}

int main()
{
    MinHeap h(11);
    h.insertKey(3);
    h.insertKey(2);
    h.deleteKey(1);
    h.insertKey(15);
    h.insertKey(5);
    h.insertKey(4);
    h.insertKey(45);
    show(h.harr, h.hsize);
    cout << h.extractMin() << endl;
    show(h.harr, h.hsize);
    cout << h.getMin() << endl;
    h.decreaseKey(2, 1);
    cout << h.getMin();
    return 0;
}


















