/*
Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function would be added by GfG's Online Judge.*/


/* A binary tree node has data, pointer to left child
   and a pointer to right child
struct Node
{
    int data;
    Node* left;
    Node* right;
}; */
/* Prints inorder traversal of Binary Tree.  In output
   all keys should be separated by space. For example
  inorder traversal of below tree should be "20 10 30"
         10
       /      \
   20       30 */

#include <stack>
#include <iostream>
using namespace std;
struct Node
{
    int data;
    Node* left = NULL;
    Node* right = NULL;
};
void inOrder(Node* root)
{
  if(root == NULL) return;
  inOrder(root->left);
  cout<<root->data<<" ";
  inOrder(root->right);
}

void preorder(Node* root){
    if(root == NULL) return;
}

void postOrder(Node* root){
    stack<Node*> lv;
    stack<Node*> rv;
    stack<Node*> s;
    if (root != NULL) s.push(root);
    while(!s.empty()){
        root = s.top();
        Node* l = root->left;
        Node* r = root->right;
        if(lv.empty() || (!lv.empty() && lv.top() != root)){
            lv.push(root);
            if(l){
                s.push(root->left);
                continue;
            }
        };
        if(rv.empty() || (!rv.empty() && rv.top() != root)){
            rv.push(root);
            if(r){
                s.push(root->right);
                continue;
            }
            else{
                s.pop(); rv.pop(); lv.pop(); cout<< root->data<<" "; continue;
            }
        }
        else{
            s.pop(); rv.pop(); lv.pop(); cout<< root->data<<" "; continue;
        }
    }

}
void postOrder2(Node* root){
    stack<Node*> s1;
    stack<Node*> s2;
    if(root != NULL) s1.push(root);
    while(!s1.empty()){
        root = s1.top();
        s1.pop();
        s1.push(root->left);
        s1.push(root->right);
        s2.push(root);
    }
    while(!s2.empty()){
        cout<<s2.top();
        s2.pop();
    }

}

int main(){
    Node n;
    Node ln, rn;
    n.data = 1;
    ln.data = 2;
    rn.data = 3;
    n.left = &ln;
    n.right = &rn;
    postOrder(&n);
    return 0;
}
