#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

vector<int> calc(int n){
    int td = 1;
    while(n>=10){
        n = n/10;
        td++;
    }
    int fd = n;
    vector<int> temp = {fd, td};
    return(temp);
}

int h(int n){
    int psum = 0;
    int x = 45;
    int csum = 0;
    while(n){
        csum = x+psum*10;
        psum = csum;
        x = x*10; n=n/10;
    }
//    cout <<"H "<<csum<<endl;
    return csum;
}

int g(int n){
    vector<int> temp = calc(n);
    int y = temp[0], td = temp[1];
    int z = y-1;
    int sum = (z*(z+1)/2)*int(pow(10, td-1)) + y;
    int x = int(pow(10, td-1)) - 1;
//    cout << "g "<<n<<" "<< z <<" "<<td<<" "<<y<<" "<<sum<<" "<<x<<endl;
    return h(x)*y + sum;
}

int sumOfDigitsFrom1ToN(int n){
    if(n<=9)
        return n*(n+1)/2;
    vector<int> temp = calc(n);
    int fd = temp[0], td = temp[1];
    int x = fd * int(pow(10,td-1));
    int y = n-x;
//    cout << x <<" "<< y <<" "<< fd << endl;
    return(g(x) + fd*y + sumOfDigitsFrom1ToN(y));

}

int main()
{
	int tcs;
	cin >>tcs;
	while(tcs--){
            int n;
    cin >>n;
        cout << "Sum of digits in numbers from 1 to " << n << " is "
		<< sumOfDigitsFrom1ToN(n)<<endl;
	}

	return 0;
}
