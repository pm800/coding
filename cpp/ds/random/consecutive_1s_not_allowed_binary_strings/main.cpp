#include <iostream>

using namespace std;

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n;
        cin >> n;
        long long int a=1, b=1;
        while(n--){
            long long int temp = b;
            b = a+b;
            if(b>=1000000007) b = b%1000000007;
            a = temp;
        }
        cout << b%1000000007 << endl;
    }
}
