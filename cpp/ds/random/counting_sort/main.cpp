#include <iostream>

using namespace std;

void show(int arr[], int n){
    for(int i=0; i<n; i++) cout<< arr[i]<< " ";
    cout <<endl;
}

int main()
{
    int n=8;
    int k=5;
    int A[]= {2,1,2,1,4,4,2,5};
    int B[8];
    int C[6] = {};
    for(int i=0; i<8; i++) C[A[i]]++;
    for(int i=1; i<6; i++) C[i] += C[i-1];
    for(int i=0; i<n; i++){
        int p = n-1 - (C[A[i]]-1);
        B[p] = A[i];
        C[A[i]]--;
    }
    show(B,8);
    return 0;
}
