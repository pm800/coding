#include <iostream>

using namespace std;

int g(int n){
    if(n<=6) return n;
    int arr[n+1];
    for(int i=0; i<=6; i++) arr[i] = i;
    for(int i=7; i<=n; i++){
        int maxm = 0;
        for(int j=i-3; j>=1; j--){
            int x = arr[j];
            int y = x + x*(i-j-2);
            if(y>maxm) maxm = y;
        }
        arr[i] = maxm;
    }
    return arr[n];
}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n;
        cin >> n;
        cout << g(n) <<endl;
    }
    return 0;
}
