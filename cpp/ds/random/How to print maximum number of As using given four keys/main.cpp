#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

int f(int s, int c, int a, int rk, int n){

    if (rk <= 0 || s>n || c>n){
        return a;
    }

    int y=0, b=0, x=0, d=0;
    if(s<a){
        y = f(a,c,a,rk-1,  n);
    }
    if(c>0){
        b = f(s,c,a+c,rk-1, n);
    }
    if(c==0){
        x = f(s,c,a+1,rk-1, n);
    }
    if(c<s){
        d = f(s,s,a,rk-1, n);
    }

    int maxm = y;
    maxm = max(maxm, b);
    maxm = max(maxm, x);
    maxm = max(maxm, d);
    return maxm;
}

void g(int n){
    int s = 0, c = 0, a = 0, rk = n;
    cout << f(s, c, a, rk,  n)<< endl;
}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n;
        cin >> n;
        g(n);
    }
    return 0;
}

