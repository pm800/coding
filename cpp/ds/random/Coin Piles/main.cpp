#include <iostream>
#include <algorithm>
#include <climits>

using namespace std;

void f(int* arr, int n, int k){
    sort(arr, arr+n);
    int msum = INT_MAX;
    int suma[n+1];
    suma[0] = 0;
    for(int i=1; i<=n; i++) suma[i] = suma[i-1] + arr[i-1];
    for(int i=0; i<=n-2; i++){
        int sum = suma[i];
        if(sum >= msum) break;
        for(int j=n-1; j>=i+1; j--){
            if(arr[j]-arr[i]<=k) break;
            sum += arr[j] - arr[i] - k;
        }
        if(sum < msum) msum = sum;
    }
    cout << msum<<endl;
}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n, k;
        cin >>n>>k;
        int arr[n];
        for(int i=0; i<n; i++) cin>>arr[i];

        f(arr, n, k);
    }
    return 0;
}
