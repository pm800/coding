#include <iostream>
#include <vector>

using namespace std;


void show(vector<vector<int>> mat){
    for(int i=0; i<mat.size(); i++){
        for(int j=0; j<mat[0].size(); j++){
            cout << mat[i][j] << " ";
        }cout <<endl;
    } cout << endl;
}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int m, n;
        cin >> m >> n;
        vector<vector<int>> points(m, vector<int>(n,0));
        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                cin >> points[i][j];
            }
        }
        vector<vector<int>> mat(m, vector<int>(n,0));

        if(points[m-1][n-1] > 0)
            mat[m-1][n-1] = 1;
        else
            mat[m-1][n-1] = -points[m-1][n-1] + 1;

        for(int i=m-1, j=n-2; j>=0; j--){
            int x = points[i][j], r = mat[i][j+1];
            int y = r - x;
            if(y<=0) y = 1;
            mat[i][j] = y;
        }

        for(int i=m-2, j=n-1; i>=0; i--){
            int x = points[i][j], r = mat[i+1][j];
            int y = r - x;
            if(y<=0) y = 1;
            mat[i][j] = y;
        }

        for(int i=m-2; i>=0; i--){
            for(int j=n-2; j>=0; j--){
                int x = points[i][j], r = mat[i][j+1];
                int y1 = r - x;
                if(y1<=0) y1 = 1;

                r = mat[i+1][j];
                int y2 = r - x;
                if(y2<=0) y2 = 1;

                mat[i][j] = min(y1, y2);
            }
        }


        cout << mat[0][0] << endl;
    }
    return 0;
}




















