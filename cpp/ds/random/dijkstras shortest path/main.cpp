#include <iostream>
#include <climits>
#include <list>
#include <vector>
#define inf INT_MAX

using namespace std;


/////////////////////////////////////////////////////////////////////////////////////////
struct Node{
    int v, w;
};


class MinHeap{
public:
    int capacity, hsize;
    Node* harr;
    MinHeap(int _capacity);
    int* position;

    int parent(int i) { return (i-1)/2; }
    int left(int i) { return (2*i + 1); }
    int right(int i) { return (2*i + 2); }

    int extractMin();
    int getMin() { return harr[0].v; }
    void deleteKey(int i);
//    void insertKey(int k, int w);
    void decreaseKey(int i, int new_val);
    void minHeapify(int i);//i = index
    bool isMinHeapNode(int v);//v = vertex

    void swp(int p1, int p2);

    int show();

};

bool MinHeap::isMinHeapNode(int v){
    int p = position[v];
    if(p < hsize && p>=0)
        return true;
    return false;
}

int MinHeap::show(){
    for(int i=0; i<hsize; i++)
        cout<<harr[i].v<<" ";
    cout<<endl;
}

void MinHeap::swp(int p1, int p2){
    int v1, v2;
    v1 = harr[p1].v;
    v2 = harr[p2].v;
    position[v1] = p2;
    position[v2] = p1;
    swap(harr[p1], harr[p2]);
}

MinHeap::MinHeap(int _capacity){
    capacity = _capacity;
    hsize = 0;
    harr = new Node[capacity];
    position = new int [capacity];
}

//void MinHeap::insertKey(int k, int w){// k is vertex.
//    if(hsize >= capacity){
//        cout<<"error "<<k<<" "<<hsize<< " "<<capacity<<endl;
//        throw invalid_argument("h1");
//    }
//    hsize++;
//    int p, c;
//    c = hsize-1;
//    harr[c] = {k, w};
//    position[k] = c;
//    while(c>=1){
//        p = parent(c);
//        if(harr[p].w >= harr[c].w){
//            swp(p,c);
//            c = p;
//        }
//        else break;
//    }
//}

void MinHeap::decreaseKey(int i, int val){// i = vertex.
    int pos = position[i];
    if(pos>=hsize){
        cout<<"error "<<i<<" "<<val<<" "<<pos<<" "<<hsize<<endl;
        throw invalid_argument("h2");
    }
    harr[pos].w = val;
    int p,c;
    c = pos;
    while(c>=1){
        p = parent(c);
        if(harr[p].w > harr[c].w){
            swp(p,c);
            c = p;
        }
        else break;
    }
}


void MinHeap::minHeapify(int i){// i = index.
    if(i>=hsize) {
        cout<<"error "<<i<<" "<<hsize<<endl;
        throw invalid_argument("h3");
    }
    int c, l, r;
    c = i;
    while(c<hsize){
        l = left(c);
        r = right(c);
        int s = c;
        if(l<hsize && harr[l].w < harr[s].w)
            s = l;
        if(r<hsize && harr[r].w < harr[s].w)
            s = r;
        if(s == c)
            break;
        swp(s,c);
        c = s;
    }
}



void MinHeap::deleteKey(int i){//i is index
    position[harr[hsize-1].v] = i;
    position[harr[i].v] = hsize-1;
    harr[i] = harr[hsize-1];
    minHeapify(i);
    hsize--;
}

int MinHeap::extractMin(){
    int i = getMin();
    deleteKey(0);
    return i;
}
//////////////////////////////////////////////////////////////////////////////////////////
struct GraphNode{
    int dest, w;
};

class Graph{
public:
    int V;
    list<GraphNode>* adj;
    Graph(int v);
    void addEdge(int u, int v, int w);
    void primsMst();
    void show(int* arr, int n);
};

void Graph::show(int* arr, int n){
    for(int i=0; i<n; i++){
        cout<<i<<" to "<< arr[i]<<endl;
    }
}

Graph::Graph(int v){//undirected
    V = v;
    adj = new list<GraphNode> [V];
}

void Graph::addEdge(int u, int v, int w){
    adj[u].push_back({v,w});
    adj[v].push_back({u,w});
}

void Graph::primsMst(){
    vector<int> v1;
    int from[V];
    fill_n(from, V, -1);
    int dist[V];
    fill_n(dist, V, inf);
    dist[0] = 0;
    MinHeap heap(V);
    for(int i=0; i<V; i++){
        heap.harr[i] = {i, inf};
        heap.position[i] = i;
    }
    heap.harr[0] = {0, 0};
    heap.position[0] = 0;
    heap.hsize = V;
    while(heap.hsize > 0){
        int m = heap.extractMin();
        list<GraphNode>::iterator it;
        for(it = adj[m].begin(); it != adj[m].end(); it++){
            int v, w;
            v = it->dest;
            w = it->w;
            if(heap.isMinHeapNode(v) && w + dist[m] < dist[v]){
                dist[v] = w + dist[m];
                from[v] = m;
                heap.decreaseKey(v, dist[v]);
            }
        }
    }
    show(dist, V);

}
///////////////////////////////////////////////////////////////////////////////////////////

void addEdge(Graph& graph, int u, int v, int w){
    graph.addEdge(u, v, w);
}

int main()
{
    int V = 9;
	Graph graph = Graph(V);
	 addEdge(graph, 0, 1, 4);
    addEdge(graph, 0, 7, 8);
    addEdge(graph, 1, 2, 8);
    addEdge(graph, 1, 7, 11);
    addEdge(graph, 2, 3, 7);
    addEdge(graph, 2, 8, 2);
    addEdge(graph, 2, 5, 4);
    addEdge(graph, 3, 4, 9);
    addEdge(graph, 3, 5, 14);
    addEdge(graph, 4, 5, 10);
    addEdge(graph, 5, 6, 2);
    addEdge(graph, 6, 7, 1);
    addEdge(graph, 6, 8, 6);
    addEdge(graph, 7, 8, 7);
	graph.primsMst();
    return 0;
}


















