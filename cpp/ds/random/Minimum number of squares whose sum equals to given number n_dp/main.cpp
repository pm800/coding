#include <iostream>

using namespace std;

void show(int arr[], int n){
    for(int i=0; i<n; i++){
        cout << arr[i] << " ";
    }cout <<endl;
}

int f(int n){
    if(n<=3) return n;
    int arr[n+1] = {};
    arr[0] = 0; arr[1] = 1; arr[2] = 2; arr[3] = 3;
    for(int i=4; i<=n; i++){
        int x = 1, mm = 1234567890;
        while(x*x <= i){
            if(mm > 1+arr[i-x*x])
                mm = 1 + arr[i-x*x];
            x++;
        }
        arr[i] = mm;
    }
    return arr[n];
}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n;
        cin >> n;
        cout << f(n) << endl;
    }
    return 0;
}


















