#include<iostream>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    cin>>t;
    while(t--)
    {
        int n;
        cin>>n;
        int arr[n];
        for(int i = 0 ; i <n ; i++)
            cin>>arr[i];
        int** M = new int*[n-1];
        for(int i = 0 ; i < n ; i++)
            M[i] = new int[n-1];
        for(int i = 0 ; i < n-1 ; i++){
            for (int j=0; j<n-1; j++){
            M[i][j] = 0;
            }
        }
        n = n-1;

        for(int l=2; l<n+1; l++){
            for(int a=0; a<n-l+1; a++){
                int b = a+l-1;
                int mm = 1000000000;
                for(int p=a; p<b; p++){
                    int x = M[a][p] + M[p+1][b] + arr[a]*arr[p+1]*arr[b+1];
                    if(x<mm){
                    mm = x;
                    }
                }
                M[a][b] = mm;
            }
        }
        cout<<M[0][n-1]<<endl;
    }
    return(0);

}
