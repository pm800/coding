
#include <bits/stdc++.h>
using namespace std;
#define MAX 1000
int findIslands(int A[MAX][MAX],int N,int M);
int main() {
	// your code goes here
	int T;
	cin>>T;
	int A[MAX][MAX];
	while(T--)
	{
		int N,M;
		cin>>N>>M;
		memset(A,0,sizeof A);
		for(int i=0;i<N;i++)
		for(int j=0;j<M;j++)
		cin>>A[i][j];
		cout<<findIslands(A,N,M)<<endl;
	}
	return 0;
}


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/*you are required to complete this method*/

int visited[MAX][MAX] = {};
int dx[8] = {-1,-1,-1,0,0,1,1,1};
int dy[8] = {-1,0,1,-1,1,-1,0,1};
int _stamp = 1;
void dfsUtil(int A[MAX][MAX], int N, int M, int i, int j){
    if(i<0 || i>=N)
        return;
    if(j<0 || j>=M)
        return;
    if(visited[i][j] != 0)
        return;
    if(A[i][j] == 1){
        visited[i][j] = _stamp;
        for(int p=0; p<8; p++){
            dfsUtil(A, N, M, i+dx[p], j+dy[p]);
        };
    }
}

int findIslands(int A[MAX][MAX], int N, int M){
    int cnt = 0;
    memset(visited, false, sizeof(visited));
    for(int i=0; i<N; i++){
        for(int j=0; j<M; j++){
            if(A[i][j] == 1 && visited[i][j] == 0){
                dfsUtil(A,N,M,i,j);
                cnt++;
                _stamp++;
            }
        }
    }


    return cnt;
}






















