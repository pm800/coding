#include <iostream>
#include <vector>

using namespace std;

void show(vector<vector<long long int>> mat){
    for(int i=0; i<mat.size(); i++){
        for(int j=0; j<mat[0].size(); j++){
            cout << mat[i][j] << " ";
        }cout <<endl;
    } cout << endl;
}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n, sum;
        cin >> n >> sum;
        vector<vector<long long int>> mat(sum+1, vector<long long int>(n,0));

        for(int j=0; j<n; j++) mat[0][j] = 1;
        for(int i=0; i<=9 && i<=sum; i++) mat[i][n-1] = 1;
        for(int i=10; i<=sum; i++) mat[i][n-1] = 0;


        for(int i=1; i<=sum; i++){
            for(int j=n-2; j>=0; j--){
                mat[i][j] = mat[i-1][j] + mat[i][j+1];
                if(i>=10) mat[i][j] -= mat[i-10][j+1];
            }
        }

        long long int ans = mat[sum-1][0];
        if(sum>=10) ans -= mat[sum-10][1];
        cout << ans << endl;
    }
    return 0;
}
