#include <iostream>
#include <algorithm>
using namespace std;

// A job has start time, finish time and profit.
struct Job
{
    int start, finish, profit;
};

int g(Job arr[], int l , int r, int x, int kind){
    if(l>=r) cout <<"error 18 "<<endl;
    switch(kind){
    case 0: // for exact match
        if(arr[l].start == x) return l;
        else if(arr[r].start == x) return r;
        else return -1;
        break;
    case 1: // for just greater than x
        if(arr[l].start>=x) return l;
        else if(arr[r].start>=x) return r;
        else return -1;
        break;
    case -1: // for just smaller than x
        if(arr[r].start<=x) return r;
        else if(arr[l].start <= x) return l;
        else return -1;
        break;
    default:
        cout <<"error";
    }
}

int jgi(Job arr[], int n, int l, int r, int x, int kind){
    if(l==r) return l;
    if(l>r) return -1;
    int m = l+(r-l)/2;
    while(m>l){
        if(arr[m].start == x) return m;
        if(arr[m].start > x) r = m;
        if(arr[m].start < x) l = m;
        m = l+(r-l)/2;
    }
    int pos = g(arr, l, r, x, kind);
    return pos;
}


bool compare_job(Job j1, Job j2){
    return j1.start <= j2.start;
}

int findMaxProfit(Job arr[], int n){
    sort(arr, arr+n, compare_job);
    int profit[n] = {};
    profit[n-1] = arr[n-1].profit;
    for(int i=n-2; i>=0; i--){
        int x = arr[i].finish;
        int y = jgi(arr, n, i+1, n-1, x, 1);
        if(y == -1){
            profit[i] = arr[i].profit;
            continue;
        }
        else{
            profit[i] = arr[i].profit + profit[y];
        }
    }
    int maxm = 0;
    for(int i=0; i<n; i++){
        if(maxm < profit[i])
            maxm = profit[i];
    }
    return maxm;
}


int main()
{
    Job arr[] = {{3, 10, 20}, {1, 2, 50}, {6, 19, 100}, {2, 100, 200}};
    int n = sizeof(arr)/sizeof(arr[0]);
    cout << "The optimal profit is " << findMaxProfit(arr, n);
    return 0;
}
