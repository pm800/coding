#include <iostream>

using namespace std;

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int m = 9, n, cnt = 0, k, j, flag;
        cin >> n;
        int l[9], l1[9], l2[9];
        for(int i=0; i<9; i++){
            l[i] = i + 2;
            l1[i] = i + 2;
            l2[i] = 1;
        }
        for(int i=1; i<n+1; i++){
            flag = 0;
            for(j=0; j<m; j++){
                if(i == l1[j]){
                    l2[j] += 1;
                    l1[j] = l[j]*l2[j];
                    cnt += 1;
                    flag = 1;
                    break;
                }
            }
            if(flag == 0) continue;
            k = j + 1;
            for(int j=k; j<m; j++){
                if(i == l1[j]){
                    l2[j] += 1;
                    l1[j] = l[j]*l2[j];
                }
            }
        }
        cout << n - cnt << ' '<<n<<' '<<cnt<< endl;
    }
    return 0;
}
