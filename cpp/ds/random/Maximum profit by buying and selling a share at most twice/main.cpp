#include <iostream>

using namespace std;

void show(int arr[], int n){
    for(int i=0; i<n; i++){
        cout <<arr[i]<<" ";
    }cout <<endl;
}

void f(int arr[], int n){
    int farr[n]={}, barr[n] = {};

    int mprofit = 0, mini = 0;
    farr[0] = 0;
    for(int i=1; i<n; i++){
        farr[i] = mprofit;
        if(arr[i]<arr[mini]){
            mini = i;
            continue;
        }
        if(arr[i]==arr[mini]){
            continue;
        }
        if((arr[i]-arr[mini]) > mprofit){
            mprofit = arr[i] - arr[mini];
            farr[i] = mprofit;
        }
    }

    int maxi = n-1;
    mprofit = 0;
    barr[n-1] = 0;
    for(int i=n-2; i>=0; i--){
        barr[i] = mprofit;
        if(arr[i]>arr[maxi]){
            maxi = i;
            continue;
        }
        if(arr[i]==arr[maxi]){
            continue;
        }
        if((arr[maxi]-arr[i]) > mprofit){
            mprofit = arr[maxi]-arr[i];
            barr[i] = mprofit;
        }
    }


    mprofit = barr[0];
    for(int i=1; i<=n-3; i++){
        int x = farr[i] + barr[i+1];
        if(x > mprofit){
            mprofit = x;
        }
    }

    cout << mprofit << endl;
}

int main()
{
	int price[] = {2, 30, 15, 10, 8, 25, 80,12,33,1,99,22,2,600,666};
	int n = sizeof(price)/sizeof(price[0]);
	f(price, n);
    return 0;
}


















