#include <iostream>
#include<iostream>
#include<list>
using namespace std;

class Graph{//directed graph.
    int v;
    list<int>* adj;
    void dfsUtil(int v, bool* visited);
    bool cycleUtil(int v, bool* visited, bool* rstack);
public:
    Graph(int v);
    void addEdge(int v, int w);
    void dfs(int v);
    bool isCyclic();
};

Graph::Graph(int v){
        this -> v = v;
        adj = new list<int> [v];
}

void Graph::addEdge(int v, int w){
    if(v > this->v || w > this->v)
        throw invalid_argument("error");
    adj[v].push_back(w);
}

void Graph::dfs(int v){
    bool visited[this->v] = {};
    dfsUtil(v, visited);
}

void Graph::dfsUtil(int v, bool* visited){
    visited[v] = true;
    cout << v<<" ";
    list<int>::iterator it;
    for(it = adj[v].begin(); it != adj[v].end(); it++){
        if(visited[*it] == true)
            continue;
        dfsUtil(*it, visited);
    }
}

bool Graph::isCyclic(){
    bool visited[this->v] = {};
    bool rstack[this->v] = {};
    for(int i=0; i<v; i++){
        for(int i=0; i<v; i++){
            if(rstack[i] == true)
                throw invalid_argument("some problem");
        }
        if(visited[i] == false)
            if(cycleUtil(i, visited, rstack))
                return true;
    }
    return false;
}

bool Graph::cycleUtil(int v, bool* visited, bool* rstack){
    visited[v] = true;
    rstack[v] = true;
    list<int>::iterator it;
    for(it = adj[v].begin(); it != adj[v].end(); it++){
        if(visited[*it] == true){
            if(rstack[*it] == true){
                cout << "cycle present "<<endl;
                rstack[v] = false;
                return true;
            }
            continue;
        }
        else{
            int x = cycleUtil(*it, visited, rstack);
            if(x == true){
                rstack[v] = false;
                return true;
            }
        }

    }
    rstack[v] = false;
    return false;
}


int main()
{
//    Graph graph = Graph(8);
//    graph.addEdge(0,1);
//    graph.addEdge(0,2);
//    graph.addEdge(1,3);
//    graph.addEdge(1,4);
//    graph.addEdge(1,7);
//    graph.addEdge(4,5);
//    graph.addEdge(4,6);
//    graph.addEdge(6,6);//1
//    graph.addEdge(6,0);//2
//    graph.addEdge(6,2);
//    graph.addEdge(7,1);//3


    Graph graph = Graph(4);
    graph.addEdge(0,1);
    graph.addEdge(1,2);
    graph.addEdge(1,3);
    graph.addEdge(2,3);
    graph.addEdge(3,0);
    graph.addEdge(3,1);

    graph.isCyclic();

//    graph.dfs(0);
    return 0;
}


























