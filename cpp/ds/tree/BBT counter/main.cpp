#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset
#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define p5(x, y, a, b, c) cout << x << " "<<y<<  " "<<a<<" "<<b<< " "<<c<<endl
#define p6(x, y, a, b, c, d) cout << x << " "<<y<<  " "<<a<<" "<<b<< " "<<c<<" "<<d<<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl

#define mod 1000000007
using namespace std;

int dp[1005];

void init(){
	fill(dp, dp+1005, 0);
	dp[0] = 1;
	dp[1] = 1;
	long int x;
	for(int i=2; i<1005; i++){
		x = dp[i-1] * dp[i-1] + dp[i-2] * dp[i-1] * 2;
		x %= mod;
		dp[i] = x;
	}
	return;
}

int main(){
	init();
	int tcs;
	cin >> tcs;
	int h;
	while(tcs--){
		cin>>h;
		cout<<dp[h]<<endl;
	}
	return 0;
}


















