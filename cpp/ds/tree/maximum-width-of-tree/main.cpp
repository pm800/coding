#include <bits/stdc++.h>
using namespace std;
/* A binary tree node has data, pointer to left child
   and a pointer to right child */
struct Node
{
    int data;
    struct Node* left;
    struct Node* right;
};
/* Function to get the maximum width of a binary tree*/
int getMaxWidth(Node* root);
/* Helper function that allocates a new node with the
   given data and NULL left and right pointers. */
struct Node* newNode(int data)
{
  struct Node* node = (struct Node*)
                       malloc(sizeof(struct Node));
  node->data = data;
  node->left = NULL;
  node->right = NULL;
  return(node);
}
/* Driver program to test size function*/
int main()
{
  int t;
  struct Node *child;
  scanf("%d", &t);
  while (t--)
  {
     map<int, Node*> m;
     int n;
     scanf("%d",&n);
     struct Node *root = NULL;
     while (n--)
     {
        Node *parent;
        char lr;
        int n1, n2;
        scanf("%d %d %c", &n1, &n2, &lr);
        if (m.find(n1) == m.end())
        {
           parent = newNode(n1);
           m[n1] = parent;
           if (root == NULL)
             root = parent;
        }
        else
           parent = m[n1];
        child = newNode(n2);
        if (lr == 'L')
          parent->left = child;
        else
          parent->right = child;
        m[n2]  = child;
     }
     cout << getMaxWidth(root) << endl;
  }
  return 0;
}


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/*  Structure of a Binary Tree
struct Node
{
    int data;
    Node* left, *right;
}; */
/* Function to get the maximum width of a binary tree*/


int getMaxWidth(Node* root){
    if(root==NULL) return 0;
    queue<Node*> q;
    int mw = 0;
    q.push(root);
    mw++;
    while(!q.empty()){
        int n = q.size();
        if(mw<n) mw = n;
        while(n--){
            Node* node = q.front();
            q.pop();
            if(node->left)
                q.push(node->left);
            if(node->right)
                q.push(node->right);
        }
    }
    return mw;
}










//int getMaxWidth(Node* root){
//    if(root==NULL) return 0;
//    vector<Node*> v1, v2;
//    int mw = 0;
//    v1.push_back(root);
//    mw++;
//    while(!v1.empty()){
//        vector<Node*> ::iterator it;
//        for(it = v1.begin(); it != v1.end(); it++){
//            Node* node = *it;
//            if(node->left)
//                v2.push_back(node->left);
//            if(node->right)
//                v2.push_back(node->right);
//        }
//        if(mw<v2.size())
//            mw = v2.size();
//        v1 = v2;
//        v2 = {};
//    }
//    return mw;
//}





















