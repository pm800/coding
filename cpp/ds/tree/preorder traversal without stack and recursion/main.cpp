#include <iostream>
using namespace std;

class Node {
    public:
        int data;
        Node *left;
        Node *right;
        Node(int d) {
            data = d;
            left = NULL;
            right = NULL;
        }
};


void preOrder(Node *root) {
    Node* cur = root;
    while(cur != NULL){
        if(cur->left == NULL){
            cout<<cur->data<<" ";
            cur = cur->right;
        }
        else{
            Node* pre;
            pre = cur->left;
            while(pre->right != NULL && pre->right != cur){
                pre = pre->right;
            }
            if(pre->right == NULL){
                cout<<cur->data<<" ";
                pre->right = cur;
                cur = cur->left;
            }
            else{
                pre->right = NULL;
                cur = cur->right;
            }
        }
    }
    cout<<endl;
}
int main(){
    return 0;
}
