#include <iostream>

using namespace std;

struct Node{
     int data;
     Node* left, *right;
};

void inOrder(Node* root, int& mheight, int level){
    if(root == NULL) return;
    level += 1;
    if(level > mheight) mheight = level;
    inOrder(root->left, mheight, level);
    inOrder(root->right, mheight, level);
}

int height(Node* root){
    int mheight = 0, level = 0;
    inOrder(root, mheight, level);
    return mheight;
}

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
