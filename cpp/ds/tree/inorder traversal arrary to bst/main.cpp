#include <iostream>

using namespace std;

int n, arr[1005];


void f(int l, int h){
    if(h<l) return;
    int m = l + (h-l)/2;
    cout<<arr[m]<<" ";
    f(l, m-1);
    f(m+1, h);
}

int main()
{
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin >> n;
		for(int i = 0; i<n; i++)
			cin >> arr[i];
		f(0, n-1);
		cout<<endl;
	}
    return 0;

}
