
#include<bits/stdc++.h>
using namespace std;
struct Node
{
  int data;
  Node* left;
  Node* right;
};
Node* newNode(int data)
{
  Node* node = (Node*)malloc(sizeof(Node));
  node->data = data;
  node->left = NULL;
  node->right = NULL;
  return(node);
}
Node *buildTree(int a[],int b[],int str,int end);
int preIndex=0;
void printPostOrder(Node *root){
	if(root==NULL)
		return;
	printPostOrder(root->left);
	printPostOrder(root->right);
	cout<<root->data<<" ";
}
int main(){
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		preIndex=0;
		Node *root=NULL;
		int a[n],b[n];
		for(int i=0;i<n;i++)
			cin>>a[i];
		for(int i=0;i<n;i++)
			cin>>b[i];
		root=buildTree(a,b,0,n-1);
		printPostOrder(root);
		cout<<endl;
	}
}


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/*Complete the code here.
Node is as follows:
struct Node
{
  int data;
  Node* left;
  Node* right;
};
*/
int ri = -1;

int g(int a, int b, int in[], int pre[], int n){
    ri++;
    int rd = pre[ri];
    for(int i=a; i<=b; i++){
        if(in[i] == rd)
            return i;
    }
    return -1;
}

Node* f(int i, int j, int in[], int pre[], int n){
    if(j<i) return NULL;
    int k = g(i, j, in, pre, n);
    Node* root = new Node;
    root->data = in[k];
    root->left = f(i, k-1, in, pre, n);
    root->right = f(k+1, j, in, pre, n);
    return root;
}



Node* buildTree(int in[],int pre[], int i, int j){
    int n = j+1;
    ri = -1;
    return f(i, j, in, pre, n);
}







//int g(int a, int b, int in[], int pre[], int n){
//    for(int i=0; i<n ; i++){
//        int x = pre[i];
//        for(int j=a; j<=b; j++){
//            if(x == in[j])
//                return j;
//        }
//    }
//    return -1;
//}
//
//Node* f(int i, int j, int in[], int pre[], int n){
//    if(j<i) return NULL;
//    int k = g(i, j, in, pre, n);
//    Node* root = new Node;
//    root->data = in[k];
//    root->left = f(i, k-1, in, pre, n);
//    root->right = f(k+1, j, in, pre, n);
//    return root;
//}
//
//
//
//Node* buildTree(int in[],int pre[], int i, int j){
//    int n = j+1;
//    return f(i, j, in, pre, n);
//}
























