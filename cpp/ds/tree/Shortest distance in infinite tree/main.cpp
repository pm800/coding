#include <iostream>
using namespace std;

int main() {
	//code
	int t;
	cin>>t;
	while(t--){
	    int a,b;
	    cin>>a>>b;
	    int cnt=0;
	    while(a!=b){
	        if(a>b)
	        a/=2;
	        else
	        b/=2;
	        cnt++;
	    }
	    cout<<cnt<<endl;
	}
	return 0;
}
