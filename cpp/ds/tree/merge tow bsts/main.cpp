//Code by 1shubhamjoshi1
#include<bits/stdc++.h>
using namespace std;
struct Node {
int data;
struct Node * right, * left;
};
void insert(Node ** tree, int val)
{
    Node *temp = NULL;
    if(!(*tree))
    {
        temp = (Node *)malloc(sizeof(Node));
        temp->left = temp->right = NULL;
        temp->data = val;
        *tree = temp;
        return;
    }
    if(val < (*tree)->data)
    { insert(&(*tree)->left, val);}
    else if(val > (*tree)->data)
    { insert(&(*tree)->right, val);}
}
void merge(Node *root1,Node *root2);
int main()
{
    int T;
    cin>>T;
    while(T--)
    {
        Node *root1;Node *root2;
        Node *tmp;
        root1 = NULL;
        root2=NULL;
        int N;
        cin>>N;
        int M;
        cin>>M;
        for(int i=0;i<N;i++)
        {
            int k;
            cin>>k;
            insert(&root1, k);}
        for(int i=0;i<M;i++)
        {
            int k;
            cin>>k;
            insert(&root2, k);}
       merge(root1,root2);
        cout<<endl;
    }
}


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/* The structure of Node is
struct Node {
int data;
Node * right, * left;
};*/
/*You are required to complete below method */
void inOrder(Node* root, queue<int>& q){
	if(root==NULL) return;
	inOrder(root->left, q);
	q.push(root->data);
    inOrder(root->right, q);
}

void merge(Node *root1, Node *root2){
	queue<int> q1, q2;
	inOrder(root1, q1);
	inOrder(root2, q2);
    while(!q1.empty() && !q2.empty() ){
		int x, y;
		x = q1.front();
		y = q2.front();
        if(x<=y){
			cout<<x<<" ";
			q1.pop();
        }
        else{
			cout<<y<<" ";
			q2.pop();
        }
    }
    while(!q1.empty()){
        cout<<q1.front()<<" ";
        q1.pop();
    }
    while(!q2.empty()){
        cout<<q2.front()<<" ";
        q2.pop();
    }
}

























