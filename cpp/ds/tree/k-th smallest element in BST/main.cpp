#include <stdio.h>
#include <stdlib.h>
#include<iostream>
using namespace std;
#define ARRAY_SIZE(arr) sizeof(arr)/sizeof(arr[0])
typedef struct Node Node;
struct Node
{
    int data;
    int lCount;

    Node* left;
    Node* right;
};
int KthSmallestElement(Node *root, int k);
Node *insert_node(Node *root, Node* node)
{
    Node *pTraverse = root;
    Node *currentParent = root;

    while(pTraverse)
    {
        currentParent = pTraverse;

        if( node->data < pTraverse->data )
        {
            pTraverse->lCount++;
            pTraverse = pTraverse->left;
        }
        else
        {
            pTraverse = pTraverse->right;
        }
    }
    if( !root )
    {
        root = node;
    }
    else if( node->data < currentParent->data )
    {
        currentParent->left = node;
    }
    else
    {
        currentParent->right = node;
    }

    return root;
}
Node* binary_search_tree(Node *root, int keys[], int const size)
{
    int iterator;
    Node *new_node = NULL;

    for(iterator = 0; iterator < size; iterator++)
    {
        new_node = (Node *)malloc( sizeof(Node) );

        /* initialize */
        new_node->data   = keys[iterator];
        new_node->lCount = 0;
        new_node->left   = NULL;
        new_node->right  = NULL;

        /* insert into BST */
        root = insert_node(root, new_node);
    }

    return root;
}

 int main(void)
{
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
    int ele[n];
	for(int i=0;i<n;i++){
		cin>>ele[i];
	}
    int k;
	cin>>k;
    Node* root = NULL;
    root = binary_search_tree(root, ele, ARRAY_SIZE(ele));
        printf("%d",KthSmallestElement(root, k));
	}
    return 0;
}


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/*Complete the function below
Node is as follows:
struct Node
{
    int data;
    int lCount;

    Node* left;
    Node* right;
};*/

int ans;

void f(Node* root, int& k){
    if(root == NULL || k <= 0) return;
    f(root->left, k);
    if(k==1){
        k--;
        ans = root->data;
        return;
    }
    k--;
    f(root->right, k);
}


int KthSmallestElement(Node *root, int k){
    f(root, k);
    if(k>1){cout<<"here";exit(55);};
    return ans;
}


//void f(Node* root, int& k, int& x, bool & flag){
//    if(flag) return;
//    if(root == NULL) return;
//    f(root->left, k, x, flag);
//    if(flag) return;
//    if(k==1){
//        k--;
//        flag = 1;
//        x = root->data;
//        return;
//    }
////    cout<<k<<" ";
//    k--;
//    f(root->right, k, x, flag);
//}
//
//
//int KthSmallestElement(Node *root, int k){
//    bool flag = 0;
//    int x;
//    f(root, k, x, flag);
//    if(flag == 0){cout<<"here";exit(55);};
//    return x;
//}






















