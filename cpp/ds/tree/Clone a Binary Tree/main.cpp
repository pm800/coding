#include <bits/stdc++.h>
using namespace std;
/* A binary tree node has data, pointer to left child
   and a pointer to right child */
struct Node
{
    int data;
    struct Node* left;
    struct Node* right;
    struct Node *random;
};
Node *cloneTree(Node *);
int printInorder(Node* a,Node *b)
{
    if ((a==NULL and b==NULL) or (a->random==NULL and b->random==NULL))
        return 1;
    if(a->random->data == b->random->data and printInorder(a->left,b->left) and printInorder(a->right,b->right))
        return 1;
    return false;
}
/* Helper function that allocates a new node with the
   given data and NULL left and right pointers. */
struct Node* newNode(int data)
{
  struct Node* node = new Node;
  node->data = data;
  node->left = NULL;
  node->right = NULL;
  node->random=NULL;
  return(node);
}
/* Computes the number of nodes in a tree. */
void inorder(Node *root)
{
    if (root == NULL)
       return;
    inorder(root->left);
    cout << root->data << " ";
    inorder(root->right);
}
/* Driver program to test size function*/
int main()
{
  int t;
  scanf("%d", &t);
  while (t--)
  {
     map<int, Node*> m;
     int n;
     scanf("%d",&n);
     struct Node *root = NULL;
     struct Node *child;
     while (n--)
     {
        Node *parent;
        char lr;
        int n1, n2;
        scanf("%d %d %c", &n1, &n2, &lr);
        if (m.find(n1) == m.end())
        {
           parent = newNode(n1);
           m[n1] = parent;
           if (root == NULL)
             root = parent;
        }
        else
           parent = m[n1];
        child = newNode(n2);
        if (lr == 'L'){
          parent->left = child;
           m[n2]  = child;
        }
        else if(lr=='R'){
          parent->right = child;
           m[n2]  = child;
        }
        else{
          parent->random = m[n2];
        }
     }
     Node *t = cloneTree(root);
      if(t==root)
        cout<<0<<endl;
     else
     cout<<printInorder(root,t);
     cout<<endl;
  }
  return 0;
}
Node* cloneTree(Node* tree);


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/* A binary tree node has data, pointer to left child
   and a pointer to right child
struct Node
{
    int data;
    Node* left;
    Node* right;
    Node *random;
};
*/
/* The function should clone the passed tree and return
   root of the cloned tree */

void copyTree(Node* croot, Node* root, vector<Node*>& lr, vector<Node*>& lc){
    croot->data = root->data;
    lr.push_back(root);
    lc.push_back(croot);
    if(root->left != NULL){
        bool flag = 0;
        int i;
        for(i=0; i<lr.size(); i++){
            if(lr[i] == root->left){ flag = 1; break;}
        }
        if(flag ==0){
            croot->left = new Node;
            copyTree(croot->left, root->left, lr, lc);
        }
        else{
            croot->left = lc[i];
        }
    }
    if(root->right != NULL){
        bool flag = 0;
        int i;
        for(i=0; i<lr.size(); i++){
            if(lr[i] == root->right){ flag = 1; break;}
        }
        if(flag ==0){
            croot->right = new Node;
            copyTree(croot->right, root->right, lr, lc);
        }
        else{
            croot->right = lc[i];
        }
    }
    if(root->random != NULL){
        bool flag = 0;
        int i;
        for(i=0; i<lr.size(); i++){
            if(lr[i] == root->random){ flag = 1; break;}
        }
        if(flag ==0){
            croot->random = new Node;
            copyTree(croot->random, root->random, lr, lc);
        }
        else{
            croot->random = lc[i];
        }
    }
}

Node* cloneTree(Node* root){
    vector<Node*> lr, lc;
    if(root == NULL) return NULL;
    Node* croot = new Node;
    copyTree(croot, root, lr, lc);
    return croot;
}
