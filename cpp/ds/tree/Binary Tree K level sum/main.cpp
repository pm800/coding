#include <iostream>

using namespace std;

int f(string s, int k){
    int sm = 0, level = -1;
    int n = s.size();
    int i=0;
    while(i<n){
        if(s[i]=='('){
			level++; i++;continue;
		}
		else if(s[i]==')'){
			level--;
			i++;continue;
		}
		else if(s[i]!='(' && s[i]!=')'){
            if(level ==k){
				int x = 0;
				while(s[i]!='(' && s[i]!=')'){
                    int y = s[i] - '0';
                    x = x*10 + y;
                    i++;
				}
				sm += x;
            }
            else{
				i++;
            }
		}
    }
    return sm;
}


int main(){
    int tcs;
    cin >>tcs;
    while(tcs--){
        int k;
        cin >>k;
        string s;
        cin >>s;
        cout<<f(s, k)<<endl;
    }
    return 0;
}
