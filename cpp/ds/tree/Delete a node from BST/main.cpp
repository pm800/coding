//Code by 1shubhamjoshi1
#include<bits/stdc++.h>
using namespace std;

struct Node {
	int data;
	struct Node * right, * left;
};

void insert(Node ** tree, int val){
    if(!(*tree))
    {
		Node* temp;
        temp = new Node;
        temp->data = val;
        *tree = temp;
        return;
    }
    if(val < (*tree)->data)
    { insert(&((*tree)->left), val);}
    else if(val > (*tree)->data)
    { insert(&((*tree)->right), val);}
}

void inorder(Node *root){
    if(root==NULL)
        return;
    inorder(root->left);
    cout<<root->data<<" ";
    inorder(root->right);
}

Node * deleteNode(Node *root,  int );

int main()
{
    int T;
    cin>>T;
    while(T--)
    {
        Node *root;
        Node *tmp;
        root = NULL;
        int N;
        cin>>N;
        for(int i=0;i<N;i++)
        {
            int k;
            cin>>k;
            insert(&root, k);}
        int r;
        cin>>r;
        root = deleteNode(root,r);
        inorder(root);
        cout<<endl;
    }
}

/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/* The structure of a BST Node is as follows:
struct Node {
  int data;
  Node * right, * left;
}; */

void f(Node* & root, int x);

Node* h(Node* root){
    Node* is = root->right;
    while(is->left){
		is = is->left;
    }
    return is;
}


Node* g(Node* & root){
    if(root->left == NULL){
        Node* temp = root->right;
        delete root;
        root = temp;
    }
    else if(root->right == NULL){
        Node* temp = root->left;
        delete root;
        root = temp;
    }
    else{
        Node* is = h(root);
        root->data = is->data;
        f(root->right, is->data);
    }
}

void f(Node* & root, int x){
    if(root == NULL) return;
    if(x < root->data){
		f(root->left, x);
    }
    else if(x > root->data){
		f(root->right, x);
    }
    else{
        g(root);
    }
}

Node * deleteNode(Node *root,  int x){
    f(root, x);
    return root;
}








//Node* h(Node* root){
//    Node* is = root->right;
//    while(is->left){
//		is = is->left;
//    }
//    return is;
//}
//
//
//Node* g(Node* root){
//    if(root->left == NULL){
//        Node* temp = root->right;
//        delete root;
//        return temp;
//    }
//    else if(root->right == NULL){
//        Node* temp = root->left;
//        delete root;
//        return temp;
//    }
//    else{
//        Node* is = h(root);
//        root->data = is->data;
//        root->right = deleteNode(root->right, is->data);
//    }
//}
//
//Node * deleteNode(Node *root,  int x){
//    if(root == NULL) return root;
//    if(x < root->data){
//		root->left = deleteNode(root->left, x);
//    }
//    else if(x > root->data){
//		root->right = deleteNode(root->right, x);
//    }
//    else{
//        root = g(root);
//    }
//    return root;
//}






















