#include<bits/stdc++.h>
using namespace std;
struct Node
{
    int data ;
    struct Node * left, * right ;
};
int flag=0;
struct Node * newNode(int data)
{
    struct Node * node = new Node;
    node->data = data;
    node->left = node->right = NULL;
    return (node);
}
void insert(Node *root,int a,int a1,char lr){
		if(root==NULL)
			return;
		if(root->data==a)
		{
			switch(lr){
					case 'L':root->left=newNode(a1);
					break;
					case 'R':root->right=newNode(a1);
					break;
				}
		}
		else {
			insert(root->left,a,a1,lr);
			insert(root->right,a,a1,lr);
		}
	}
int btWithKleaves(struct Node *ptr,int k);
	int main(){
		int t;
		char lr;
		cin>>t;
		while(t-->0){
			int n,k;
			int a,a1;
			cin>>n>>k;
			Node* root=NULL;
			if(n==0||n==1)
			{
				cout<<"-1"<<endl;
				continue;
			}
			for(int i=0;i<n;i++){
				cin>>a>>a1;
				fflush(stdin);
				cin>>lr;
				if(root==NULL){
					root=newNode(a);
					switch(lr){
						case 'L':root->left=newNode(a1);
						break;
						case 'R':root->right=newNode(a1);
						break;
					}
				}
				else{
					insert(root,a,a1,lr);
				}
			}
			btWithKleaves(root,k);
		}
	}


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/*The Node is defined as follows:
struct Node
{
    int data ;
    struct Node * left, * right ;
};*/
/*You are required to complete below method */

int k;
bool flg = 0;

int f(Node* root){
    if(root == NULL) return 0;
    int leafs = 0;
    leafs += f(root->left);
    leafs += f(root->right);
    if(leafs == k){
		cout<<root->data<<" "; flg = 1;
    }
    if(leafs == 0)
		return 1;
	else
		return leafs;
}

int btWithKleaves(Node *root, int K){
	flg = 0;
	k = K;
	if(k == 0){
		cout<<-1<<endl; return 0;
	}
	f(root);
	if(flg == 0)
		cout<< -1;
	cout<<endl;
	return 0;
}























