#include <iostream>
#define N 17
#include <vector>
using namespace std;

vector<int> t1[N];
vector<int> t2[N];

int f(int a){
    if(t1[a].size() != t2[a].size()) return 0;
    int n = t1[a].size();
    for(int i=0; i<n; i++){
        if( t1[a][i] != t2[a][n-1-i] ) return 0;
        else{
            if( f(t1[a][i]) == 0 ) return 0;
        }
    }
    return 1;
}


int main(){
    int tcs;
    cin>>tcs;
    while(tcs--){
        int n, e;
        cin >>n>>e;
        for(int i=0; i<N; i++){
            t1[i] = {}; t2[i] = {};
        }
        for(int i=0; i<e; i++){
            int a, b;
            cin >>a>>b;
            if(b==1){cout<<"here error";exit(55);};
            t1[a].push_back(b);
        }
        for(int i=0; i<e; i++){
            int a, b;
            cin >>a>>b;
            if(b==1){cout<<"here error";exit(55);};
            t2[a].push_back(b);
        }
        cout<<f(1)<<endl;
    }
    return 0;
}
