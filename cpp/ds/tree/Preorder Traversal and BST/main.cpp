#include <iostream>
#include <algorithm>

using namespace std;

int pre[1005];


int f(int i, int j){
    if(j<i) return 1;
    int root = pre[i];
    int k = i+1;
    bool flag = 0;
    while(k <= j){
		if(pre[k] >= root){
			flag = 1;
			break;
		}
		k++;
    }
    if(flag == 1){
		while(k <= j){
			if(pre[k] <= root)
				return 0;
			k++;
		}
		if( f(i+1, k-1) == 0  ) return 0;
		if( f(k, j) == 0  ) return 0;
    }
    else{
		if( f(i+1, j) == 0  ) return 0;
    }
    return 1;
}

int main()
{
	int tcs;
	cin >> tcs;
	while(tcs--){
		int n;
		cin >> n;
		for(int i=0; i<n; i++){
			cin >> pre[i];
		}
        cout<< f(0, n-1)<< endl;
	}
    return 0;
}



//
//#include <iostream>
//#include <algorithm>
//
//using namespace std;
//
//
//int n, p = 0;
//int pre[1005], in[1005];
//
//int f(int a, int b, int x){
//	for(int i=a; i<=b; i++)
//		if(in[i] == x)
//			return i;
//	return -1;
//}
//
//bool g(int i, int j){
//    if(j<i) return 1;
//    int k = f(i, j, pre[p]); p++;
//    if(k == -1) return 0;
//    if(  g(i, k-1) == 0  ) return 0;
//    if(  g(k+1, j) == 0  ) return 0;
//    return 1;
//}
//
//
//int main()
//{
//	int tcs;
//	cin >> tcs;
//	while(tcs--){
//		p = 0;
//		cin >> n;
//		for(int i=0; i<n; i++){
//			int x;
//			cin >> x;
//			pre[i]= in[i] = x;
//		}
//        sort(in, in+n);
//        cout<< g(0, n-1)<< endl;
//	}
//    return 0;
//}
