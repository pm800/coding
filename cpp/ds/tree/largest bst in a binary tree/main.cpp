#include <bits/stdc++.h>
using namespace std;
/* A binary tree node has data, pointer to left child
   and a pointer to right child */
struct Node
{
    int data;
    struct Node* left;
    struct Node* right;
};
/* Helper function that allocates a new node with the
   given data and NULL left and right pointers. */
struct Node* newNode(int data)
{
  struct Node* node = new Node;
  node->data = data;
  node->left = NULL;
  node->right = NULL;
  return(node);
}
void inorder(Node *root)
{
    if (root == NULL)
       return;
    inorder(root->left);
    cout << root->data << " ";
    inorder(root->right);
}
int largestBst(Node *root);
/* Driver program to test size function*/
int main()
{
  int t;
  scanf("%d", &t);
  while (t--)
  {
     map<int, Node*> m;
     int n;
     scanf("%d",&n);
     struct Node *root = NULL;
     struct Node *child;
     while (n--)
     {
        Node *parent;
        char lr;
        int n1, n2;
        scanf("%d %d %c", &n1, &n2, &lr);
        if (m.find(n1) == m.end())
        {
           parent = newNode(n1);
           m[n1] = parent;
           if (root == NULL)
             root = parent;
        }
        else
           parent = m[n1];
        child = newNode(n2);
        if (lr == 'L')
          parent->left = child;
        else
          parent->right = child;
        m[n2]  = child;
     }
    cout<<largestBst(root)<< endl;
  }
  return 0;
}


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/* Tree node structure  used in the program
 struct Node
 {
     int data;
     struct Node* left, *right;
};*/
/*You are required to complete this method */

struct Info{
	int s, l, sz;
};

int msize = 0;

Info f(Node* root){
    if(!root) return {INT_MIN, INT_MAX, 0};
    Info ld, rd;
    ld = f(root->left);
    if(ld.sz == -1) return {-1, -1, -1};
    rd = f(root->right);
    if(rd.sz == -1) return {-1, -1, -1};
    int l, s, sz;
    l = s = root->data;
    sz = 1;
    if(ld.sz != 0){
    	if(root->data <= ld.l) return {-1,-1,-1};
		sz += ld.sz;
		l = max(l, ld.l);
		s = min(s, ld.s);
    }
    if(rd.sz != 0){
		if(root->data >= rd.s) return {-1,-1,-1};
		sz += rd.sz;
		l = max(l, rd.l);
		s = min(s, rd.s);
    }
    if(sz > msize)
		msize = sz;
	return {s, l, sz};
}



int largestBst(Node *root){
	msize = 0;
	f(root);
	return msize;
}

























