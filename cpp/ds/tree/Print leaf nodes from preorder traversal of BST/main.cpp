#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

int n, arr[105];

void f(){
    stack<int> stk;
    stk.push(arr[0]);
    for(int i=1; i<n; i++){
        if(arr[i] < stk.top() )
			stk.push(arr[i]);
		else{
			bool found = false;
			int ans = stk.top();
			stk.pop();
			while(!stk.empty() && stk.top() < arr[i]){
				found = true;
				stk.pop();
			}
            if(found)
				cout<<ans<<" ";
			stk.push(arr[i]);
		}
    }
    if(!stk.empty())
		cout<<stk.top();
	cout<<endl;
	return;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		f();
	}
	return 0;
}


