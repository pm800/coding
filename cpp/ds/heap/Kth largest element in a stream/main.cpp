#include <iostream>
#include <vector>
#include <queue>

using namespace std;

int main()
{
	int tcs;
	cin >>tcs;
	while(tcs--){
		priority_queue<int, vector<int>, greater<int>> minh;
		int n, k;
		cin>>k>>n;
		int x = k-1;
		while(x--){

			int y;
			cin>>y;
			minh.push(y);
			cout<< -1<<" ";
		}
		cin>>x;
		minh.push(x);
		cout<<minh.top()<<" ";
		n = n-k;
		while(n--){
            cin >> x;
			if(x>minh.top()){
				minh.push(x);
				minh.pop();
			}
			cout<<minh.top()<<" ";
		}
		cout<<endl;
	}
    return 0;
}
