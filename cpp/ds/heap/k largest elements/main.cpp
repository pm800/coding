#include <iostream>

using namespace std;

int arr[105], n, k;


void inst(int& hsize, int x){
	arr[hsize] = x;
	int c = hsize;
	while(c != 0){
		int p = (c-1)/2;
		if(arr[p] >= arr[c])
			break;
		int temp = arr[c];
		arr[c] = arr[p];
		arr[p] = temp;
		c = p;
	}
	hsize++;
}

void maxHeapify(int i, int hsize){
	int c = i;
	while(c<hsize){
		int l, r, b, temp;
		l = 2*c+1;
		r = 2*c+2;
		b = c;
		if(l<hsize){
			if(arr[l] > arr[b])
				b = l;
		}
		if(r<hsize){
			if(arr[r] > arr[b])
				b = r;
		}
		if(b == c)
			break;
		temp = arr[c];
		arr[c] = arr[b];
		arr[b] = temp;
		c = b;
	}

}

void builMaxHeap(){//build maxheap from a array
	int i = (n-1)/2;
	while(i>=0){
		maxHeapify(i, n);
		i--;
	}
}


void extractMax(int& hsize){
	if(hsize == 0)
		exit(55);
	arr[0] = arr[hsize-1];
	maxHeapify(0, hsize);
	hsize--;
}

void f(){
	builMaxHeap();
	int hsize = n;
	while(k--){
		cout<<arr[0]<<" ";
		extractMax(hsize);
	}
	cout<<endl;
}


int main()
{
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n>>k;
		for(int i=0; i<n; i++)
			cin>>arr[i];
        f();

	}
    return 0;
}
