#include <iostream>     // cout
#include <algorithm>    // make_heap, pop_heap, push_heap, sort_heap
#include <vector>       // vector
using namespace std;

void show(vector<int> v){
	for(int i: v)
		cout<<i<<" ";
	cout<<endl;
}

bool comp(int a, int b){
	return a >= b;
}

int main () {
	int myints[] = {10,20,30,5,15};
	vector<int> v(myints,myints+5);

	make_heap (v.begin(),v.end(), comp);
	show(v);


	pop_heap (v.begin(),v.end());
	v.pop_back();


	v.push_back(99);
	push_heap (v.begin(),v.end());


	sort_heap (v.begin(),v.end());


  return 0;
}
