#include<stdio.h>
#include<stdlib.h>
#include<iostream>
using namespace std;
/* Linked list Node */
struct Node
{
    int data;
    struct Node* next;
};
static void reverse(struct Node** head_ref)
{
    struct Node* prev   = NULL;
    struct Node* current = *head_ref;
    struct Node* next;
    while (current != NULL)
    {
        next  = current->next;
        current->next = prev;
        prev = current;
        current = next;
    }
    *head_ref = prev;
}

/* Function to create a new Node with given data */
struct Node *newNode(int data)
{
    struct Node *new_Node = (struct Node *) malloc(sizeof(struct Node));
    new_Node->data = data;
    new_Node->next = NULL;
    return new_Node;
}

/* Function to insert a Node at the beginning of the Doubly Linked List */
void push(struct Node** head_ref, int new_data)
{
    /* allocate Node */
    struct Node* new_Node = newNode(new_data);

    /* link the old list off the new Node */
    new_Node->next = (*head_ref);

    /* move the head to point to the new Node */
    (*head_ref)    = new_Node;
}


// A utility function to print a linked list
void printList(struct Node *Node)
{
    while(Node != NULL)
    {
        printf("%d ", Node->data);
        Node = Node->next;
    }
    printf("");
}
void freeList(struct Node *Node)
{
	struct Node* temp;
    while(Node != NULL)
    {

        temp=Node;
        Node = Node->next;
        free(temp);
    }

}
Node * mergeKList(Node *arr[],int );
/* Driver program to test above function */
int main(void)
{
   int t,n,m,i,x;
   cin>>t;
   while(t--)
   {

	   int N;
	   cin>>N;
       struct Node **arr = new Node *[N];
       for(int j=0;j<N;j++){
	    cin>>n;
		    for(i=0;i<n;i++)
	    	{
			cin>>x;
			push(&arr[j], x);
			}
			reverse(&arr[j]);
		   // printList(arr[j]);
   		}

   		Node *res = mergeKList(arr,N);
		printList(res);


   }
   return 0;
}


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/*Linked list Node structure
struct Node
{
    int data;
    Node* next;
};*/
/* arr[] is an array of pointers to heads of linked lists
  and N is size of arr[]  */



Node* SortedMerge(Node* a,   Node* b){
	Node* res = new Node;
	Node* curr = res;
	while(1){
		if(!a){
			curr->next = b;
			break;
		}
		if(!b){
			curr->next = a;
			break;
		}
		if(a->data <= b->data){
			curr->next = a;
            a = a->next;
		}
		else{
			curr->next = b;
            b= b->next;
		}
		curr = curr->next;
	}
	return res->next;
}

void f(Node* arr[], int l){
	while(l > 0){
		int s = 0;
		while(s < l){
			arr[s] = SortedMerge(arr[s], arr[l]);
			s++; l--;
		}
	}
}


Node * mergeKList(Node *arr[], int n){
	f(arr, n-1);
	return arr[0];
}




//
//#include <algorithm>
//
//bool comp(Node* n1, Node* n2){
//	return n1->data >= n2->data;
//}
//
//Node * mergeKList(Node *arr[], int n){
//	Node* harr[n];
//	int hsize = 0;
//	for(int i=0; i<n; i++){
//		if(arr[i]){
//			harr[hsize] = arr[i];
//			hsize++;
//		}
//	}
//	make_heap(harr, harr+hsize, comp);
//	Node* mlist = new Node;
//	Node* curr = mlist;
//	while(hsize > 0){
//        Node* root = harr[0];
//        curr->next = root;
//        curr = curr->next;
//        pop_heap(harr, harr+hsize, comp);
//        hsize--;
//        if(root->next){
//			hsize++;
//			harr[hsize-1] = root->next;
//			push_heap(harr, harr+hsize, comp);
//        }
//	}
//	return mlist->next;
//}




















