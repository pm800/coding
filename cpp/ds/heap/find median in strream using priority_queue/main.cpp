#include <iostream>
#include <algorithm>
#include <queue>
#include <vector>
using namespace std;

priority_queue<int, vector<int>, greater<int>> minh;
priority_queue<int> maxh;
int  mid;

void f(int x){
	if(x >= mid){
		minh.push(x);
		if(minh.size() == maxh.size() + 2){
			maxh.push(mid);
			mid = minh.top();
			minh.pop();
		}
	}
	else{
		maxh.push(x);
		if(maxh.size() == minh.size() + 2){
			minh.push(mid);
			mid = maxh.top();
			maxh.pop();
		}
	};
	int ans;
	if(minh.size() == maxh.size())
		ans = mid;
	else if(minh.size() > maxh.size())
		ans = ( mid + minh.top() )/2;
	else
		ans = ( mid + maxh.top() )/2;
	cout<<ans<<endl;
}

int main(){
	int n;
	cin >> n;
    cin >> mid;
    cout<<mid<<endl;
    n = n-1;
    while(n--){
        int x;
        cin >> x;
        f(x);
    }
    return 0;

}
