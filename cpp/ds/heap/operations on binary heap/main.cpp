//Initial Template for C++
#include<bits/stdc++.h>
using namespace std;
typedef long long int ll;
void swap(int *x, int *y);

class MinHeap
{ public:
    int *harr;
    int capacity;
    int heap_size;

    MinHeap(int capacity);


    void MinHeapify(int );

    int parent(int i) { return (i-1)/2; }


    int left(int i) { return (2*i + 1); }


    int right(int i) { return (2*i + 2); }


    int extractMin();


    void decreaseKey(int i, int new_val);


    int getMin() ;


    void deleteKey(int i);


    void insertKey(int k);
};
//Position this line where user code will be pasted.
int main()
{
   int t;
   cin>>t;
   while(t--)
   {
     ll a;
     cin>>a;
     MinHeap h(a);
    for(ll i=0;i<a;i++)
      {
        int c;
        int  n;
        cin>>c;
        if(c==1)
            {  cin>>n;

               h.insertKey(n);
             }
        if(c==2)
           {  cin>>n;
              h.deleteKey(n);
	    }
        if(c==3)
              {
               cout<<h.extractMin()<<" ";
               }

      }
       cout<<endl;
    delete h.harr;
     h.harr=NULL;

   }
  return 0;
  }


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/*The structure of the class is
class MinHeap
{
    int *harr;
    int capacity;
    int heap_size;
public:
MinHeap(int cap=100) {heap_size = 0; capacity = cap; harr = new int[cap];}
    int extractMin();
    void deleteKey(int i);
    void insertKey(int k);
};*/
/* Removes min element from min heap and returns it */





MinHeap::MinHeap(int cap){
	capacity = cap;
	heap_size = 0;
	harr = new int [capacity];
}


int MinHeap ::  extractMin(){
	if(heap_size == 0) return -1;
	int temp = harr[0];
	deleteKey(0);
	return temp;
}
/* Removes element from position x in the min heap  */
void MinHeap :: deleteKey(int i){
	if(heap_size == 0 || i >= heap_size) return;
//	if(heap_size == 0 || i >= heap_size) {cout<<"\n aaa \n";exit(55);};
    harr[i] = harr[heap_size - 1];
    heap_size--;
    MinHeapify(i);
}
/* Inserts an element at position x into the min heap*/
void MinHeap ::insertKey(int k){
	heap_size++;
	harr[heap_size - 1] = k;
	int c = heap_size -1;
	while(c > 0){
		int p = parent(c);
		if(harr[p] <= harr[c])
			break;
		swap(harr[p], harr[c]);
		c = p;
	}
}


void MinHeap::MinHeapify(int i){
	int l, r, s;
	l = left(i);
	r = right(i);
	s = i;
	if(l < heap_size && harr[l] < harr[s])
		s = l;
	if(r < heap_size && harr[r] < harr[s])
		s = r;
	if( s != i){
		swap(harr[i], harr[s]);
		MinHeapify(s);
	}
}















