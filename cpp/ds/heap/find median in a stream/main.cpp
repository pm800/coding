#include <iostream>

using namespace std;

class MinHeap{

	public:
	int arr[100009];
	int hsize = 0;

	int root() {return arr[0];}

	void inst(int x){
		arr[hsize] = x;
		int c = hsize;
		while(c != 0){
			int p = (c-1)/2;
			if(arr[p] <= arr[c])
				break;
			int temp = arr[c];
			arr[c] = arr[p];
			arr[p] = temp;
			c = p;
		}
		hsize++;
	}

	void minHeapify(int i){
		int c = i;
		while(c<hsize){
			int l, r, s, temp;
			l = 2*c+1;
			r = 2*c+2;
			s = c;
			if(l<hsize){
				if(arr[l] < arr[s])
					s = l;
			}
			if(r<hsize){
				if(arr[r] < arr[s])
					s = r;
			}
			if(s == c)
				break;
			temp = arr[c];
			arr[c] = arr[s];
			arr[s] = temp;
			c = s;
		}

	}

	void extractMin(){
		if(hsize == 0)
			exit(55);
		arr[0] = arr[hsize-1];
		minHeapify(0);
		hsize--;
	}

};


class MaxHeap{
	public:
	int arr[100009];
	int hsize = 0;
	int root() {return arr[0];}

	void inst(int x){
		arr[hsize] = x;
		int c = hsize;
		while(c != 0){
			int p = (c-1)/2;
			if(arr[p] >= arr[c])
				break;
			int temp = arr[c];
			arr[c] = arr[p];
			arr[p] = temp;
			c = p;
		}
		hsize++;
	}

	void maxHeapify(int i){
		int c = i;
		while(c<hsize){
			int l, r, b, temp;
			l = 2*c+1;
			r = 2*c+2;
			b = c;
			if(l<hsize){
				if(arr[l] > arr[b])
					b = l;
			}
			if(r<hsize){
				if(arr[r] > arr[b])
					b = r;
			}
			if(b == c)
				break;
			temp = arr[c];
			arr[c] = arr[b];
			arr[b] = temp;
			c = b;
		}

	}

	void extractMax(){
		if(hsize == 0)
			exit(55);
		arr[0] = arr[hsize-1];
		maxHeapify(0);
		hsize--;
	}

};

MinHeap minh;
MaxHeap maxh;
int  mid;

void f(int x){
	if(x >= mid){
		minh.inst(x);
		if(minh.hsize == maxh.hsize + 2){
			maxh.inst(mid);
			mid = minh.root();
			minh.extractMin();
		}
	}
	else{
		maxh.inst(x);
		if(maxh.hsize == minh.hsize + 2){
			minh.inst(mid);
			mid = maxh.root();
			maxh.extractMax();
		}
	};
	int ans;
	if(minh.hsize == maxh.hsize)
		ans = mid;
	else if(minh.hsize > maxh.hsize)
		ans = ( mid + minh.root() )/2;
	else
		ans = ( mid + maxh.root() )/2;
	cout<<ans<<endl;
}

int main(){
	int n;
	cin >> n;
    cin >> mid;
    cout<<mid<<endl;
    n = n-1;
    while(n--){
        int x;
        cin >> x;
        f(x);
    }
    return 0;

}
