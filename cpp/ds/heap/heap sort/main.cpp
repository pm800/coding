#include <iostream>
using namespace std;

int arr[1005], n;


void maxHeapify(int i, int hsize){
	int c = i;
	while(c<hsize){
		int l, r, b, temp;
		l = 2*c+1;
		r = 2*c+2;
		b = c;
		if(l<hsize){
			if(arr[l] > arr[b])
				b = l;
		}
		if(r<hsize){
			if(arr[r] > arr[b])
				b = r;
		}
		if(b == c)
			break;
		temp = arr[c];
		arr[c] = arr[b];
		arr[b] = temp;
		c = b;
	}

}

void builMaxHeap(){//build maxheap from a array
	int i = (n-1)/2;
	while(i>=0){
		maxHeapify(i, n);
		i--;
	}
}
void heapSort(int arr[], int n){
    builMaxHeap();
    for (int i=n-1; i>=0; i--){
        swap(arr[0], arr[i]);
        maxHeapify(0, i);
    }
}

int main(){
	return 0;
}
