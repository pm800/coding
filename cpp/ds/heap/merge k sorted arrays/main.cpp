#include<bits/stdc++.h>
#define N 11
using namespace std;
int *mergeKArrays(int arr[][N],int k);
void printArray(int arr[], int size)
{
for (int i=0; i < size; i++)
	cout << arr[i] << " ";
}
int main()
{
	int t;
	cin>>t;
	while(t--){
	    int k;
	    cin>>k;
	    int arr[N][N];
	    for(int i=0; i<k; i++){
	        for(int j=0; j<k; j++)
	        {
	            cin>>arr[i][j];
	        }
	    }
	int *output = mergeKArrays(arr, k);
	printArray(output, k*k);
	cout<<endl;
}
	return 0;
}


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/





int* marr = new int[1];
struct S{
	int val, i, j;
};
S heap[N];

bool comp(S s1, S s2){
	return s1.val >= s2.val;
}

int *mergeKArrays(int arr[][N], int k){
	delete [] marr;
	marr = new int [k*k];
	for(int i=0; i<k; i++){
		heap[i] = {arr[i][0], i, 0};
	}
	make_heap(heap, heap+k, comp);
    int p=0;
	while(p < k*k){
        marr[p] = heap[0].val;
        p++;
        int i = heap[0].i;
        int j = heap[0].j;
        pop_heap(heap, heap+k, comp);
        if(j <= k-2){
			heap[k-1] = {arr[i][j+1], i, j+1};
			push_heap(heap, heap+k, comp);
        }
        else{
        	heap[k-1] = {INT_MAX, -1, -1};
			push_heap(heap, heap+k, comp);
        }
	}
	return marr;
}








// your task is tocomplete this function
// function should return an pointer to output array int*
// of size k*k
//
//int first[N];
//int* marr = new int[1];
//
//bool f(int arr[][N], int k, int* marr, int& p){
//    if(p >= k*k)
//		return 1;
//	int s = INT_MAX;
//	int si;
//	for(int i=0; i<k; i++){
//		if(first[i] >= k)
//			continue;
//        int j = first[i];
//        if(arr[i][j] < s ){
//			s = arr[i][j]; si = i;
//        }
//	}
//	marr[p] = s;
//	p++;
//	first[si]++;
//	return 0;
//}
//
//int *mergeKArrays(int arr[][N], int k){
//	delete [] marr;
//	marr = new int [k*k];
//	fill(first, first+k, 0);
//	int p = 0;
//    while(1){
//		if( f(arr, k, marr, p) == 1 )
//			break;
//    }
//    return marr;
//}























