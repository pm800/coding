#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl


using namespace std;

int n,fre[30];
string s;

struct Node{
	int f;
	char k;
	Node *left = NULL, *right = NULL;
};

class Comp{
public:
	bool operator() (Node* a, Node* b){
		return a->f > b->f;
	}
};

void g(Node* root, string &v){
    if(!root->left && !root->right){
		cout<< v<< " ";
		return;
    }
    if(root->left){
		v.push_back('0');
		g(root->left, v);
		v.pop_back();
    }
    if(root->right){
		v.push_back('1');
		g(root->right, v);
		v.pop_back();
    }
    return;
}

void f(){
	priority_queue<Node*, vector<Node*>, Comp> q;
    for(int i=0; i<n; i++){
		Node* tmp = new Node;
		tmp->f = fre[i];
		tmp->k = s[i];
		q.push(tmp);
    }
    while(q.size() > 1){
		Node *x, *y;
        x = q.top(); q.pop();
        y = q.top(); q.pop();
        Node* tmp = new Node;
        tmp->f = x->f + y->f;
        tmp->k = '4';
        if(x->f <= y->f){
			tmp->left = x;
			tmp->right = y;
        }
        else{
			tmp->left = y;
			tmp->right = x;
        }
        q.push( tmp);
    }
    Node* root = q.top();
    string v;
    g(root, v);
    cout<<endl;
	return;
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>s;
		n = s.size();
		for(int i=0; i<n; i++)
			cin>>fre[i];
		f();
	}
	return 0;
}


