#include <iostream>
#include <algorithm>

using namespace std;

class MinHeap{

	public:
	int arr[100009];
	int hsize = 0;

	static bool comp(const int& a, const int& b){
		return a <= b;
	}

	int root() {return arr[0];}

	void inst(int x){
		hsize++;
		arr[hsize-1] = x;
		push_heap(arr, arr+hsize, comp);
	}

	void extractMin(){
		if(hsize == 0) exit(55);
		pop_heap(arr, arr+hsize, comp);
		hsize--;
	}

};


class MaxHeap{
	public:
	int arr[100009];
	int hsize = 0;
	int root() {return arr[0];}

	void inst(int x){
		hsize++;
        arr[hsize-1] = x;
        push_heap(arr, arr+hsize);
	}


	void extractMax(){
		if(hsize == 0) exit(55);
		pop_heap(arr, arr+hsize);
		hsize--;
	}

};

MinHeap minh;
MaxHeap maxh;
int  mid;

void f(int x){
	if(x >= mid){
		minh.inst(x);
		if(minh.hsize == maxh.hsize + 2){
			maxh.inst(mid);
			mid = minh.root();
			minh.extractMin();
		}
	}
	else{
		maxh.inst(x);
		if(maxh.hsize == minh.hsize + 2){
			minh.inst(mid);
			mid = maxh.root();
			maxh.extractMax();
		}
	};
	int ans;
	if(minh.hsize == maxh.hsize)
		ans = mid;
	else if(minh.hsize > maxh.hsize)
		ans = ( mid + minh.root() )/2;
	else
		ans = ( mid + maxh.root() )/2;
	cout<<ans<<endl;
}

int main(){
	int n;
	cin >> n;
    cin >> mid;
    cout<<mid<<endl;
    n = n-1;
    while(n--){
        int x;
        cin >> x;
        f(x);
    }
    return 0;

}
