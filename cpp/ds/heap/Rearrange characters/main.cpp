#include <iostream>

using namespace std;


int arr[26];

bool f(string s){
	fill(arr, arr+26, 0);
	int maxm = 0;
	for(char c: s){
		arr[c-'a']++;
		if(arr[c-'a'] > maxm)
			maxm = arr[c-'a'];
	}
	if(maxm > s.size()/2)
		return 0;
	return 1;
}

int main()
{
	int tcs;
	cin >> tcs;
	while(tcs--){
		string s;
		cin >>s;
		cout<<f(s)<<endl;
	}
    return 0;
}
