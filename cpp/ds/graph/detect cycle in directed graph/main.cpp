#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl

using namespace std;

class Graph
{
    int V;
    list<int> *adj;
public :
    Graph(int V);
    void addEdge(int v,int w);
    bool isCyclic();
};
Graph::Graph(int V)
{
    this->V = V;
    adj = new list<int>[V];
}
void Graph::addEdge(int v,int w)
{
    adj[v].push_back(w);
}
int main()
{
    int T;
    cin>>T;
    while(T--)
    {
        int _size,N;
        cin>>_size>>N;
        Graph *g = new Graph(_size);
        for(int i=0;i<N;i++)
        {
            int u,v;
            cin>>u>>v;
            g->addEdge(u,v);
        }
        cout<<g->isCyclic()<<endl;
    }
}




//////////////////////


bool vist[105];
bool rs[105];
list<int> *adj;

bool dfs(int v){
    if(vist[v]){
        if(rs[v]){
			return 1;
        }
        return 0;
    }
    vist[v] = 1;
    rs[v] = 1;
    for(auto it = adj[v].begin(); it != adj[v].end(); it++){
		if( dfs(*it) ){
			return 1;
		}
    }
    rs[v] = 0;
    return 0;
}

bool Graph :: isCyclic(){
	::adj = Graph::adj;
	fill( (bool*) vist, (bool*) vist+105, false );
	fill( (bool*) rs, (bool*) rs+105, false );
	for(int i=0; i<V; i++){
		if(!vist[i]){
			if( dfs(i) ){
				return 1;
			}
		}
	}
	return 0;
}


