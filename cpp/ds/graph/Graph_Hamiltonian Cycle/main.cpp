#include <iostream>
#include <cstring>

using namespace std;

void show(int* arr, int n){
    for(int i=0; i<n; i++)
        cout<<arr[i]<<" ";
    cout<<endl;
}

class Graph{
public:
    int V;
    bool** adj;
    Graph(int v, bool** mat);
    void hamCycle();
    bool hamUtil(int pos, int* path);
    bool isSafe(int* path, int v, int pos);
};

Graph::Graph(int v,  bool** mat){
    V = v;
    adj = mat;
}

void Graph::hamCycle(){
    int path[V+1];
    memset(path, -1, sizeof(path));
    path[0] = 0;
    if(hamUtil(1, path)){
        cout<<endl;
        for(int i: path)
            cout<<i<<" ";
        cout<<endl;
    }

}


bool Graph::hamUtil(int pos, int* path){
    int lp = pos - 1;
    if(pos == V){
        if(adj[path[lp]][0]){
            path[pos] = 0;
            return true;
        }
        else
            return false;
    }
    for(int i=1; i<V; i++){
        if(adj[path[lp]][i]){
            if(isSafe(path, i, lp)){
                path[pos] = i;
                show(path, V+1);
                if(hamUtil(pos+1, path))
                    return true;
                path[pos] = -1;
            }
        }
    }
    return false;
}

bool Graph::isSafe(int* path, int v, int pos){
    for(int i=0; i<=pos; i++){
        if(path[i] == v)
            return false;
    }
    return true;
}

int main()
{
    int tcs;
    cin >>tcs;
    while(tcs--){
        int V, E;
        cin >> V >> E;
        bool** graph;
        graph = new bool* [V];
        for(int i=0; i<V; i++){
            graph[i] = new bool [V];
        }
        for(int i=0; i<V; i++){
            for(int j=0; j<V; j++)
                graph[i][j] = 0;
        }
        int arr[2*E];
        for(int i=0; i<2*E; i++){
            cin >> arr[i];
        }
        for(int i=0; i<2*E; i+=2){
            graph[arr[i]-1][arr[i+1]-1] = true;
            graph[arr[i+1]-1][arr[i]-1] = true;
        }
        Graph g(V, graph);
        g.hamCycle();
    }
    return 0;
}




















