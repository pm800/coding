#include <iostream>
#include <list>
#include <stack>
#include <climits>
#include <vector>
#define NINF INT_MIN

using namespace std;

struct Node{
    int dest, weight;
};

class Graph{//directed
public:
    int V;
    list<Node>* adj;
    void addEdge(int v, int w, int weight);
    Graph(int v);
    void topologicalSortUtil(int v, stack<int>& stk, bool visited[]);
    void longestPath(int s);
};

Graph::Graph(int v){
    V = v;
    adj = new list<Node> [v];
}

void Graph::addEdge(int v, int w, int weight){
    adj[v].push_back({w, weight});
}

void Graph::longestPath(int s){
    stack<int> stk;
    vector<int> stkVector;
    bool visited[V] = {};
    for(int i=0; i<V; i++)
        if(visited[i] == false)
            topologicalSortUtil(i, stk, visited);
    int dist[V];
    fill_n(dist, V, NINF);
    dist[s] = 0;
    while(! stk.empty()){
        int v = stk.top();
        stkVector.push_back(v);
        stk.pop();
        if(dist[v] != NINF){
            list<Node>::iterator it;
            for(it = adj[v].begin(); it != adj[v].end(); it++){
                int u, w;
                u = it->dest;
                w = it->weight;
                int x = dist[v] + w;
                if(dist[u] < x)
                    dist[u] = x;
            }
        }
    }
    vector<int>::iterator it;
    for(it=stkVector.begin(); it != stkVector.end(); it++){
        cout<<*it<<" "<<dist[*it]<<endl;
    }
}

void Graph::topologicalSortUtil(int v, stack<int>& stk, bool visited[]){
    visited[v] = true;
    list<Node>::iterator it;
    for(it = adj[v].begin(); it != adj[v].end(); it++){
        if(visited[it->dest] == false)
            topologicalSortUtil(it->dest, stk, visited);
    }
    stk.push(v);
}

int main()
{
    Graph g(6);
    g.addEdge(0, 1, 5);
    g.addEdge(0, 2, 3);
    g.addEdge(1, 3, 6);
    g.addEdge(1, 2, 2);
    g.addEdge(2, 4, 4);
    g.addEdge(2, 5, 2);
    g.addEdge(2, 3, 7);
    g.addEdge(3, 5, 1);
    g.addEdge(3, 4, -1);
    g.addEdge(4, 5, -2);

    int s = 1;
    g.longestPath(s);
    return 0;
}



















