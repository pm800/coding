#include <iostream>
#include <list>
#include <queue>
using namespace std;

class Graph{
public:
    int V;
    list<int>* adj;
    Graph(int v) {  V = v; adj = new list<int> [V];}
    void addEdge(int u, int v) { adj[u].push_back(v); };
    void bfs(int i, int l);
};

void Graph::bfs(int v, int l){//if v
    bool visited[V] = {};
    int level[V] = {};
    queue<int> q;
    q.push(v);
    visited[v] = true;
    level[v] = 0;
    while(! q.empty()){
        int w = q.front();
        if(level[w] >= l) break;
        q.pop();
        for(int i: adj[w]){
            if(visited[i] == false){
                visited[i] = true;
                q.push(i);
                level[i] = level[w] + 1;
            }
        }
    }
    while(! q.empty()){
        cout<<q.front()<<" ";
        q.pop();
    }cout<<endl;
}



int main()
{
    Graph g(6);
    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(1, 3);
    g.addEdge(1, 4);
    g.addEdge(2, 4);
    g.addEdge(2, 5);

    g.bfs(0, 2);
    return 0;
}

























