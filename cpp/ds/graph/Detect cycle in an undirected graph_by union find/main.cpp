#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl


using namespace std;

struct S{
	int s, d;
};

S edges[105];
int V, E;
struct SS{
	int p, r;
};
SS arr[105];

int findf(int x){
	if(arr[x].p != x){
        arr[x].p = findf(arr[x].p);
	}
	return arr[x].p;
}

void unionf(int s, int d){
	int sr = findf(s);
	int dr = findf(d);
	if(arr[sr].r < arr[dr].r){
		arr[sr].p = dr;
	}
	else if(arr[sr].r > arr[dr].r){
		arr[dr].p = sr;
	}
	else{
        arr[dr].p = sr;
        arr[sr].r++;
	}
	return;
}


bool isCyclic(){
	for(int i=0; i<V; i++){
		arr[i] = {i, 0};
	}
    for(int i=0; i<E; i++){
        int s = edges[i].s;
        int d = edges[i].d;
        int sr = findf(s);
        int dr = findf(d);
        if(sr == dr){
			return 1;
        }
        unionf(s, d);
    }
	return 0;
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>V>>E;
		for(int i=0; i<E; i++){
			int s, d;
			cin>>s>>d;
			edges[i] = {s,d};
		}
		cout<<isCyclic()<<endl;
	}
	return 0;
}


