#include <iostream>
#include <set>
#include <list>
#include <queue>

using namespace std;

bool ant(string s1, string s2){
    int n = s1.size();
    bool flag = false;
    for(int i=0; i<n; i++){
        if(s1[i] != s2[i]){
            if(!flag) flag = true;
            else  return false;
        }
    }
    if(flag) return true;
    return false;
}

int bfs(string* arr, int n, int s, int d, list<int>* adj){
    bool* visited = new bool [n];
    queue<int> q;
    q.push(s);
    visited[s] = true;
    int* level = new int [n];
    level[s] = 0;
    if(s==d) return 1;
    while(!q.empty()){
        int x = q.front(); q.pop();
        for(int i: adj[x]){
            if( !visited[i] ){
                if(i==d) return level[x]+2;
                level[i] = level[x]+1;
                q.push(i);
                visited[i] == true;
            }
        }
    }
    return -1;
}

int shortestChainLen(string s, string t, set<string>D){
    D.insert(s);
    D.insert(t);
    int n = D.size();
    string* arr = new string [n];
    int i = 0;
    int src=-1, dest=-1;
    for(string st: D){
        if(st==s) src = i;
        if(st==t) dest = i;
        arr[i] = st; i++;
    }
    list<int>* adj = new list<int> [n];
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            if(ant(arr[i], arr[j]))
                adj[i].push_back(j);
        }
    }
    int ans = bfs(arr, n, src, dest, adj);
    return ans;

}

int main()
{
    // make dictionary
    set<string> D;
    D.insert("poon");
    D.insert("plee");
    D.insert("same");
    D.insert("poie");
    D.insert("plie");
    D.insert("poin");
    D.insert("plea");
    string start = "toon";
    string target = "plea";
    cout << shortestChainLen(start, target, D);
    return 0;
}
