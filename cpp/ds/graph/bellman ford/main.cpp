#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl


using namespace std;


int V, E;
struct Edge{
	int u, v, w;
};
Edge edges[1000006];
int dist[10004];

void bellman(){
	fill(dist, dist+V+1, INT_MAX);
	dist[1] = 0;
	int tmp = V;
    while(tmp--){
		bool flag = 0;
        for(int i=0; i<E; i++){
			int u = edges[i].u, v = edges[i].v, w = edges[i].w;
            if(dist[u] != INT_MAX && dist[v] > dist[u] + w){
            	flag = 1;
				dist[v] = dist[u] + w;
            }
        }
		if(flag == 0){
			break;
		}
    }
	for(int i=0; i<E; i++){
		int u = edges[i].u, v = edges[i].v, w = edges[i].w;
		if(dist[u] != INT_MAX && dist[v] > dist[u] + w){
			cout<<"Negative cycle exits"<<endl;
		}
	}
	for(int i=2; i <= V; i++){
		printf("%d ", dist[i]);
	}
	cout<<endl;
}

int main(){
	cin >> V>> E;
	for(int i = 0;i < E;++i){
		int x,y,w;
		cin >> x >> y >> w;
        edges[i] = {x, y, w};
	}
	bellman();
    return 0;
}



//4 4
//1 2 2
//1 3 3
//2 3 2
//2 4 1
















