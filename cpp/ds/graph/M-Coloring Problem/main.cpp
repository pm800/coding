#include <iostream>
#define VM 51
using namespace std;

int graph[VM][VM];
bool visited[VM];
int color[VM];

int V, m, E;

bool isSafe(int v, int c){
    for(int i=0; i<V; i++){
        if(graph[v][i] && color[i]==c)
            return false;
    }
    return true;
}

bool dfsUtil(int v){
    if(v==V) return true;
    for(int clr=0; clr<m; clr++){
        if(isSafe(v,clr)){
            color[v] = clr;
            bool x = dfsUtil(v+1);
            if(x) return true;
            color[v] = -1;
        }
    }
    return false;
}

void dfs(){
    fill_n(visited, VM, false);
    fill_n(color, VM, -1);
    bool ans = dfsUtil(0);
    if(ans) cout<<1<<endl;
    else cout<<0<<endl;
}

int main()
{
    int tcs;
    cin >>tcs;
    while(tcs--){
        cin >>V>>m>>E;
        fill(&graph[0][0], &graph[VM][VM], 0);
        for(int i=0; i<E; i++){
            int x, y;
            cin >>x>>y;
            graph[x-1][y-1] = 1;
            graph[y-1][x-1] = 1;
        }
        dfs();
    }
    return 0;
}
