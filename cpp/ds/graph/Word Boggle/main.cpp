#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
using namespace std;

int m, n;

char mat[7][7];
bool visited[7][7] = {};

string arr[10] ={};

string s;



bool dfs(int i, int j, int p){
    if(p >= s.size()) {
		return 1;
    }
    if(visited[i][j]) return 0;
    if(i<0 || j<0 || i>=m || j>=n) return 0;
    if(mat[i][j] != s[p] ) return 0;
    visited[i][j] = 1;
    p++;
    if(
		dfs(i-1, j-1, p) ||
		dfs(i, j-1, p) ||
		dfs(i-1, j, p) ||
		dfs(i+1, j+1, p) ||
		dfs(i, j+1, p) ||
		dfs(i+1, j, p) ||
		dfs(i+1, j-1, p) ||
		dfs(i-1, j+1, p)
	){
        visited[i][j] = 0;
        return 1;
	}
	visited[i][j] = 0;
	return 0;
}


bool f(){
	for(int i=0; i<m; i++){
		for(int j=0; j<n; j++){
			if( dfs(i, j, 0) == 1 ) return 1;
		}
	}
	return 0;
}



int main()
{
	fill((bool*)visited, (bool*)visited+49, 0);
	int tcs;
	cin>>tcs;
	while(tcs--){
		int x;
		cin>>x;
		for(int i=0; i<x; i++)
			cin >> arr[i];
		unordered_set<string> st;
		for(int i=0; i<x; i++)
			st.insert(arr[i]);
		cin>>m>>n;
		int cnt = 0;
		for(int i=0; i<m; i++){
			for(int j=0; j<n; j++){
				cin >> mat[i][j];
			}
		}
		vector<string> v;
		for(auto it = st.begin(); it != st.end(); it++){
			s = *it;
			if( f() == 1){
				v.push_back(s);
			}
		}
		sort(v.begin(), v.end());
		for(string s: v)
            cout<<s<<" ";
		if(v.size() == 0)
			cout<<-1;
		cout<<endl;

	}
    return 0;
}



