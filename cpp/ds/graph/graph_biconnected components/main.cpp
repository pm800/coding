#include <iostream>
#include <list>
#include <vector>
#include <cstring>
#include <stack>
using namespace std;

struct Edge{
    int u, v;
};

void show(int* arr, int n){
    for(int i=0; i<n; i++)
        cout<< arr[i]<<" ";
    cout<<endl;
}

class Graph{
public:
    int V;
    list<int>* adj;
    Graph(int v){ adj = new list<int> [v]; V = v; };
    void addEdge(int u, int v){ adj[u].push_back(v); adj[v].push_back(u); };
    void bcc();
    void dfsUtil(int u, bool* visited, int* parent, int* low, int* disc, stack<Edge>& st);
};

void Graph::bcc(){
    bool visited[V] = {};
    int parent[V], low[V], disc[V];
    stack<Edge> st;
    fill_n(parent, V, -1);
    for(int i=0; i<V; i++){
        if(!visited[i]){
            dfsUtil(i, visited, parent, low, disc, st);
            while(!st.empty()){
                cout<<st.top().u<<"->"<<st.top().v<<"  ";
                st.pop();
            }cout<<endl;
        }
    }

}

void Graph::dfsUtil(int u, bool* visited, int* parent, int* low, int* disc, stack<Edge>& st){
    static int dt = 0; dt++;
    disc[u] = dt;
    low[u] = dt;
    visited[u] = true;
    int childern = 0;
    for(int v: adj[u]){
        if(!visited[v]){
            st.push({u,v});
            parent[v] = u;
            childern++;
            dfsUtil(v, visited, parent, low, disc, st);
            low[u] = min(low[u], low[v]);
            if(disc[u] == 1 && childern > 1  || disc[u] > 1 && low[v]>=disc[u]){
                while( 1 ){
                    cout<<st.top().u<<"->"<<st.top().v<<"  ";
                    if(st.top().u == u && st.top().v == v){
                        st.pop();
                        break;
                    }
                    st.pop();
                }cout<<endl;
            }
        }
        else if( disc[v]<disc[u] && v != parent[u] ){
            st.push({u,v});
            low[u] = min(low[u], disc[v]);
        }
    }
}

int main()
{
    Graph g(12);
    g.addEdge(0,1);
    g.addEdge(1,2);
    g.addEdge(1,3);
    g.addEdge(2,3);
    g.addEdge(2,4);
    g.addEdge(3,4);
    g.addEdge(1,5);
    g.addEdge(0,6);
    g.addEdge(5,6);
    g.addEdge(5,7);
    g.addEdge(5,8);
    g.addEdge(7,8);
    g.addEdge(8,9);
    g.addEdge(10,11);
    g.bcc();
    return 0;
}
