#include <iostream>
#define MAX 20
using namespace std;

int graph[MAX][MAX];


int f(int V, int x, int y, int k){
    if(k==0){
        if(x==y) return 1;
        return 0;
    }
    int arr[V][k], temp[V];
    for(int i=0; i<V; i++){
        arr[i][1] = graph[i][y];
    }
    int p=2;
    while(p<=k){
        for(int i=0; i<V; i++){
            int sum = 0;
            for(int j=0; j<V; j++){
                if(graph[i][j]==1)
                    sum +=  arr[j][p-1];
            }
            arr[i][p] = sum;
        }
        p++;
    }
    return arr[x][k];
}

int main()
{
    int tcs;
    cin>>tcs;
    while(tcs--){
        int V, x, y, k;
        cin>>V;
        for(int i=0; i<V; i++){
            for(int j=0; j<V; j++){
                cin >> graph[i][j];
            }
        }
        cin>>x>>y>>k;
        int ans = f(V, x, y, k);
        cout<<ans<<endl;
    }
    return 0;
}
