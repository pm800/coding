#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset
#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl


using namespace std;

int n;
int mat[12][12];
vector<int> stk;
bool flag = 0;

bool issafe(int i, int j){
	int ti = i, tj = j;
    if(mat[i][j]){
		return false;
    }
    while(--j>=0){
		if(mat[i][j]){
			return false;
		}
    }
    i = ti; j= tj;
    while(--j>=0 && --i>=0){
		if(mat[i][j]){
			return false;
		}
    }
    i = ti; j = tj;
    while(--j>=0 && ++i<n){
		if(mat[i][j]){
			return false;
		}
    }
    return true;
}

void g(int j){
    if(j == n){
		flag = 1;
		printf("[");
		for(int p=0; p<stk.size(); p++){
			printf("%d ", stk[p]);
		}
		printf("] ");
		return;
    }
    for(int i=0; i<n; i++){
		if(issafe(i, j)){
            mat[i][j] = 1;
            stk.push_back(i+1);
            g(j+1);
            mat[i][j] = 0;
            stk.pop_back();
		}
    }
    return;
}

void f(){
	flag = 0;
    for(int i=0; i<n; i++){
		for(int j=0; j<n; j++){
			mat[i][j] = 0;
		}
    }
    g(0);
    if(flag == 0){
		cout<<-1;
    }
    cout<<endl;
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		f();
	}
	return 0;
}


/////////////////////////////////////////////////////////
//int n;
//int mat[12][12];
//vector<int> stk;
//bool flag = 0;
//
//bool ip(int i, int j){
//	if( i<0 || j<0 || i>=n || j>=n ){
//		return 0;
//	}
//	return 1;
//}
//
//void vist(int i, int j, int t){
//	mat[i][j] += t;
//    int ti = i, tj = j;
//    while(ip(i, ++j)){
//		mat[i][j] += t;
//    }
//    i = ti; j = tj;
//    while(ip(--i, ++j)){
//		mat[i][j] += t;
//    }
//    i = ti, j = tj;
//    while(ip(++i, ++j)){
//		mat[i][j] += t;
//    }
//}
//
//
//void g(int j){
//    if(j == n){
//		flag = 1;
//		printf("[");
//		for(int p=0; p<stk.size(); p++){
//			printf("%d ", stk[p]);
//		}
//		printf("] ");
//		return;
//    }
//    for(int i=0; i<n; i++){
//		if(mat[i][j] == 0){
//            vist(i, j, 1);
//            stk.push_back(i+1);
//            g(j+1);
//            vist(i, j, -1);
//            stk.pop_back();
//		}
//    }
//    return;
//}
//
//void f(){
//	flag = 0;
//    for(int i=0; i<n; i++){
//		for(int j=0; j<n; j++){
//			mat[i][j] = 0;
//		}
//    }
//    g(0);
//    if(flag == 0){
//		cout<<-1;
//    }
//    cout<<endl;
//}
//
//int main(){
//	int tcs;
//	cin >> tcs;
//	while(tcs--){
//		cin>>n;
//		f();
//	}
//	return 0;
//}


















