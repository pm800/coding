#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl


using namespace std;

const int MAX = 1e4 + 5;
int spanningTree(vector <pair<int,int> > g[], int n);
int main()
{
	int t ;
	cin>>t;
	while(t--)
	{
	vector <pair<int,int> > adj[MAX];
    int n,e;
    int w, mC;
    cin >> n>> e;
    for(int i = 0;i < e;++i)
    {
    	int x,y;
        cin >> x >> y >> w;
        adj[x].push_back({w, y});
        adj[y].push_back({w, x});
    }

    mC= spanningTree(adj, MAX);
    cout << mC << endl;
	}
    return 0;
}


/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/* Finds the sum of weights of the edges of the Minimum Spanning Tree.
    Graph is represented as adjacency list using array of vectors.  MAX
    is an upper  limit on number of vertices.
   g[u] represents adjacency list of vertex u,  Every element of this list
   is a pair<w,v>  where v is another vertex and w is weight of edge (u, v)
  Note : Vertex numbers start with 1 and don't need to be contiguous.   */

struct S{
	int p, v, w;
};

class Comp{
public:
	bool operator() (S s1, S s2){
		return s1.w > s2.w; // minHeap
	}
};

int spanningTree(vector <pair<int,int> > adj[], int MAX){
	priority_queue<S, vector<S>, Comp> q;
    q.push({-1, 1, 0});
    S s;
    bool done[MAX] = {0};
    vector<S> ans;
    int mstsm = 0;
    while(!q.empty()){
		s = q.top();
        q.pop();
        int v = s.v, w = s.w, p = s.p;
        if(done[v]){
			continue;
        }
        mstsm += w;
        done[v] = 1;
        ans.push_back({p, v, w});
        for(int i=0; i<adj[v].size(); i++){
            pair<int, int> mp = adj[v][i];
            int mv = mp.second, mw = mp.first;
            if(done[mv]){
				continue;
            }
            q.push({v, mv, mw});
        }
    }
//    for(int i=1; i<ans.size(); i++){
//		p3(ans[i].p, ans[i].v, ans[i].w);
//    }
	return mstsm;
}

//1
//4 4
//1 2 2
//1 3 3
//2 3 2
//2 4 1
















