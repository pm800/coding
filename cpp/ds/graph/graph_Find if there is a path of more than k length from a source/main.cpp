#include <iostream>
#include <climits>
#include <list>
#include <vector>
#define NINF INT_MIN

using namespace std;

struct GraphNode{
    int v, w;
};

class Graph{
public:
    int V;
    list<GraphNode>* adj;
    Graph(int v){  V = v; adj = new list<GraphNode> [V]; };
    void addEdge(int u, int v, int w) {adj[u].push_back({v, w}); adj[v].push_back({u, w});  };
    void largestPath(int s, int k);
    bool dfsUtil(int s, int uptos, int k, int* maxdist, bool* rstack);
    void show(int* arr, int n);
};

void Graph::show(int* arr, int n){
    for(int i=0; i<n; i++){
        cout<<i<<" to "<< arr[i]<<endl;
    }
}


void Graph::largestPath(int s, int k){
    bool rstack[V] = {};
    int maxdist[V] = {NINF};
    dfsUtil(s, 0, k, maxdist, rstack);
    maxdist[s] = 0;
    show(maxdist, V);
}

bool Graph::dfsUtil(int s, int uptos, int k, int* maxdist, bool* rstack){
    static int cnt = 0; cnt++;
//    if(uptos > k) return true;
    rstack[s] = true;
    for(GraphNode node : adj[s]){
        int v, w;
        v = node.v; w = node.w;
        if(rstack[v] == false){
            if(maxdist[v] < uptos + w)
                maxdist[v] = uptos + w;
            int x = dfsUtil(v, uptos + w, k, maxdist, rstack);
//            if(x == true) return true;
        }
    }
    rstack[s] = false;
    return false;
}


///////////////////////////////////////////////////////////////////////////////////////////

void addEdge(Graph& graph, int u, int v, int w){
    graph.addEdge(u, v, w);
}

int main()
{
    int V = 9;
    Graph g(V);
    g.addEdge(0, 1, 4);
    g.addEdge(0, 7, 8);
    g.addEdge(1, 2, 8);
    g.addEdge(1, 7, 11);
    g.addEdge(2, 3, 7);
    g.addEdge(2, 8, 2);
    g.addEdge(2, 5, 4);
    g.addEdge(3, 4, 9);
    g.addEdge(3, 5, 14);
    g.addEdge(4, 5, 10);
    g.addEdge(5, 6, 2);
    g.addEdge(6, 7, 1);
    g.addEdge(6, 8, 6);
    g.addEdge(7, 8, 7);
	g.largestPath(0, 84848);
    return 0;
}


















