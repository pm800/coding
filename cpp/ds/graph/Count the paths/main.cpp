#include <iostream>
#include <list>
#define VM 11
using namespace std;

list<int> adj[VM];
bool rstack[VM];



void dfs(int v, int d, int& cnt){
    rstack[v] = true;
    for(int i: adj[v]){
        if(rstack[i]==false){
            if(i==d){
                cnt++;
                continue;
            }
            dfs(i, d, cnt);
        }
    }
    rstack[v] = false;
}

int f(int V, int E, int s, int d){
    int cnt = 0;
    fill_n(rstack, V, false);
    if(s==d) cnt = 1;
    dfs(s, d, cnt);
    return cnt;
}

int main()
{
    int tcs;
    cin>>tcs;
    while(tcs--){
        int V, E;
        cin>>V>>E;
        for(int i=0; i<V; i++)
            adj[i] = list<int>();
        for(int i=0; i<E; i++){
            int a, b;
            cin >> a>>b;
            adj[a].push_back(b);
        }
        int s, d;
        cin>>s>>d;
        int ans = f(V, E, s, d);
        cout<<ans<<endl;
    }
    return 0;
}
