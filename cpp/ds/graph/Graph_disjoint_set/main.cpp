#include <iostream>
#include <algorithm>

using namespace std;

void show(int* arr, int n){
    for(int i=0; i<n; i++)
        cout<<arr[i]<<" ";
    cout<<endl;
}

struct Edge{
    int src, dest, weight;
};

struct Subset{
    int parent, rnk;
};

class Graph{
public:
    int V, E;
    Edge* earr;
    Graph(int v, int e);
    bool isCyclic();
    int fnd(int x, Subset* ssarr);
    void unn(int x, int y, Subset* ssarr);
    void addEdge(int x, int y);
    void addEdge(int x, int y, int w);
    void kruskalMst();
};

Graph::Graph(int v, int e){
    V = v;
    E = e;
    earr = new Edge [e];
}

void Graph::addEdge(int x, int y){
    if(x>V or y>V)
        throw invalid_argument("error");
    static int i = 0;
    earr[i].src = x;
    earr[i].dest = y;
    i++;
}

void Graph::addEdge(int x, int y, int w){
    if(x>V or y>V)
        throw invalid_argument("error");
    static int i = 0;
    earr[i].src = x;
    earr[i].dest = y;
    earr[i].weight = w;
    i++;
}

bool Graph:: isCyclic(){//union find (disjoint set)
    Subset ssarr[V];
    for(int i=0; i<V; i++){
        ssarr[i].parent = i;
        ssarr[i].rnk = 0;
    }
    for(int i=0; i<E; i++){
        int x, y;
        x = earr[i].src;
        y = earr[i].dest;
        int xrep, yrep;
        xrep = fnd(x, ssarr);
        yrep = fnd(y, ssarr);
        if(xrep == yrep){
            cout<< "cycle exists"<<endl;
            return true;
        }
        else
            unn(xrep, yrep, ssarr);
    }
    return false;
}

int Graph::fnd(int x, Subset* ssarr){//finding root of tree or representative of subset.
    if (ssarr[x].parent != x)
        ssarr[x].parent = fnd(ssarr[x].parent, ssarr);
    return ssarr[x].parent;
}

void Graph::unn(int x, int y, Subset* ssarr){
    int xrep, yrep;
    xrep = fnd(x, ssarr);
    yrep = fnd(y, ssarr);
    if(ssarr[xrep].rnk < ssarr[yrep].rnk)
        ssarr[xrep].parent = yrep;
    else if(ssarr[xrep].rnk > ssarr[yrep].rnk)
        ssarr[yrep].parent = xrep;
    else{
        ssarr[yrep].parent = xrep;
        ssarr[xrep].rnk += 1;
    }
}

bool compareWeight(Edge s1, Edge s2){
    return s1.weight < s2.weight;
}

void Graph::kruskalMst(){
    sort(earr, earr+E, compareWeight);
    Subset ssarr[V];
    for(int i=0; i<V; i++){
        ssarr[i].parent = i;
        ssarr[i].rnk = 0;
    }
    for(int i=0; i<E; i++){
        int x, y;
        x = earr[i].src;
        y = earr[i].dest;
        int xrep, yrep;
        xrep = fnd(x, ssarr);
        yrep = fnd(y, ssarr);
        if(xrep != yrep){
            unn(xrep, yrep, ssarr);
            cout<<x<<" to "<<y<<" with weight "<<earr[i].weight<<endl;
        }
    }
}



int main()
{
//    Graph graph = Graph(6,6);
//    graph.addEdge(0,1);
//    graph.addEdge(1,2);
//    graph.addEdge(1,5);
//    graph.addEdge(5,4);
//    graph.addEdge(2,4);
//    graph.addEdge(2,3);
//    graph.isCyclic();

//    Graph graph = Graph(9,14);
//    graph.addEdge(0,1,4);
//    graph.addEdge(0,7,8);
//    graph.addEdge(1,2,8);
//    graph.addEdge(1,7,11);
//    graph.addEdge(7,6,1);
//    graph.addEdge(7,8,7);
//    graph.addEdge(2,8,2);
//    graph.addEdge(8,6,6);
//    graph.addEdge(2,5,4);
//    graph.addEdge(2,3,7);
//    graph.addEdge(6,5,2);
//    graph.addEdge(3,5,14);
//    graph.addEdge(3,4,9);
//    graph.addEdge(5,4,10);
//    graph.kruskalMst();

    return 0;
}













