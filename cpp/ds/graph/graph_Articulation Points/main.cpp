#include <iostream>
#include <list>

using namespace std;

class Graph{
public:
    int V;
    list<int>* adj;
    Graph(int v){ V = v; adj = new list<int> [V];};
    void addEdge(int v, int u){  adj[v].push_back(u); adj[u].push_back(v);};
    void AP();
    void APUtil(int v, bool* visited, int* disc, int* parent, bool* ap, int* low);
};

void Graph::AP(){
    bool visited[V] = {};
    int parent[V];
    fill_n(parent, V, -1);
    int disc[V] = {};
    int low[V] = {};
    bool ap[V] = {};
    for(int i=0; i<V; i++){
        if(!visited[i]){
            APUtil(i, visited, disc, parent, ap, low);
        }
    }
    for(int i=0; i<V; i++){
        if(ap[i])
            cout<<i<<endl;
    }
}

void Graph::APUtil(int v, bool* visited, int* disc, int* parent, bool* ap, int* low){
    visited[v] = true;
    static int dt = 0;
    dt++;
    disc[v] = dt;
    low[v] = dt;
    int children = 0;
    for(int w: adj[v]){
        if(!visited[w]){
            parent[w] = v;
            children++;
            APUtil(w, visited, disc, parent, ap, low);
            low[v] = min(low[v], low[w]);
            if(parent[v] == -1){
                if(children >= 2)
                    ap[v] = true;
            }
            else{
                if(low[w] >= disc[v])
                    ap[v] = true;
            }
        }
        else{
            if(w != parent[v])
                low[v] = min(low[v], disc[w]);
        }
    }

}

int main()
{
    int V = 8;
    Graph g(V);
    g.addEdge(0, 2);
    g.addEdge(0, 2);
    g.addEdge(1, 2);
    g.addEdge(2, 3);
    g.addEdge(3, 4);
    g.addEdge(4, 5);
    g.addEdge(4, 6);
    g.addEdge(5, 6);
    g.addEdge(5, 7);
    g.AP();
    return 0;
}



































