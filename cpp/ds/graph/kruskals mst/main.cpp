#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl


using namespace std;
int V, E;
struct Edge{
	int u, v, w;
};

bool comp(Edge e1, Edge e2){
	return e1.w < e2.w;
}

vector<Edge> edges;

struct S{
	int p, r;
};
vector<S> subset;

int findf(int v){
	if(subset[v].p != v){
        subset[v].p = findf(subset[v].p);
	}
	return subset[v].p;
}

void unionf(int ur, int vr){
    if(subset[ur].r < subset[vr].r){
		subset[ur].p = vr;
    }
    else if(subset[ur].r > subset[vr].r){
		subset[vr].p = ur;
    }
    else{
		subset[vr].p = ur;
		subset[ur].r++;
    }
}

int f(){
	int ans = 0;
	sort(edges.begin(), edges.end(), comp);
	subset.clear();
	subset.resize(V+1);
	for(int i=1; i<=V; i++){
		subset[i].p = i;
		subset[i].r = 0;
	}
    for(int i=0; i<edges.size(); i++){
		int u = edges[i].u, v = edges[i].v, w = edges[i].w;
		int ur = findf(u);
		int vr = findf(v);
		if(ur == vr){
			continue;
		}
		ans += w;
		unionf(ur, vr);
    }
    return ans;
}

int main(){
	cin>>V>>E;
	edges.clear();
	edges.resize(E);
	for(int i=0; i<E; i++){
		int u, v, w;
        cin>>u>>v>>w;
        edges[i] = {u, v, w};
	}
	cout<<f()<<endl;
    return 0;
}



























