#include <iostream>
#include <list>
#include <stack>
#include <vector>
using namespace std;


void printCircuit(list<int> adj[]){
    stack<int> path;
    vector<int> circuit;
    path.push(0);
    int v;
    while(!path.empty()){
        v = path.top();
        if(!adj[v].empty()){
            int x = adj[v].back();
            adj[v].pop_back();
            path.push(x);
        }
        else{
            path.pop();
            circuit.push_back(v);
        }
    }
    cout<<"ans ";
    for(int i=circuit.size()-1; i>=0; i--)
        cout<<circuit[i]<<" ";
    cout<<endl;
}

int main()
{
    list<int> adj1[3];
    list<int> adj2[7];

    // Build the edges
    adj1[0].push_back(1);
    adj1[1].push_back(2);
    adj1[2].push_back(0);
    printCircuit(adj1);
    cout << endl;

    // Input Graph 2

    adj2[0].push_back(1);
    adj2[0].push_back(6);
    adj2[1].push_back(2);
    adj2[2].push_back(0);
    adj2[2].push_back(3);
    adj2[3].push_back(4);
    adj2[4].push_back(2);
    adj2[4].push_back(5);
    adj2[5].push_back(0);
    adj2[6].push_back(4);
    printCircuit(adj2);
    return 0;
}
