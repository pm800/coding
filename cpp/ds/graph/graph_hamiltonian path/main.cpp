#include <iostream>
#include <cstring>

using namespace std;

void show(int* arr, int n){
    for(int i=0; i<n; i++)
        cout<<arr[i]<<" ";
    cout<<endl;
}

class Graph{
public:
    int V;
    bool** adj;
    Graph(int v, bool** mat);
    void hamCycle();
    bool hamUtil(int pos, int* path);
    bool isSafe(int* path, int v, int pos);
};

Graph::Graph(int v,  bool** mat){
    V = v;
    adj = mat;
}

void Graph::hamCycle(){
    int path[V+1];

    for(int i=0; i<V; i++){
        fill_n(path, V+1, -1);
        path[0] = i;
        if(hamUtil(1, path)){
            cout<<1<<endl;
            return;
        }
    }
    cout<<0<<endl;
}


bool Graph::hamUtil(int pos, int* path){
    int lp = pos - 1;
    if(pos == V){
        return true;
    }
    for(int i=0; i<V; i++){
        if(adj[path[lp]][i]){
            if(isSafe(path, i, lp)){
                path[pos] = i;
//                show(path, V+1);
                if(hamUtil(pos+1, path))
                    return true;
                path[pos] = -1;
            }
        }
    }
    return false;
}

bool Graph::isSafe(int* path, int v, int pos){
    for(int i=0; i<=pos; i++){
        if(path[i] == v)
            return false;
    }
    return true;
}

int main()
{
    int tcs;
    cin >>tcs;
    while(tcs--){
        int V, E;
        cin >> V >> E;

        bool** graph;
        graph = new bool* [V];
        for(int i=0; i<V; i++){
            graph[i] = new bool [V];
        }

        for(int i=0; i<V; i++){
            for(int j=0; j<V; j++)
                graph[i][j] = 0;
        }

        for(int i=0; i<2*E; i+=2){
            int a, b;
            cin >>a>>b; a--; b--;
            graph[a][b] = true;
            graph[b][a] = true;
        }
        Graph g(V, graph);
        g.hamCycle();

        for(int i=0; i<V; i++){
            delete [] graph[i];
        }
        delete [] graph;

    }
    return 0;
}


















