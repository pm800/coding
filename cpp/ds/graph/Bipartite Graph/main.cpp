#include <iostream>
#include <list>
#include <queue>
using namespace std;
#define VM 15

int graph[VM][VM]; // graph is a directed graph;
bool visited[VM];
bool color[VM];
int V;

bool bfs(int v){
    bool clr;
    bool flag = false;
    for(int i=0; i<V; i++){
        if(graph[v][i] && visited[i]){
            if(flag == true && color[i] == clr) return 0;
            flag = true;
            clr = !color[i];
        }
    }
    queue<int> q;
    q.push(v);
    visited[v] = true;
    color[v] = clr;
    while(!q.empty()){
        int x = q.front();
        q.pop();
        clr = !color[x];
        for(int i=0; i<V; i++){
            if(graph[x][i] && visited[i]){
                if(color[i] != clr) return 0;
                continue;
            }
            if(graph[x][i] && !visited[i]){
                visited[i] = true;
                q.push(i);
                color[i] = clr;
            }
        }
    }
    return 1;
}

bool isBipartite()
{
    fill_n(visited, V, false);
    fill_n(color, V, false);
    for(int i=0; i<V; i++){
        if(!visited[i]){
            bool x = bfs(i);
            if(x==0) return 0;
        }
    }
    return 1;
}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        cin >> V;
        for(int i=0; i<V; i++){
            for(int j=0; j<V; j++){
                cin >> graph[i][j];
            }
        }
        cout<<isBipartite()<<endl;
    }
    return 0;
}
