#include <iostream>
#include <queue>
#include <climits>

#define VM 50
int graph[VM][VM];
int rgraph[VM][VM];

using namespace std;

void dfs(bool* visited, int V, int v, int d){
    visited[v] = true;
    for(int i=0; i<V; i++){
        if(!visited[i] && rgraph[v][i] ){
            if(i==d){
                visited[i] = true;
                continue;
            }
            dfs(visited, V, i, d);
        }
    }
}

bool bfs(int V, int s, int d, int* parent){
    fill_n(parent, V, -1);
    bool visted[V] = {};
    queue<int> q;
    q.push(s);
    visted[s] = true;
    while(!q.empty()){
        int x = q.front(); q.pop();
        for(int i=0; i<V; i++){
            if( !visted[i] &&  rgraph[x][i]>0 ){
                visted[i] = true;
                q.push(i);
                parent[i] = x;
                if(i == d)
                    return true;
            }
        }
    }
    return false;
}

void fordFulkerson(int V, int s, int d){
    int maxflow = 0;
    int parent[V];
    while(bfs(V, s, d, parent)){
        int flow = INT_MAX, c=d;
        while(c != s){
            int p = parent[c];
            flow = min(flow, rgraph[p][c]);
            c = p;
        }
        c = d;
        while(c != s){
            int p = parent[c];
            rgraph[p][c] -= flow;
            rgraph[c][p] += flow;
            c = p;
        }
        maxflow += flow;

    }
//    cout<<"maxflow is "<<maxflow<<endl;

    bool visited[V] = {};
    dfs(visited, V, s, d);
    for(int i=0; i<V; i++){
        if(visited[i]){
            for(int j=0; j<V; j++){
                if(!visited[j] && graph[i][j]){
                    cout<<i<<" "<<j<<" ";
                }
            }
        }
    }

}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int V;
        cin >>V;
        for(int i=0; i<V; i++){
            for(int j=0; j<V; j++){
                int x;
                cin>> x;
                graph[i][j] = x;
                rgraph[i][j] = x;
            }
        }
        int s, t;
        cin >>s>>t;
        fordFulkerson(V, s, t);
        cout<<endl;
    }

    return 0;
}











