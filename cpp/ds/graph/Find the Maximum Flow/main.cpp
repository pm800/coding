#include <iostream>
#include <queue>
#include <climits>

#define V 1001
int graph[V][V];

using namespace std;

bool bfs(int graph[V][V], int s, int d, int* parent){
    fill_n(parent, V, -1);
    bool visted[V] = {};
    queue<int> q;
    q.push(s);
    visted[s] = true;
    while(!q.empty()){
        int x = q.front(); q.pop();
        for(int i=0; i<V; i++){
            if( !visted[i] &&  graph[x][i]>0 ){
                visted[i] = true;
                q.push(i);
                parent[i] = x;
                if(i == d)
                    return true;
            }
        }
    }
    return false;
}

int fordFulkerson(int graph[V][V], int s, int d){
    int maxflow = 0;
    int parent[V];
    while(bfs(graph, s, d, parent)){
        int flow = INT_MAX, c=d;
        while(c != s){
            int p = parent[c];
            flow = min(flow, graph[p][c]);
            c = p;
        }
        c = d;
        while(c != s){
            int p = parent[c];
            graph[p][c] -= flow;
            graph[c][p] += flow;
            c = p;
        }
        maxflow += flow;

    }
    return maxflow;
}

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        for(int i=0; i<V; i++)
            for(int j=0; j<V; j++)
                graph[i][j] = 0;
        int n, m;
        cin >>n>>m;
        for(int i=0; i<m; i++){
            int x, y, w;
            cin>>x>>y>>w;
            graph[x-1][y-1] += w;
            graph[y-1][x-1] += w;
        }
        cout<<fordFulkerson(graph, 0, n-1)<<endl;
    }

    return 0;
}











