    #include <bits/stdc++.h>
    using namespace std;
    #define M 100
    #define N 100
    int maxBPM(bool bpGraph[M][N],int m,int n);
    int main()
    {
        int t;
        cin>>t;
        while(t--){
            int m,n;
            cin>>m>>n;
            bool bpGraph[M][N];
            for(int i=0;i<m;i++)
                for(int j=0;j<n;j++)
                    cin>>bpGraph[i][j];
            cout << maxBPM(bpGraph,m,n)<<endl;
        }
        return 0;
    }

    /*Please note that it's Function problem i.e.
    you need to write your solution in the form of Function(s) only.
    Driver Code to call/invoke your function is mentioned above.*/

    /*Complete the function below*/


    bool g(int i, bool bpGraph[M][N], int m, int n, int match[]){
    //    if(i<0||i>=m){cout<<"myerr";exit(1);}
        for(int j=0; j<n; j++){
    //        if(i<0||i>=m||j<0||j>=n){cout<<"myerr";exit(1);}
            if(bpGraph[i][j] && match[j]<0){
                match[j] = i;
                return true;
            }
            else if(bpGraph[i][j] && match[j]>=0){
                int x = match[j];
                if(x==i) continue;
                if( g(x, bpGraph, m, n, match) ){
                    match[j] = i;
                    return true;
                }
            }
        }
        return false;
    }





    int maxBPM(bool bpGraph[M][N],int m,int n)
    {
            int match[n];
            memset(match,-1,sizeof(match));
    //        if(m<0||m>M||n<0||n>N){cout<<"myerr";exit(1);}
            int res = 0;
            for(int i=0; i<m; i++){
                if(g(i, bpGraph, m, n, match)) res++;
            }
            return res;

    }
