#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl


using namespace std;

vector<vector <pair<int,int> >> adj;
int V, E;


struct S{
	int v, w;
};

int parent[10004];
int dist[1004];
bool mstset[1004];

class Comp{
public:
	bool operator() (S s1, S s2){
		return s1.w > s2.w; // minHeap
	}
};

void dijkstra(){
	priority_queue<S, vector<S>, Comp> q;
	for(int i=1; i<=V; i++){
		dist[i] = INT_MAX;
		parent[i] = -1;
		mstset[i] = 0;
	}
    q.push({1, 0});
    dist[1] = 0;
    S s;
    vector<S> ans;
    while(!q.empty()){
		s = q.top();
        q.pop();
        int v = s.v;
        if(mstset[v]){
			continue;
        }
        mstset[v] = 1;
        for(int i=0; i<adj[v].size(); i++){
            pair<int, int> mp = adj[v][i];
            int mv = mp.second, mw = mp.first;
            if(mstset[mv] || dist[v] == INT_MAX || dist[mv] <= dist[v] + mw ){
				continue;
            }
            dist[mv] = dist[v] + mw;
            parent[mv] = v;
            q.push({mv, dist[mv]});
        }
    }
	for(int i=2; i<=V; i++){
		printf("%d ", dist[i]);
	}
	cout<<endl;
	return;
}

int main()
{

	cin >> V>> E;
	adj.clear();
	adj.resize(V+1, vector<pair<int,int>>());
	for(int i = 0;i < E;++i){
		int x,y,w;
		cin >> x >> y >> w;
		adj[x].push_back({w, y});
		adj[y].push_back({w, x});
	}
	dijkstra();
    return 0;
}



//4 4
//1 2 2
//1 3 3
//2 3 2
//2 4 1
















