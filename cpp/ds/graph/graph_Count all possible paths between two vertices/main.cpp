#include <iostream>
#include <list>
using namespace std;

class Graph{
public:
    int V;
    list<int>* adj;
    Graph(int v) {  V = v; adj = new list<int> [V];}
    void addEdge(int u, int v) { adj[u].push_back(v); };
    void totalPaths(int s, int d);
    int dfsUtil(int v, int d, bool* visited, int* paths, bool* rstack);
};

void Graph::totalPaths(int s, int d){
    bool visited[V] = {};
    int paths[V] = {};
    bool rstack[V] = {};
    int ans = dfsUtil(s, d, visited, paths, rstack);
    cout<<ans<<endl;
}

int Graph::dfsUtil(int v, int d, bool* visited, int* paths, bool* rstack){
    if(v == d)
        return 1;
    visited[v] = true;
    rstack[v] = true;
    for(int i: adj[v]){
        if(visited[i] == true){
            if(rstack[i]==false)
                paths[v] += paths[i];
        }
        else{
            int x = dfsUtil(i, d, visited, paths, rstack);
            paths[v] += x;
        }
    }
    rstack[v] = false;
    return paths[v];
}


int main()
{
    Graph g(6);
    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(0, 3);
    g.addEdge(2, 0);
    g.addEdge(2, 1);
    g.addEdge(1, 3);
    g.addEdge(1, 4);
    g.addEdge(1, 5);
    g.addEdge(4, 3);
    g.addEdge(5, 3);
    g.totalPaths(2,3);
    return 0;
}


















