#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl


using namespace std;

int n, m, ans;
char mat[53][53];
bool vist[53][53];
int dx[] = {0, 0, 1, -1};
int dy[] = {1, -1, 0, 0};


bool iv(int i, int j){
	if(i < 0 || j < 0 || i >= m || j >= n){
		return 0;
	}
	return 1;
}


void dfs(int a, int b){
	if(!iv(a, b)){
		return;
	}
	if(vist[a][b] || mat[a][b] == 'O'){
		return;
	}
	vist[a][b] = 1;
	for(int i=0; i<4; i++){
		dfs(a+dx[i], b+dy[i]);
	}
	return;
}

int f(){
	ans = 0;
    fill((bool*)vist, (bool*) vist + 53*53, false);
    for(int i=0; i<m; i++){
		for(int j=0; j<n; j++){
			if(!vist[i][j] && mat[i][j] == 'X'){
				dfs(i, j);
				ans++;
			}
		}
    }
    return ans;

}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>m>>n;
		for(int i=0; i<m; i++){
			for(int j=0; j<n; j++){
				cin>>mat[i][j];
			}
		}
        cout<<f()<<endl;
	}
	return 0;
}


