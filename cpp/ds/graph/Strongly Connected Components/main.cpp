#include <iostream>
#include <list>
#include <vector>
#include <stack>

using namespace std;

class Graph{//directed, connected
public:
    int V;
    list<int>* adj;
    Graph(int v);
    void addEdge(int v, int u);
    void kosarajusStronglyConnectedComponents();
    void kosarajuDfsUtil(int i, stack<int>& stk, bool* visited);
    void kosarajusDfsUtil2(int v, bool* visited, list<list<int>>& ans, list<int>* radj);
};

Graph::Graph(int v){
    V = v;
    adj = new list<int> [V];
}

void Graph::kosarajusDfsUtil2(int v, bool* visited, list<list<int>>& ans, list<int>* radj){
    visited[v] = true;
    ans.back().push_back(v);
    list<int>::iterator it;
    for(it = radj[v].begin(); it != radj[v].end(); it++){
        if(visited[*it] == false)
            kosarajusDfsUtil2(*it, visited, ans, radj);
    }
}

void Graph::addEdge(int v, int u){
    adj[v].push_back(u);
}

void Graph::kosarajuDfsUtil(int i, stack<int>& stk, bool* visited){
    visited[i] = true;
    list<int>::iterator it;
    for(it = adj[i].begin(); it != adj[i].end(); it++){
            if(visited[*it] == false)
                kosarajuDfsUtil(*it, stk, visited);
    }
    stk.push(i);
}

void Graph::kosarajusStronglyConnectedComponents(){
    stack<int> stk;
    bool visited[V] = {};
    for(int i=0; i<V; i++){
        if(visited[i] == false)
            kosarajuDfsUtil(i, stk, visited);
    }
    list<int>* radj;
    radj = new list<int> [V];
    for(int i=0; i<V; i++){
        list<int>::iterator it;
        for(it = adj[i].begin(); it != adj[i].end(); it++){
            radj[*it].push_back(i);
        }
    }
    fill_n(visited, V, false);
    list<list<int>> ans;
    while(! stk.empty()){
        int v = stk.top();
        stk.pop();
        if(visited[v] == false){
            ans.push_back({});
            kosarajusDfsUtil2(v, visited, ans, radj);
        }
    }
    list<list<int>>::iterator it;
    list<int>::iterator it2;
    for(it = ans.begin(); it != ans.end(); it++){
        for(it2 = it->begin(); it2 != it->end(); it2++){
            cout<<*it2<<" ";
        }cout<<endl;
    }
}

int main()
{
    Graph g(11);
    g.addEdge(0,1);
    g.addEdge(1,2);
    g.addEdge(2,0);
    g.addEdge(1,3);
    g.addEdge(3,4);
    g.addEdge(4,5);
    g.addEdge(5,3);
    g.addEdge(6,4);
    g.addEdge(6,7);
    g.addEdge(7,8);
    g.addEdge(8,9);
    g.addEdge(9,6);
    g.addEdge(9,10);
    g.kosarajusStronglyConnectedComponents();
    return 0;
}


























