#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset
#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p5(x, y, a, b, c) cout << x << " "<<y<<  " "<<a<<" "<<b<< " "<<c<<endl
#define p6(x, y, a, b, c, d) cout << x << " "<<y<<  " "<<a<<" "<<b<< " "<<c<<" "<<d<<endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl
#define VMAX 12


using namespace std;

int V, E;
vector<int> adj[VMAX];
int tim = 0, child = 0;
bool visited[VMAX], ap[VMAX];
int disc[VMAX], parent[VMAX], low[VMAX];
vector<pair<int,int>> bridges;

void dfs(int v){
    visited[v] = 1;
    disc[v] = tim;
    low[v] = tim;
    tim++;
    for(int j=0; j<adj[v].size(); j++){
		int i = adj[v][j];
		if(!visited[i]){
			if(parent[v] == -1){
				child++;
				if(child >= 2){
					ap[v] = 1;
				}
			}
			parent[i] = v;
			dfs(i);
			low[v] = min(low[v], low[i]);
			if( parent[v] != -1 && low[i] >= disc[v]){
				ap[v] = 1;
			}
			if(low[i] > disc[v]){
				bridges.push_back({v, i});
			}
		}
		else{
			if(parent[v] != i){
				low[v] = min(low[v], disc[i]);
			}
		}
    }
    return;
}
void printsolution(){
	int tap = 0;
	for(int i=0; i<V; i++){
		if(ap[i]){
			tap++;
		}
	}
    cout<<tap<<endl;
    if(tap > 0){
		for(int i=0; i<V; i++){
			if(ap[i]){
				printf("%d ", i);
			}
		}
		cout<<endl;
    }
    int tb = bridges.size();
	sort(bridges.begin(), bridges.end());
    cout<<tb<<endl;
    if(tb > 0){
		for(int i=0; i<tb; i++){
			printf("%d %d\n", bridges[i].first, bridges[i].second);
		}
    }
	return;
}

void f(){
	tim = 0;
	bridges.clear();
	fill(visited, visited+V, 0);
	fill(ap, ap+V, 0);
	fill(parent, parent+V, -1);
	for(int i=0; i<V; i++){
		if(!visited[i]){
			child = 0;
			dfs(i);
		}
	}
	printsolution();
	return;
}

int main(){
	cin>>V>>E;
	for(int i=0; i<V; i++){
		adj[i].clear();
	}
	int tmp = E;
	int x, y;
	while(tmp--){
		scanf("%d %d", &x, &y);
		adj[x].push_back(y);
		adj[y].push_back(x);
	}
	f();
	return 0;
}


















