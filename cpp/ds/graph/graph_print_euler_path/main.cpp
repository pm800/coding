#include <iostream>
#include <list>
using namespace std;

class Graph{
public:
    int V;
    list<int>* adj;
    Graph(int v) { V = v; adj = new list<int> [V]; };
    void addEdge(int u, int v) {  adj[u].push_back(v); adj[v].push_back(u); };
    void printEP();
    void EPUtil(int v, int* vs);
    bool validEdge(int u, int v, int* vs);
    bool bridge(int u, int v, int* vs);
    void removeEdge(int u, int v, int* vs);
    void addEdge2(int u, int v, int* vs);
    int dfs(int v);
    void dfsUtil(int v, bool* visited, int& cnt);

};

void Graph::printEP(){
    int v = 0;
    for(int i=0; i<V; i++){
        if(adj[i].size() & 1){
            v = i; break;
        }
    }
    int vs[V];
    for(int i=0; i<V; i++){
        vs[i] = adj[i].size();
    }
    EPUtil(v, vs);
    cout<<endl;
}
void Graph::EPUtil(int v, int* vs){
    for(int i: adj[v]){
        if(validEdge(v, i, vs)){
            cout<<v<<" to "<<i<<endl;
            removeEdge(v, i, vs);
            EPUtil(i, vs);
            break;
        }
    }
}
bool Graph::validEdge(int u, int v, int* vs){
    if(v==-1) return false;
    else if(vs[u] == 1) return true;
    else if(!bridge(u, v, vs)) return true;
    else return false;
}
bool Graph::bridge(int u, int v, int* vs){
    int s1 = dfs(u);
    removeEdge(u,v,vs);
    int s2 = dfs(u);
    addEdge2(u,v,vs);
    if(s1>s2) return true;
    else return false;
}
void Graph::removeEdge(int u, int v, int* vs){
    list<int>::iterator it;
    for(it = adj[u].begin(); it != adj[u].end(); it++){
        if(*it == v){
            *it = -1; break;
        }
    }
    for(it = adj[v].begin(); it != adj[v].end(); it++){
        if(*it == u){
            *it = -1; break;
        }
    }
    vs[u]--; vs[v]--;
}
void Graph::addEdge2(int u, int v, int* vs){
    list<int>::iterator it;
    for(it = adj[u].begin(); it != adj[u].end(); it++){
        if(*it == -1){
            *it = v; break;
        }
    }
    for(it = adj[v].begin(); it != adj[v].end(); it++){
        if(*it == -1){
            *it = u; break;
        }
    }
    vs[u]++; vs[v]++;
}
int Graph::dfs(int v){
    bool visited[V] = {};
    int cnt = 0;
    dfsUtil(v, visited, cnt);
    return cnt;
}
void Graph::dfsUtil(int v, bool* visited, int& cnt){
    visited[v] = true;
    cnt++;
    for(int i: adj[v]){
        if(i != -1 && (!visited[i])){
            dfsUtil(i, visited, cnt);
        }
    }
}

int main()
{
    Graph g1(4);
    g1.addEdge(0, 1);
    g1.addEdge(0, 2);
    g1.addEdge(1, 2);
    g1.addEdge(2, 3);
    g1.printEP();

    Graph g2(3);
    g2.addEdge(0, 1);
    g2.addEdge(1, 2);
    g2.addEdge(2, 0);
    g2.printEP();

    Graph g3(5);
    g3.addEdge(1, 0);
    g3.addEdge(0, 2);
    g3.addEdge(2, 1);
    g3.addEdge(0, 3);
    g3.addEdge(3, 4);
    g3.addEdge(3, 2);
    g3.addEdge(3, 1);
    g3.addEdge(2, 4);
    g3.printEP();
    return 0;
}
