#include <iostream>
#include <list>
#include <vector>
#include <stack>

using namespace std;

class Graph{//directed, connected
public:
    int V;
    list<int>* adj;
    Graph(int v);
    void addEdge(int v, int u);
    void motherVertices();
    void kosarajuDfsUtil(int i, stack<int>& stk, bool* visited);
    void kosarajusDfsUtil2(int v, bool* visited, list<list<int>>& ans, list<int>* radj);
    void dfs(int v, bool* visited, int& cnt);
};

Graph::Graph(int v){
    V = v;
    adj = new list<int> [V];
}

void Graph::kosarajusDfsUtil2(int v, bool* visited, list<list<int>>& ans, list<int>* radj){
    visited[v] = true;
    ans.back().push_back(v);
    list<int>::iterator it;
    for(it = radj[v].begin(); it != radj[v].end(); it++){
        if(visited[*it] == false)
            kosarajusDfsUtil2(*it, visited, ans, radj);
    }
}

void Graph::addEdge(int v, int u){
    adj[v].push_back(u);
}

void Graph::kosarajuDfsUtil(int i, stack<int>& stk, bool* visited){
    visited[i] = true;
    list<int>::iterator it;
    for(it = adj[i].begin(); it != adj[i].end(); it++){
            if(visited[*it] == false)
                kosarajuDfsUtil(*it, stk, visited);
    }
    stk.push(i);
}

void Graph::dfs(int v, bool* visited, int& cnt){
    visited[v] = true;
    cnt++;
    list<int>::iterator it;
    for(it = adj[v].begin(); it != adj[v].end(); it++){
        if(visited[*it] == false)
            dfs(*it, visited, cnt);
    }
}

void Graph::motherVertices(){
    stack<int> stk;
    bool visited[V] = {};
    for(int i=0; i<V; i++){
        if(visited[i] == false)
            kosarajuDfsUtil(i, stk, visited);
    }

    int cnt = 0;
    fill_n(visited, V, false);
    dfs(stk.top(), visited, cnt);

    if(cnt < V){
        cout<<"no mother vertex"<<endl;
        return;
    }

    list<int>* radj;
    radj = new list<int> [V];
    for(int i=0; i<V; i++){
        list<int>::iterator it;
        for(it = adj[i].begin(); it != adj[i].end(); it++){
            radj[*it].push_back(i);
        }
    }
    fill_n(visited, V, false);
    list<list<int>> ans;
    int v = stk.top();
    ans.push_back({});
    kosarajusDfsUtil2(v, visited, ans, radj);

    list<list<int>>::iterator it;
    list<int>::iterator it2;
    for(it = ans.begin(); it != ans.end(); it++){
        for(it2 = it->begin(); it2 != it->end(); it2++){
            cout<<*it2<<" ";
        }cout<<endl;
    }
}

int main()
{
    Graph g(7);
    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(1, 3);
    g.addEdge(4, 1);
    g.addEdge(6, 4);
    g.addEdge(5, 6);
    g.addEdge(5, 2);
    g.addEdge(6, 0);
    g.addEdge(0, 5);
    g.motherVertices();
    return 0;
}


























