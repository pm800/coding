#include <iostream>
#include <unordered_map>
#include <climits>
using namespace std;

string s;
int k;

void f(){
	unordered_map<char, int> mp;
    int n = s.size();
    int i=0, j=0, cnt = 0, mx = INT_MIN;
    while(j<n){
		char c = s[j];
		mp[c]++;
		if(mp[c] == 1)
			cnt++;
		while(cnt > k){
			char c = s[i];
			mp[c]--;
			if(mp[c] == 0)
				cnt--;
			i++;
		}
		if(cnt == k){
			if ( (j-i+1) > mx )
				mx = j-i+1;
		}
		j++;
    }
    if(mx != INT_MIN)
		cout<<mx<<endl;
	else
		cout<<-1<<endl;
}


int main()
{
	int tcs;
	cin>>tcs;
	while(tcs--){
		cin>>s>>k;
		f();
	}
    return 0;
}
