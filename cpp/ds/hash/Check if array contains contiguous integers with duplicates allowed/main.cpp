#include <iostream>
#include <unordered_set>
#include <climits>
using namespace std;


int n, arr[100005];

bool f(){
    int s = INT_MAX, b = INT_MIN;
    unordered_set<int> v;
    for(int i=0; i<n; i++){
		int x = arr[i];
		v.insert(x);
		if(x<s)
			s = x;
		if(x>b)
			b = x;
    }
    if(v.size() != b-s+1)
		return 0;
	return 1;
}


int main()
{
	int tcs;
	cin >>tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		if(f())
			cout<<"Yes"<<endl;
		else
			cout<<"No"<<endl;
	}
    return 0;
}
