#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
	int tcs;
	cin>>tcs;
	while(tcs--){
		int n;
		cin>>n;
		int arr[n];
        for(int i=0; i<n; i++){
        	cin >>arr[i];
        }
        sort(arr, arr+n);
        bool f = 0;
        for(int i=0; i<n; i++){
			int x = arr[i];
			bool flag = 0;
			while(x>0){
				int z = x%10;
				if(z!= 1&& z != 2 && z != 3){
					flag = 1;
					break;
				}
				x = x/10;
			}
			if(flag==0){
				f = 1;
				cout<<arr[i]<<" ";
			}
        }
        if(f == 0)
			cout<<-1;
		cout<<endl;
	}
    return 0;
}
