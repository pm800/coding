#include <iostream>
#include <unordered_map>
using namespace std;

int n, arr[10005], k=0;


void f(){
	unordered_map<int,int> mp;
	mp[0] = 1;
	int sm = 0, ans = 0;
	for(int i=0; i<n; i++){
		sm += arr[i];
		if(mp.find(sm-k) != mp.end())
			ans += mp[sm-k];
		mp[sm]++;
	}
	cout<<ans<<endl;
}

int main()
{
	int tcs;
	cin>>tcs;
	while(tcs--){
		cin >>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		f();
	}
    return 0;
}
