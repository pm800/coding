#include <iostream>
#include <unordered_map>
using namespace std;

int a[105], b[105], n;

bool g(){
	unordered_map<int, int> am, bm;
	unordered_map<int, int>::iterator it, got;
	for(int i=0; i<n; i++){
		am[a[i]]++;
	}
	for(int i=0; i<n; i++){
		int x = b[i];
		got = am.find(x);
		if(got == am.end())
			return 0;
		if(got->second == 0)
			return 0;
		got->second--;
	}
	return 1;
}

int main()
{
    int tcs;
    cin >>tcs;
    while(tcs--){
		cin >>n;
		for(int i=0; i<n; i++)
			cin >>a[i];
		for(int i=0; i<n; i++)
			cin >>b[i];
		cout<<g()<<endl;
    }
    return 0;
}
