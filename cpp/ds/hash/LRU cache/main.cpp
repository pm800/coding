#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl

using namespace std;

class LRUCache
{
public:
    LRUCache(int );

    int get(int );

    void set(int , int );
};
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
    int n;
    cin>>n;
    LRUCache *l  = new LRUCache(n);
    int q;
    cin>>q;
    for(int i=0;i<q;i++)
    {
        string a;
        cin>>a;
        if(a=="SET")
        {
            int aa,bb;
            cin>>aa>>bb;
            l->set(aa,bb);
        }else if(a=="GET")
        {
            int aa;
            cin>>aa;
            cout<<l->get(aa)<<" ";
        }
    }
    cout<<endl;
    }
}



/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/*The structure of the class is as follows
class LRUCache
{
public:
    LRUCache(int );
    int get(int );
    void set(int , int );
};*/
/*You are required to complete below methods */
/*Inititalize an LRU cache with size N */


int k;
unordered_map<int, list<pair<int,int>>::iterator> m;
list<pair<int,int>> l;

LRUCache::LRUCache(int N)
{
	m.clear();
	l.clear();
	k = N;
}
/*Sets the key x with value y in the LRU cache */
void LRUCache::set(int x, int y)
{
	if(m.find(x) != m.end()){
		auto mit = m.find(x);
		auto it = mit->second;
		l.erase(it);
	}
	else{
        if(l.size() >= k){
			int p = l.back().first;
			l.pop_back();
			m.erase(p);
        }
	}
	l.push_front({x, y});
	m[x] = l.begin();
	return;
}
/*Returns the value of the key x if
present else returns -1 */
int LRUCache::get(int x)
{
	if(m.find(x) != m.end()){
		auto mit = m.find(x);
		auto it = mit->second;
		int val = it->second;
		l.erase(it);
		l.push_front({x, val});
		m[x] = l.begin();
		return val;
	}
	else{
		return -1;
	}
}


