#include <iostream>
#include <unordered_set>
#include <algorithm>
using namespace std;


int a[1005];
int b[1005];

int main()
{
	int tcs;
	cin >> tcs;
	while(tcs--){
		int n, m, x;
		cin >>n >>m>>x;
		for(int i=0; i<n;i++)
			cin>>a[i];
		sort(a, a+n);
		for(int j=0; j<m; j++)
			cin>>b[j];
		unordered_set<int> s;
		for(int i=0; i<m ; i++)
			s.insert(b[i]);
		bool flag = 0;
		for(int i=0; i<n ; i++){
            int y = x - a[i];
            unordered_set<int>::iterator got = s.find(y);
            if(got != s.end() && flag == 0){
				flag = 1;
				cout<<a[i]<<" "<<y;
            }
            else if(got != s.end() && flag == 1){
				cout<<", "<<a[i]<<" "<<y;
            }
		}
		if(flag==0)
			cout<<-1;
		cout<<endl;
	}
    return 0;
}
