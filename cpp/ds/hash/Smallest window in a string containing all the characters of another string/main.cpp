#include <iostream>
#include<unordered_map>
#include <climits>
using namespace std;

string s, t;

void f(){
	unordered_map<char, int> mp;
	for(char c: t){
		mp[c]++;
	}
    int n = t.size();
    int m = s.size();
    int i=-1, j=-1, cnt = 0;
    int mn = INT_MAX, mi, mj;
    while(j<m){
		while(cnt<n && j<m){
			j++;
			char c = s[j];
			if( mp.find(c) != mp.end()){
                mp[c]--;
                if(mp[c]>=0)
					cnt++;
			}
		}
		if(cnt != n)
			break;
		while(cnt > n-1){
			i++;
			char c = s[i];
			if( mp.find(c) != mp.end()){
                mp[c]++;
				if(mp[c] > 0)
					cnt--;
			}
		}
		if( (j-i+1) < mn ){
			mn = j-i+1;
			mi = i;
			mj = j;
		}
    }
    if(mn != INT_MAX){
		for(int i=mi; i<=mj; i++)
			cout<<s[i];
		cout<<endl;
    }
    else{
		cout<<-1<<endl;
    }

}

int main()
{
	int tcs;
	cin>>tcs;
	while(tcs--){
		cin>>s>>t;
		f();
	}
    return 0;
}
