#include <iostream>
#include <string>
#include <map>
#include<vector>
using namespace std;



void f(int n, int d){
	cout<<n/d;
	int r = n%d;
	if(r==0){
		cout<<endl;
        return;
	}
	cout<<".";
	map<int, int> mp;
	vector<int> v;
    int ind=0, rep = -1;
    while(r>0){
		mp[r] = ind;
		ind++;
		v.push_back((r*10)/d);
		r = (r*10)%d;
		if(mp.find(r) != mp.end()){
			rep = mp[r];
			break;
		}
    }
    if(rep == -1){
		for(int i: v)
			cout<<i;
		cout<<endl;
    }
    else{
        for(int i=0; i<=rep-1; i++)
			cout<<v[i];
		cout<<"(";
		for(int i=rep; i<v.size(); i++)
			cout<<v[i];
		cout<<")"<<endl;
    }
    return;
}


int main()
{
	int tcs;
	cin >>tcs;
	while(tcs--){
		int n, d;
		cin>>n>>d;
		f(n,d);
	}
    return 0;
}
