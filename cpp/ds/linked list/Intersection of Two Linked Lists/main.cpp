
#include<bits/stdc++.h>
using namespace std;
struct node
{
	int data;
	struct node* next;
};
void printList(struct node *node)
{
    while(node!=NULL){
        cout<<node->data<<' ';
        node = node->next;
    }
    printf("
");
}
void push(struct node** head_ref, int new_data)
{
    struct node* new_node =	(struct node*) malloc(sizeof(struct node));
    new_node->data = new_data;
    new_node->next = (*head_ref);
    (*head_ref) = new_node;
}
struct node* findIntersection(struct node* head1, struct node* head2);
int main()
{
    long test;
    cin>>test;
    while(test--)
    {
        struct node* a = NULL;
        struct node* b = NULL;
        int n, m, tmp;
        cin>>n;
        for(int i=0; i<n; i++)
        {
            cin>>tmp;
            push(&a, tmp);
        }
        cin>>m;
        for(int i=0; i<m; i++)
        {
            cin>>tmp;
            push(&b, tmp);
        }
        printList(findIntersection(a, b));
    }
    return 0;
}



/*Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function is mentioned above.*/

/*
structure of the node is as
struct node
{
	int data;
	struct node* next;
};
*/
struct node* findIntersection(struct node* head1, struct node* head2)
{
    map<int, int> m1, m2;
    node* curr = head1;
    while(curr){
		m1[curr->data]++;
		curr = curr->next;
    }
    curr = head2;
    while(curr){
		m2[curr->data]++;
		curr = curr->next;
    }

    node* res = new node;
    res->next = NULL;
    node* t = res;
    for(auto it = m1.begin(); it != m1.end(); it++){
		int data = it->first;
		int f = it->second;
		if(m2.find(data) != m2.end()){
			int mm = min(f, m2[data]);
			while(mm--){
				t->next = new node;
				t->next->next = NULL;
				t = t->next;
				t->data = data;
			}
		}
    }
    return res->next;
}






















