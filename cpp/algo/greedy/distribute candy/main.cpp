#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

int f(vector<int>& arr){
	vector<pair<int,int>> v;
	for(int i=0; i<arr.size(); i++)
		v.push_back({arr[i], i});
	sort(v.begin(), v.end());
	int n = arr.size();
	int ans[n];
	fill(ans, ans+n, 1);
	for(int i=0; i<n; i++){
		int j = v[i].second;
		if( (j-1) >= 0 ){
			if(arr[j-1] < arr[j] ){
				ans[j] = ans[j-1] + 1;
			}
//			else if(arr[j-1] == arr[j]){
//				ans[j] = ans[j-1];
//			}
		}
		if( (j+1) < n ){
			if(arr[j+1] < arr[j] ){
				ans[j] = max( ans[j], ans[j+1]+1 );
			}
//			else if(arr[j+1] == arr[j]){
//				ans[j] = max( ans[j], ans[j+1] );
//			}
		}
	}
	int sm = 0;
	for(int i=0; i<n; i++)
		sm += ans[i];
	return sm;
}

int main(){
//	int tcs;
//	cin >> tcs;
//	while(tcs--){
//
//	}
	return 0;
}


