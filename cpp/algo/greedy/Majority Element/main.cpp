#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

int n, arr[105];

int f(){
	int cnt = 1;
	int me = arr[0];
	for(int i=1; i<n; i++){
		if(cnt == 0){
			me = arr[i];
			cnt++;
			continue;
		}
		if(arr[i] == me){
			cnt++;
		}
		else{
			cnt--;
		}
	}
	cnt = 0;
	for(int i=0; i<n; i++){
		if(arr[i] == me)
			cnt++;
	}
	if(cnt >= n/2+1)
		return me;
	else
		return -1;
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		int ans = f();
		if(ans == -1)
			cout<<"NO Majority Element"<<endl;
		else
			cout<<ans<<endl;
	}
	return 0;
}


