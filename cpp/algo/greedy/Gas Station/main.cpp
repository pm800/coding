#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

class Solution{
public:
	int canCompleteCircuit(const vector<int> &A, const vector<int> &B);
};

int Solution::canCompleteCircuit(const vector<int> &A, const vector<int> &B) {
    int n = A.size();
    int arr[n];
    for(int i=0; i<n; i++)
		arr[i] = A[i] - B[i];
    int i=0, j=0, sm = arr[0], cnt = 1;
    while(i<n){
        if(cnt == n && sm >= 0){
			return i;
        }
        if(sm < 0){
			if(i == j){
				i++; j++;
				if(i >= n){
					return -1;
				}
				sm = arr[i];
				cnt = 1;
			}
			else{
				sm -= arr[i];
				i++;
				cnt--;
			}
        }
        else{
			if(j == n-1){
				j  = 0;
			}
			else{
				j++;
			}
            sm += arr[j];
            cnt++;
        }
    }
    return -1;
}


int main(){
	return 0;
}


