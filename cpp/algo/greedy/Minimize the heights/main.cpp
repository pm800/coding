#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

int k, n, arr[35];

int f(){
    sort(arr, arr+n);
    int ans = arr[n-1] - arr[0];
    int c = arr[0] + k;
    int d = arr[n-1] - k;
    int a, b, mm, mx;
    for(int i=0; i<(n-1); i++){
		a = arr[i] + k;
		b = arr[i+1] - k;
		mm = min(a, min(b, min(c, d)));
		mx = max(a, max(b, max(c, d)));
		ans = min(ans, (mx - mm));
    }
	return ans;
}



int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>k>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		cout<<f()<<endl;
	}
	return 0;
}


