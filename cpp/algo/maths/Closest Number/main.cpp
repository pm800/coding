#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

int n, m;

int f(){
	if(m == 0)
		return -1;
	if(n == 0)
		return abs(m);
	if(n == m)
		return abs(n) + 1;
	int x, y, c;
	if(abs(n) < abs(m)){
        x = 0;
        y = abs(m);
        if(n < 0)
			y *= -1;
	}
	else{
		int p = abs(n/m);
		x = abs(m*p);
		y = abs(m*(p+1));
		if(n < 0){
			x *= -1;
			y *= -1;
		}
	}
	if( abs(x-n) < abs(y-n) )
		c = x;
	else if(  abs(x-n) > abs(y-n) )
		c = y;
	else{
		if(abs(x) >= abs(y))
			c = x;
		else
			c = y;
	}
	return c;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n>>m;
		cout<<f()<<endl;
	}
	return 0;
}


