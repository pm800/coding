#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

int n;

int f(){
    string s = to_string(n);
    int m = s.size();
    if(m == 1)
		return n;
	string ts, s1, s2, s3, rs1, rs2, rs3;
    int ti, t1, t2, t3, t4, t5;
    ts.assign(s, 0, m/2);
    s3 = ts;
    rs3 = s3;
    reverse(rs3.begin(), rs3.end());
    t1 = 0; t2 = 0; s1 = ""; s2 = "";
    if(m%2 != 0){
		s1 = s3; s2 = s3;
		char c = s[m/2];
		s3.push_back(c);
		if(c == '1'){
			s1.push_back(c+1);
		}
		else if(c == '9'){
			s2.push_back(c-1);
		}
		else{
			s1.push_back(c+1);
			s2.push_back(c-1);
//			cout<<s1<<" "<<s2<<endl;
		}
    }
	s3 += rs3;
	t3 = stoi(s3);

	int ans = 10*(m-1) -1;

    if(!s2.empty()){
		s2 += rs3;
		t2 = stoi(s2);
//		cout<<t2<<" "<<ans<<" "<<t1<<endl;
		if(abs(n-t2) < abs(n-ans)){
			ans = t2;
		}
    }
	if(abs(n-t3) < abs(n-ans))
		ans = t3;
    if(!s1.empty()){
		s1 += rs3;
		t1 = stoi(s1);
		if(abs(n-t1) < abs(n-ans))
			ans = t1;
    }

	t4 = 10*m + 1;
	if(abs(n-t4) < abs(n-ans))
		ans = t4;

	return ans;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		cout<<f()<<endl;
	}
	return 0;
}


//int f(){
//    string s = to_string(n);
//    int m = s.size();
//    if(m == 1)
//		return n;
//	string ts, s1, s2, s3, rs1, rs2, rs3;
//    int ti, t1, t2, t3, t4, t5;
//    ts.assign(s, 0, m/2);
//    ti = stoi(ts);
//    t1 = ti - 1;
//    t2 = ti + 1;
//    s1 = to_string(t1);
//    rs1 = s1;
//    reverse(rs1.begin(), rs1.end());
//    s2 = to_string(t2);
//    rs2 = s2;
//    reverse(rs2.begin(), rs2.end());
//    s1 += rs1;
//    s2 += rs2;
//    t1 = stoi(s1);
//    t2 = stoi(s2);
//    s3 = ts;
//    rs3 = s3;
//    reverse(rs3.begin(), rs3.end());
//    if(m%2 != 0)
//		s3.push_back(s[m/2]);
//	s3 += rs3;
//	t3 = stoi(s3);
//
//	int ans = t1;
//	if(abs(n-t2) < abs(n-ans))
//		ans = t2;
//	if(abs(n-t3) < abs(n-ans))
//		ans = t3;
//	if(m%2 == 0){
//		t4 = 10*m + 1;
//		if(abs(n-t4) < abs(n-ans))
//			ans = t4;
//        t5 = 10*(m-1) -1;
//		if(abs(n-t5) < abs(n-ans))
//			ans = t5;
//	}
//	return ans;
//}

