#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;
#define MAX_FACT 20


string s, os;
int n, len=0, sz;
map<char, int> m;

int fact[MAX_FACT];

// utility for calculating factorials
void facf()
{
    fact[0] = 1;
    for (int i = 1; i < MAX_FACT; i++)
        fact[i] = fact[i - 1] * i;
}


int g(){
	int ans;
	ans = fact[sz-len];
	for(auto it = m.begin(); it != m.end(); it++){
		ans /= fact[it->second];
	}
	return ans;
}

void h(){
	for(auto it = m.rbegin(); it != m.rend(); it++){
		while(it->second--){
			os.push_back(it->first);
		}
	}
}

void f(){
    if(len == sz)
		return;
	len++;
	bool flag = 0;
    for(auto it = m.begin(); it != m.end(); it++){
		char c = it->first;
		it->second--;
		int x = g();
		if(n > x){
			n -= x;
			it->second++;
			continue;
		}
		if(n < x){
			if(it->second <= 0)
				m.erase(c);
			os.push_back(c);
			break;
		}
		if(n == x){
			if(it->second <= 0)
				m.erase(c);
			os.push_back(c);
			flag = 1;
			h();
			break;
		}
    }
    if(flag)
		return;
	f();
	return;
}

void fm(){
	m.clear();
	for(char c: s){
		m[c]++;
	}
	int x = fact[s.size()];
	for(auto it = m.begin(); it != m.end(); it++){
		x /= fact[it->second];
	}

	for(int i = 1; i<=x; i++){
		n = i;
		len = 0;
		os = "";
		sz = s.size();
		m.clear();
		for(char c: s){
			m[c]++;
		}
		f();
		cout<<os<<" ";
	}
	cout<<endl;
}


int main(){
	facf();
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>s;
		fm();
	}
	return 0;
}


