#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

int main(){
	int tcs;
	int x1, x2, y1, y2, a1, a2, b1, b2;
	cin >> tcs;
	while(tcs--){
		cin>>x1>>y1>>x2>>y2>>a1>>b1>>a2>>b2;
		if(y1 < b2 || b1 < y2){
			cout<<0<<endl;
			continue;
		}
		if(x2 < a1 || a2 < x1){
			cout<<0<<endl;
			continue;
		}
		cout<<1<<endl;
	}
	return 0;
}


