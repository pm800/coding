#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

int n;

void f(){
    stack<int> stk;
    while(n > 0){
		int x = n%26;
		stk.push(x);
		int y = n/26;
		if(x == 0)
			y--;
		n = y;
    }
    while(!stk.empty()){
		int x = stk.top();
		stk.pop();
		if(x == 0){
			cout<<'Z';
		}
		else{
            char c = 'A' + x - 1;
            cout<<c;
		}
    }
    cout<<endl;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		f();
	}
	return 0;
}


