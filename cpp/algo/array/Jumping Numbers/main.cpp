#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>

using namespace std;

int n;

void g(int i){
    if(i>n)
		return;
	cout<<i<<" ";
	int t = i %10;
	int y;
	if(t == 0){
		y = stoi( to_string(i) + "1" );
		g(y);
	}
	else if(t == 9){
		y = stoi( to_string(i) + "8" );
		g(y);
	}
	else{
		y = stoi( to_string(i) + to_string(t-1) );
		g(y);
		y = stoi( to_string(i) + to_string(t+1) );
		g(y);
	}
	return;
}

void f(){
	if(0<=n)
		cout<<0<<" ";
	for(int i=1; i<=9; i++)
		g(i);
	return;
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		f();
	}
	return 0;
}


