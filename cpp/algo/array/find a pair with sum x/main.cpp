#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>

using namespace std;

int n, sm;
int arr[205];

int f(){
    sort(arr, arr+n);
    int i=0, j = n-1;
    while(i<j){
		if( (arr[i] + arr[j]) == sm )
			return 1;
		else if( (arr[i] + arr[j]) < sm )
            i++;
		else
			j--;
    }
    return 0;

}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n>>sm;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		if(f())
			cout<<"Yes"<<endl;
		else
			cout<<"No"<<endl;

	}
	return 0;
}


