#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>

using namespace std;

int n;
int arr[15];
int dp[5];

int f(){
	int ans = 0;
    fill(dp, dp+5, 0);
    for(int i=0; i<n; i++){
		int x= arr[i];
        int j=1;
        for(int p=0; p<5; p++){
            if(x & j)
				dp[p]++;
			j=j<<1;
        }
    }
	for(int i=0; i<5; i++){
		ans += dp[i] * (n-dp[i]);
	}
    return ans*2;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n;i++)
			cin>>arr[i];
		cout<<f()<<endl;
	}
	return 0;
}


