#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>

using namespace std;

int n, arr[1005];

void f(){
	int ms =-1, me = -1;
	int b = INT_MAX;
    sort(arr, arr+n);
    int s = 0, e = n-1;
    while(s<e){
		int sm = arr[s] + arr[e];
		if(abs(sm) < b){
			ms = s;
			me = e;
			b = abs(sm);
		}
		if(sm == 0){
			ms = s;
			me = e;
			break;
		}
		if(sm > 0)
			e--;
		else
			s++;
    }
    cout<<arr[ms]<<" "<<arr[me]<<endl;

}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		f();
	}
	return 0;
}


