#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>

using namespace std;

char mat[3][3];

bool f(){
	int cntx = 0;
	for(int i=0; i<3; i++)
		for(int j=0; j<3; j++)
			if(mat[i][j] == 'X')
				cntx++;
	int cnto = 9 - cntx;

	if(cntx != cnto && cntx != cnto + 1)
		return 0;


    int xc=0, oc = 0;
    for(int i=0; i<3; i++){
		if(mat[i][0] == mat[i][1] && mat[i][1] == mat[i][2]){
			if(mat[i][0] == 'X')
				xc++;
			else
				oc++;
		}
    }
    for(int i=0; i<3; i++){
		if(mat[0][i] == mat[1][i] && mat[1][i] == mat[2][i]){
			if(mat[0][i] == 'X')
				xc++;
			else
				oc++;
		}
    }
    if(mat[0][0] == mat[1][1] && mat[1][1] == mat[2][2]){
			if(mat[0][0] == 'O')
				oc++;
			else
				xc++;
    }
    if(mat[1][1] == mat[0][2] && mat[1][1] == mat[2][0]){
			if(mat[1][1] == 'O')
				oc++;
			else
				xc++;
    }

	if(xc > 0 && oc > 0)
		return 0;
	if(xc > 0 && cntx != cnto + 1)
		return 0;
	if(oc > 0 && cnto != cntx)
		return 0;
	return 1;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
				cin>>mat[i][j];
        if(f())
			cout<<"Valid"<<endl;
		else
			cout<<"Invalid"<<endl;
	}
	return 0;
}


