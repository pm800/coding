#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>

using namespace std;

int n, arr[105];

bool comp(int a, int b){
    if(a%2 == 0 && b%2 == 0)
		return a <= b;
	if(a%2 != 0 && b%2 != 0)
		return b <= a;
	if(a%2 != 0)
		return true;
	else
		return false;
}

void f(){
    sort(arr, arr+n, comp);
    for(int i=0; i<n; i++)
		cout<<arr[i]<<" ";
	cout<<endl;
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		f();
	}
	return 0;
}


