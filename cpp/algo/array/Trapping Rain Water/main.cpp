#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>

using namespace std;

int n, arr[105];
int nl[105], rm[105];

int f(){
	fill(nl, nl+n, -1);
    stack<int> s;
    s.push(0);
	for(int i=1; i<n; i++){
        while(!s.empty() && arr[s.top()] < arr[i]){
			int x = s.top();
			nl[x] = i;
			s.pop();
        }
        s.push(i);
	}

	rm[n-1] = n-1;
	for(int i = n-2; i>=0; i--){
		if(arr[i]>arr[rm[i+1]])
			rm[i] = i;
		else
			rm[i] = rm[i+1];
	}

	int i=0;
	int ans = 0;
	while(i<(n-1)){
		int x= nl[i];
		if(x == -1){
			x = rm[i+1];
		}
		int mm = min(arr[i], arr[x]);
		i++;
		while(i<x){
			ans += (mm-arr[i]);
			i++;
		}
		i = x;
	}
	return ans;
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		cout<<f()<<endl;
	}
	return 0;
}


