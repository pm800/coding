#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>

using namespace std;

int n, arr[105];
int cnt = 0;

void mrg(int i, int m, int j){
    queue<int> q1;
    for(int p=i; p<=m; p++)
		q1.push(arr[p]);
	queue<int> q2;
    for(int p=m+1; p<=j; p++)
		q2.push(arr[p]);
    while(!q1.empty() && !q2.empty()){
		if(q2.front() < q1.front()){
			cnt += q1.size();
            arr[i] = q2.front();
            i++;
            q2.pop();
		}
		else{
            arr[i] = q1.front();
            i++;
            q1.pop();
		}
    }
    while(!q1.empty()){
		arr[i] = q1.front();
		i++;
		q1.pop();
    }
    while(!q2.empty()){
		arr[i] = q2.front();
		i++;
		q2.pop();
    }
}


void ms(int i, int j){
	if(i >= j)
		return;
	int m = i+(j-i)/2;
	ms(i, m);
	ms(m+1, j);
	mrg(i, m, j);
	return;
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cnt = 0;
		cin>>n;
		for(int i=0; i<n ; i++)
			cin>>arr[i];
		ms(0, n-1);
		cout<<cnt<<endl;
	}
	return 0;
}


