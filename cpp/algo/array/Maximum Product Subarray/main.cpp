#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>

using namespace std;

int n, arr[15];

int f(){
    int bp = 0, bn = 0;
    int ans = INT_MIN;
    if(arr[0]>0){
		bp = arr[0];
		ans = arr[0];
    }
	else
		bn = arr[0];
	for(int i=1; i<n; i++){
		if(arr[i] >= 0){
			bp = max(arr[i], arr[i]*bp);
			bn = arr[i]*bn;
		}
        else {
			int temp = bn;
			bn = min(arr[i], arr[i]*bp);
			bp = arr[i]*temp;
        }
        ans = max(ans, bp);
	}
	return ans;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		cout<<f()<<endl;
	}
	return 0;
}


