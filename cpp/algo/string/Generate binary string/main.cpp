#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

string s;
int n;

void f(int i){
	if(i >= n){
		cout<<s<<" ";
		return;
	}
    for(int j = i; j<n; j++){
		if(s[j] == '?'){
			s[j] = '0';
			f(j+1);
			s[j] = '1';
			f(j+1);
			s[j] = '?';
			return;
		}
    }
    cout<<s<<" ";
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>s;
		n= s.size();
		f(0);
		cout<<endl;
	}
	return 0;
}


