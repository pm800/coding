#include <iostream>

using namespace std;

string pattern;
int lps[100000];

int lpsGenrator(){
    int n = pattern.size();
    fill_n(lps, n, 0);
    if(n==1) return 0;
    lps[0] = 0;
    int i=1, j=0;
    while(i<n){
        if(pattern[i] == pattern[j]){
            lps[i] = j+1;
            i++; j++;
            continue;
        }
        else{
            if(j==0){
                lps[i] = 0; i++;
            }
            else
                j = lps[j-1];
        }
    }
    return lps[n-1];
}

int main()
{
    int tcs;
    cin >>tcs;
    while(tcs--){
        cin >> pattern;
        cout<<lpsGenrator()<<endl;;
    }
    return 0;
}
