#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset
#define li long int
using namespace std;

int n, arr[100005];
li k;

li f(){
    queue<int> q;
    li prd = 1, ans = 0;
    for(int i=0; i<n ;i++){
		q.push(arr[i]);
		prd *= arr[i];
		if(prd < k){
			ans += q.size();
		}
		else{
			while(!q.empty() && prd >= k){
				prd /= q.front();
				q.pop();
			}
			ans += q.size();
		}
//		cout<<ans<<" "<<arr[i]<<" "<<prd<<endl;
    }
    return ans;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n>>k;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		cout<<f()<<endl;
	}
	return 0;
}


