#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

//class Solution{
//public:
//	isMatch(const string A, const string B);
//};

void g(bool* a, bool* b, int n, string pat, string txt, int i){
	a[0] = 0;
	for(int j=1; j<n; j++){
		bool ans = 0;
		if(pat[j] == '?'){
			ans  = b[j-1];
		}
		if(pat[j] != '*' && pat[j] == txt[i] ){
			ans = b[j-1];
		}
		if(pat[j] == '*'){
			ans = b[j] || a[j-1];
		}
		a[j] = ans;
	}
	return;
}

int f(const string A, const string B) {
	string txt, pat;
	txt = " "+A;
	pat = " "+B;
	int m, n;
	m = txt.size();
	n = pat.size();

    bool a[n] = {0};
    bool b[n] = {0};

	a[0] = 1;
	for(int j=1; j<n; j++)
		if(pat[j] == '*')
			a[j] = a[j-1];
    for(int i=1; i<m; i++){
		if(i & 1){
			g(b, a, n, pat, txt, i);
		}
		else{
			g(a, b, n, pat, txt, i);
		}
    }
    if((m-1) & 1){
		return b[n-1];
    }
	return a[n-1];
}

int main(){
	cout<<f("cc", "?");
	return 0;
}















