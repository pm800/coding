#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n, sm;
int res[100005];
int arr[105];


int f(){
    res[0] = 0;
    for(int i=1; i<sm+1; i++)
		res[i] = -1;
	for(int j=0; j<n; j++){
		int x = arr[j];
		for(int i=1; i<sm+1; i++){
			if( (i-x)>=0 && res[i-x] != -1  ){
                int y = 1 + res[i-x];
                if(y < res[i] || res[i] == -1)
					res[i] = y;
			}
		}
//		for(int i=0; i<sm+1; i++)
//			cout<<res[i]<<" ";
//		cout<<endl;
	}
	return res[sm];
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n>>sm;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		cout<<f()<<endl;
	}
	return 0;
}


