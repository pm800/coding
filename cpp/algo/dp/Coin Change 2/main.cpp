#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

int n, arr[100005], sm;
int dp[100005];

int f(){
	fill(dp, dp+sm+1, INT_MAX);
	dp[0] = 0;
	for(int j=0; j<n; j++){
		for(int i=arr[j]; i<=sm; i++){
			if(dp[i-arr[j]] != INT_MAX){
				dp[i] = min(dp[i], 1+dp[i-arr[j]]);
			}
		}
	}
	if(dp[sm] == INT_MAX)
		return -1;
	return dp[sm];
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n>>sm;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		cout<<f()<<endl;
	}
	return 0;
}


