#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;



int main(){

	long int mat[1003][103];

	for(int i=1; i<=9; i++)
		mat[i][1] = 1;
	for(int i=10; i<1003; i++)
		mat[i][1] = 0;
	for(int j=1; j<103; j++)
		mat[1][j] = 1;

	for(int i=2; i<1003; i++){
		for(int j=2; j<103; j++){
			long int x = 0;
			for(int p=0; p<=9 && p<i; p++){
                x += mat[i-p][j-1];
                x %= 1000000007;
			}
			mat[i][j] = x;
		}
	}


	int tcs;
	int n, sm;
	cin >> tcs;
	while(tcs--){
        cin >>n>>sm;
        if(mat[sm][n] == 0)
			cout<<-1<<endl;
		else
			cout<<mat[sm][n]%1000000007<<endl;
	}
	return 0;
}


