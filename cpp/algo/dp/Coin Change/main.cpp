#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int arr[303];
int mat[303][303];
int n, sm;


int f(){
	for(int i=1; i<(sm+1); i++){
		for(int j=n-1; j>=0; j--){
			int x = i-arr[j];
			if(x<0)
				mat[i][j] = 0;
			else
				mat[i][j] = mat[x][j];
		}
		for(int j=n-2; j>=0; j--)
			mat[i][j] += mat[i][j+1];
	}
//	for(int i=0; i<(sm+1); i++){
//		for(int j=0; j<n; j++)
//			cout<<mat[i][j]<<" ";
//		cout<<endl;
//	}
	return mat[sm][0];
}

int main(){
	for(int i=0; i<303; i++)
		mat[0][i] = 1;
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
        cin>>sm;
        cout<<f()<<endl;
	}
	return 0;
}


