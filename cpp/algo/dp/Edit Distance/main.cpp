#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int m, n;
string s1, s2;
int mat[102][102];


int f(){
    for(int i=1; i<=n; i++){
		for(int j=1; j<=m; j++){
			if(s1[j-1] == s2[i-1])
				mat[i][j] = mat[i-1][j-1];
			else{
                int x = min(mat[i-1][j], mat[i][j-1]);
                x = min(x, mat[i-1][j-1]);
                mat[i][j] = x+1;
			}
		}
    }

	return mat[n][m];
}

int main(){
	for(int i = 0; i<102; i++){
		mat[0][i] = i;
	}
	for(int i=0; i<102; i++)
		mat[i][0] = i;
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin >> m>>n;
		cin>>s1>>s2;
		cout<<f()<<endl;
	}
	return 0;
}


