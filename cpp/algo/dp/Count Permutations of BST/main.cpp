#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset
#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define p5(x, y, a, b, c) cout << x << " "<<y<<  " "<<a<<" "<<b<< " "<<c<<endl
#define p6(x, y, a, b, c, d) cout << x << " "<<y<<  " "<<a<<" "<<b<< " "<<c<<" "<<d<<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl

using namespace std;
//////////////////////////////


class Solution{
	public:
		int cntPermBST(int A, int B);
};

////////////////////////////////////

#define NMAX 52
int dp[NMAX][NMAX];

int g(int n, int h){
    if(h == 0){
		if(n == 0){
			return 1;
		}
		return 0;
    }
    if(h > n || h < 0){
		return 0;
    }
    if(dp[h][n] != -1){
		return dp[h][n];
    }
    int ans = 0;
    int x, y;
    for(int i = 0; i <= n-1; i++){
		x = i;
		y = n - x - 1;
        int lsm = 0;
        for(int p=0; p <= h-2; p++){
			lsm += g(x, p);
        }
        ans += lsm * g(y, h-1);

        int rsm = 0;
        for(int p=0; p <= h-2; p++){
			rsm += g(y, p);
        }
        ans += g(x, h-1) * rsm;

        ans += g(x, h-1) * g(y, h-1);
    }
    dp[h][n] = ans;
    return ans;
}

int Solution::cntPermBST(int n, int h) {
	h = h + 1;
	for(int k=0; k<h+1; k++){
		for(int i=0; i<n+1; i++){
			dp[k][i] = -1;
		}
	}
    int res = g(n, h);
	return res;
}

//////////////////////////////
int main(){
	Solution s;
	cout<<s.cntPermBST(4, 2)<<endl;
	return 0;
}


















