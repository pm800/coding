#include <iostream>
#include <vector>
#include <climits>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int m;
string s;
vector<int> v;

bool g(int i){
    if(v[i] > 9)
		return 0;
	if( ( v[i-1]*10 + v[i] ) <= 26  )
		return 1;
	return 0;
}

int f(){
	if(check() == 0)
		return 0;
	v = {};
	for(int i=0; i<m-1; i++){
		if(s[i] == '0')
			return 0;
        string t = "";
        t+= s[i];
        if(s[i+1] == '0'){
			t += s[i+1];
			int x = stoi(t);
			if(x>26)
				return 0;
			v.push_back(x);
			i++;
        }
        else
			v.push_back(stoi(t));
	}
	if(s[m-1] != '0')
		v.push_back(s[m-1] - '0');
	else if(m-2 >= 0 && s[m-2] == '0')
		return 0;
    if(s=="0")
		return 0;


    int n = v.size();
    if(n == 1)
		return 1;
	int p, c;
	p = 1; c = 1;
	if(g(1))
		c++;
	for(int i=2; i<n; i++){
        int temp = c;
        if(g(i))
			temp += p;
		p = c;
		c = temp;
	}
	return c;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>m;
		cin>>s;
        cout<<f()<<endl;
	}
	return 0;
}


