#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n, m;
long long int a1[1003], a2[1003];
long long int mat[1003][1003];

long int f(){
    for(int i=0; i<m+1; i++)
		for(int j=0; j<n+1; j++)
            mat[i][j] = 0;
	int x = n-(m-1);
	for(int i = m-1,j = n-1;   i>=0;   i--,j--){
		int c = j, y = x;
		while(y--){
			mat[i][c] = max( mat[i][c+1], mat[i+1][c+1] + a1[c] * a2[i] );
			c--;
		}
	}
	return mat[0][0];
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin >>n>>m;
		for(int i=0; i<n; i++)
			cin>>a1[i];
		for(int i=0; i<m; i++)
			cin>>a2[i];
		cout<<f()<<endl;

	}
	return 0;
}


