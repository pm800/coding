#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl


using namespace std;

int V, E;
vector<vector<int>> g;
int ans = 0;


void isCyclicUtil(int v, bool* vist, bool *rstk){
	if(vist[v]){
		if(rstk[v]){
			ans++;
			return;
		}
	}
	vist[v] = true;
	rstk[v] = true;
    for(int i: g[v]){
		isCyclicUtil(i, vist, rstk);
    }
    rstk[v] = false;
    return;
}

void isCyclic(){
    bool vist[V+1] = {0};
    bool rstk[V+1] = {0};

    for(int i = 0; i < V; i++){
		if(vist[i] == false){
			isCyclicUtil(i, vist, rstk);
		}
    }
    return;
}

int build(int v, int e, int** mat){
	//////////
	int* m = mat;
    for(int i=0; i<e; i+=3){
		int u = *(m+i), v = *(m+i+1);
		p4("u", u, "v", v);
    }
	///////////
	ans = 0;
	V = v;
	E = e;
	g.clear();
	g.resize(V+1, vector<int>());
	for(int i=0; i<E; i++){
		int u = mat[i][0];
		int v = mat[i][1];
		g[u].push_back(v);
	}
	isCyclic();
	return ans;
}

int main(){
	int **mat;(){
	mat = new int*[5];
	for(int i = 0; i <5; i++)
		mat[i] = new int[2];
	mat[0][0] = 1;
	mat[0][1] = 2;
	mat[1][0] = 2
	mat[1][1] = 3;
	mat[2][0] = 3;
	mat[2][1] = 4;
	mat[3][0] = 4;
	mat[3][1] = 1;
	mat[4][0] = 3;
	mat[4][1] = 1;

	cout<<build(4, 5, (int**)mat);
    return 0;
}


