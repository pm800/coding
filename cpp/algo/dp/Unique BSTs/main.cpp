#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n;
int mat[13][13];

int f(){
    for(int c = 2; c < n+1; c++){
		int i = 1, j = c;
		while(i<n+1 && j<n+1){
			int ans = 0;
			ans += mat[i+1][j];
			for(int p=i+1; p<=j-1; p++){
				ans += mat[i][p-1] * mat[p+1][j];
			}
			ans += mat[i][j-1];
			mat[i][j] = ans;
			i++; j++;
		}
    }
    return mat[1][n];
}

int main(){
	for(int i=1; i<13; i++)
		mat[i][i] = 1;
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		cout<<f()<<endl;
	}
	return 0;
}


