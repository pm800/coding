#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl


using namespace std;

int main(){
	return 0;
}


vector<int> arr;
vector<vector<vector<int>>> dp;

int f(int l, int r, bool me){
    if( r <= l){
		return  0;
    }
    if(dp[l][r][me] != -1){
		return dp[l][r][me];
    }
    if(me == 1){
		int lm = arr[l];
		lm += f(l+1, r, 0);
		int rm = arr[r];
		rm += f(l, r-1, 0);
		dp[l][r][me] = max(lm, rm);
		return dp[l][r][me];
    }
    else{
		int lm, rm;
		lm = f(l+1, r, 1);
		rm = f(l, r-1, 1);
		dp[l][r][me] = min(lm, rm);
		return dp[l][r][me];
    }
}

int Solution::maxcoin(vector<int> &v) {
	int n = v.size();
	dp.clear();
	dp.resize(n, vector<vector<int>>(n, vector<int>(2, -1)));
	arr = v;
	return f(0, n-1, 1);
}

//////////////////////////////////////////////////

//vector<int> arr;
//
//int f(int l, int r, int m){
//    if( r == l+1){
//		return  m + max(arr[l], arr[r]);
//    }
//    int ml, mr, fl, fr;
//    ml = m + arr[l];
//    mr = m + arr[r];
//    fl = max( f(l+2, r, ml), f(l+1, r-1, ml) );
//    fr = max( f(l+1, r-1, mr), f(l, r-2, mr) );
//    return min(fl, fr);
//}
//
//int Solution::maxcoin(vector<int> &v) {
//	arr = v;
//	return f(0, v.size()-1, 0);
//}






















