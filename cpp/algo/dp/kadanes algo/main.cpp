#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int arr[1005], rarr[1005], n;

void f(){
    rarr[0] = arr[0];
    int mxm = arr[0];
    for(int i=1; i<n; i++){
		rarr[i] = max(arr[i], arr[i]+rarr[i-1]);
		mxm = max(mxm, rarr[i]);
    }
    cout<<mxm<<endl;
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin >> n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		f();
	}
	return 0;
}

