#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>

using namespace std;

int m, n;
int mat[13][13];
int dp[13][13];

int f(){
    if(mat[m-1][n-1] >= 1)
		dp[m-1][n-1] = 0;
	else
		dp[m-1][n-1] = 1- mat[m-1][n-1];

	for(int j = n-2; j>=0; j--){
        int p = mat[m-1][j] - dp[m-1][j+1];
        if(p >= 1)
			dp[m-1][j] = 0;
		else
			dp[m-1][j] = 1-p;
	}

	for(int i=m-2; i>=0; i--){
		int p = mat[i][n-1] - dp[i+1][n-1];
		if(p >= 1)
			dp[i][n-1] = 0;
		else
			dp[i][n-1] = 1-p;
	}

    for(int i=m-2; i>=0; i--){
		for(int j=n-2; j>=0; j--){
			int mm = min(dp[i][j+1], dp[i+1][j]);
            int p = mat[i][j] - mm;
            if(p >= 1)
				dp[i][j] = 0;
			else
				dp[i][j] = 1-p;
		}
    }
    return dp[0][0];

}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>m>>n;
		for(int i=0; i<m; i++)
			for(int j=0; j<n; j++)
				cin>>mat[i][j];
		int ans = f();
		if(ans == 0)
			cout<<1<<endl;
		else
			cout<<ans<<endl;
	}
	return 0;
}


