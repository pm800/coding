#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n;
string arr[14];
int m;
string s;
int dp[1005];

int g(int i, int j){
	for(char c: arr[j]){
		if(i >= m)
			return -1;
		if(c != s[i])
			return -1;
		i++;
	}
	return i;
}


int f(int i){
    if(i>=m){
		dp[i] = 1;
		return 1;
    }
	for(int j=0; j<n; j++){
		int p = g(i,j);
		if(p != -1){
			if(dp[p] != -1){
				if(dp[p] == 1){
					dp[i] = 1;
					return 1;
				}
			}
			else{
				if( f(p) == 1 ){
					dp[i] = 1;
					return 1;
				}
			}
		}
	}
	dp[i] = 0;
	return 0;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin >> n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		cin>>s;
		m = s.size();
		for(int i=0; i<m+3; i++)
			dp[i] = -1;
		cout<<f(0)<<endl;
	}
	return 0;
}


