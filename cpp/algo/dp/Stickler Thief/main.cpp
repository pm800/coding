#include <iostream>
#include <vector>
#include <climits>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n;
int arr[1005];
int dp[1005];

int f(){
    if(n>=1){
		dp[0] = arr[0];
    }
    if(n>=2){
		dp[1] = max(arr[0], arr[1]);
    }
    for(int i=2; i<n; i++){
		dp[i] =  max( dp[i-1],  (arr[i] + dp[i-2]) );
    }
    return dp[n-1];
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin >> n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		cout<<f()<<endl;
	}
	return 0;
}


