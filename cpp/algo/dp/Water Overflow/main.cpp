#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <iomanip>
using namespace std;

vector<vector<double>> v;
int r, c;

void g(int i, int j, double k){
    if(i>=r)
		return;
	v[i][j] += k;
	if(v[i][j] > 1){
		double x = v[i][j] -1;
		v[i][j] = 1;
		g(i+1, j, x/2);
		g(i+1, j+1, x/2);
	}
	return;
}


double f(double k){
	v = {};
	for(int i=0; i<r; i++){
		v.push_back({});
	}
	for(int i=0; i<r; i++){
		int x = i+1;
		while(x--)
			v[i].push_back(0);
	}
	g(0,0,k);
	return v[r-1][c-1];

}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		int k;
		cin>>k;
		cin>>r>>c;
		cout<< setprecision(6)  <<f(k)<<endl;
	}
	return 0;
}


