#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

//class Solution{
//public:
//	isMatch(const string A, const string B);
//};

int f(const string A, const string B) {
	string txt, pat;
	txt = " "+A;
	pat = " "+B;
	int m, n;
	m = txt.size();
	n = pat.size();
	bool mat[m][n];
	fill((bool*)mat, (bool*)mat+m*n, 0);
	mat[0][0] = 1;
	for(int j=1; j<n; j++)
		if(pat[j] == '*')
			mat[0][j] = mat[0][j-2];
    for(int i=1; i<m; i++){
		for(int j=1; j<n; j++){
			bool ans = 0;
			if(pat[j] == '.'){
				ans  = mat[i-1][j-1];
			}
			if(pat[j] != '*' && pat[j] == txt[i] ){
				ans = mat[i-1][j-1];
			}
			if(pat[j] == '*'){
                ans = mat[i][j-2];
                if(txt[i] == pat[j-1] || pat[j-1] == '.'){
					ans = ans || mat[i-1][j];
                }
			}
			mat[i][j] = ans;
		}
    }
	return mat[m-1][n-1];
}

int main(){
	cout<<f("a", "a");
	return 0;
}















