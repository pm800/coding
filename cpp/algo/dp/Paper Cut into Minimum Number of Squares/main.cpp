#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int dp[300][300];

int f(int m, int n){
    if(m == n)
        return 1;
	if(dp[m][n] != 0)
		return dp[m][n];
	int mm = INT_MAX;
	for(int i=1; i<=m/2; i++){
		int c = f(i, n)+f(m-i, n);
		if(c<mm)
			mm = c;
	}
	for(int i=1; i<=n/2; i++){
		int c = f(m, i)+f(m, n-i);
		if(c<mm)
			mm = c;
	};
	dp[m][n] = mm;
	return mm;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		int m,n;
		cin>>m>>n;
		for(int i=0; i<=m; i++)
			for(int j=0; j<=n; j++)
				dp[i][j] = 0;
		cout<<f(m,n)<<endl;
	}
	return 0;
}


