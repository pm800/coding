#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n, arr[1005];
vector<int> v;

int g(int x){
	int s = v.size();
	int l, h, m;
	l = 0; h = s-1;
	while( (h-l) >= 2){
		m = l + (h-l)/2;
		if(x > v[m])
			l = m;
		else if(x < v[m])
			h = m;
		else
			h = m;
	}
	if(s == 1){
		if(v[0] < x)
			return 0;
		return -1;
	}
	else{
		if(v[h] < x)
			return h;
		else if(v[l] < x)
			return l;
		else
			return -1;
	}
}

void f(){
    v = {};
    if(n == 0){
		cout<<0<<endl;
		return;
    }
    v.push_back(arr[0]);
    for(int i=1; i<n; i++){
		int x = arr[i];
		int k = g(x);
        int m = v.size();
		if(k==-1)
			v[0] = x;
		else if(k == m-1)
			v.push_back(x);
		else
			v[k+1] = x;
    }
    cout<<v.size()<<endl;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin >> n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		f();
	}
	return 0;
}


