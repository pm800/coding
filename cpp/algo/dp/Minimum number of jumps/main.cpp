#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

int n, arr[105];

int f(){
    int jmp = 0;
    if(n == 1 )
		return 0;
	if(arr[0] == 0)
		return -1;
	int i = 0;
	if(i+arr[i] >= n-1)
		return 1;
	int a = i+1, b = i+arr[i];
	while(1){
		int t = -1;
		for(int j = a; j<=b; j++){
			if(arr[j] == 0)
				continue;
			if((j + arr[j]) > t)
				t = j+ arr[j];
			if(t >= n-1){
				jmp += 2;
				return jmp;
			}
		}
		if(t <= b)
			return -1;
		a = b+1;
		b = t;
		jmp++;
	}
	return -1;
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		cout<<f()<<endl;
	}
	return 0;
}


