#include <iostream>
#include <vector>
#include <climits>
#include <string>
#include <stack>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n;
string s;
int dp[10005];

int f(){
	fill(dp, dp+n, -1);
	stack<int> stk;
	for(int i=0; i<n; i++){
		if(s[i] == '('){
				stk.push(i);
		}
		else{
			if(stk.empty())
				continue;
            int x= stk.top();
            stk.pop();
            dp[x] = i;
		}
	}
	int mxm = 0;
	int i= 0;
	while(i<n){
		if(dp[i] != -1){
			int c = 0;
			while(i<n && dp[i] != -1){
				c += (dp[i] - i + 1);
				i = dp[i] + 1;
			}
			if(c > mxm)
				mxm = c;
		}
        i++;
	}
	return mxm;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>s;
		n = s.size();
		cout<<f()<<endl;
	}
	return 0;
}


