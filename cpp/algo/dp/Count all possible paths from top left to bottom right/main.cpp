#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int m, n;
int mat[103][103];

int f(){
    for(int i=0; i<m; i++)
		mat[i][n-1] = 1;
	for(int j=0; j<n; j++)
		mat[m-1][j] = 1;
	for(int i=m-2; i>=0; i--){
		for(int j=n-2; j>=0; j--){
            mat[i][j] = (mat[i][j+1] + mat[i+1][j]) % 1000000007;
		}
	}
	return mat[0][0];
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>m>>n;
		cout<<f()<<endl;
	}
	return 0;
}


