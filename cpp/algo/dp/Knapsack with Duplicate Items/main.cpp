#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n, val[1005], wt[1005], w;
int dp[1005];

int f(){
	for(int i=0; i<w+1; i++)
		dp[i] = 0;
	for(int c = 1; c<n+1; c++){
		int x = wt[c];
		for(int r = 1; r<w+1; r++){
			if(r >= x){
				int wout, with;
				wout = max(dp[r], dp[r-1]);
				with = val[c] + dp[r-x];
				dp[r] = max(wout, with);
			}
		}
	}
	return dp[w];
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		cin>>w;
		for(int i=1; i<=n; i++)
			cin>>val[i];
		for(int i=1; i<=n; i++)
			cin>>wt[i];
		cout<<f()<<endl;
	}
	return 0;
}
















//#include <iostream>
//#include <vector>
//#include <climits>
//#include <stack>
//#include <string>
//#include <algorithm>
//#include <list>
//#include <unordered_map>
//#include <unordered_set>
//
//using namespace std;
//
//int n, val[1005], wt[1005], w;
//int mat[1005][1005];
//
//int f(){
//	for(int c = 1; c<n+1; c++){
//		int x = wt[c];
//		for(int r = 1; r<w+1; r++){
//			if(r<x){
//				mat[r][c] = mat[r][c-1];
//			}
//			else{
//				int wout, with;
//				wout = max(mat[r][c-1], mat[r-1][c]);
//				with = val[c] + mat[r-x][c];
//				mat[r][c] = max(  with, wout );
//			}
//		}
//	}
//	return mat[w][n];
//}
//
//
//int main(){
//	for(int i=0; i<1005; i++)
//		mat[i][0] = 0;
//	for(int i=0; i<1005; i++)
//		mat[0][i] = 0;
//	int tcs;
//	cin >> tcs;
//	while(tcs--){
//		cin>>n;
//		cin>>w;
//		for(int i=1; i<=n; i++)
//			cin>>val[i];
//		for(int i=1; i<=n; i++)
//			cin>>wt[i];
//		cout<<f()<<endl;
//	}
//	return 0;
//}
//
//
//
//
//
//
//
//
//
//
//
//
