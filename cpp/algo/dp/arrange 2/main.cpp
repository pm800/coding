#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl
#define pvv(v) for(int i=0; i<v.size(); i++){for(int j=0; j<v[0].size(); j++)cout<<v[i][j]<<" ";cout<<endl;}cout<<endl



using namespace std;

vector<vector<int>> dp;
int m, n;
string s;

int g(int i, int k){
	if(i >= n){
		return -1;
	}
	if(k == 0){
		if(dp[k][i] != -2){
			return dp[k][i];
		}
		int b = 0, w = 0;
		for(int j = i; j<n; j++){
			if(s[j] == 'B'){
				b++;
			}
			else{
				w++;
			}
		}
		return b*w;
	}
	if(i+k >= n || k < 0){
		return -1;
	}
	if(dp[k][i] != -2){
		return dp[k][i];
	}
	int b = 0, w = 0;
	int ans = INT_MAX;
	for(int j = i; j<n; j++){
		if(s[j] == 'B'){
			b++;
		}
		else{
			w++;
		}
		int c = b*w;
		int x = g(j+1, k-1);
        if(x == -1){
			break;
        }
        c += x;
        if(c < ans){
			ans = c;
        }
	}
	dp[k][i] = ans;
	return ans;
}

int f(string str, int k) {
	m = k;
	s = str;
	n = s.size();
	for(int i=0; i<m; i++){
		dp.push_back(vector<int>(n, -2));
	}
	return g(0, k-1);
}



int main(){
//	cout<<f("WWW", 2);
	cout<<f("BWBWWWWBWBBWBWBWBBWBBBWWWBWBWBWWWBWBWBWBBWBW", 19);
	return 0;
}


