#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

using namespace std;

struct S{
	int f, l, r;
};

int f(string s){
	unordered_map<char, S> m;
    for(int i = 0; i<s.size(); i++){
		char c = s[i];
		auto it = m.find(c);
		if(it != m.end()){
			int r = it->second.r;
			for(auto it2 = m.begin(); it2 != m.end(); it2++){
				if(it2->second.f >= 2 && it2->second.l < r){
					return 1;
				}
			}
			m[c].f++;
			m[c].r = i;
		}
		else{
			m[c] = {1, i, i};
		}
    }
    return 0;
}

int main(){
	cout<<f("abab");
	return 0;
}


