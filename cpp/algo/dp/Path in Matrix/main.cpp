#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n;
int mat[23][23];

int f(){
    for(int i=n-2; i>=0; i--){
		for(int j=0; j<n; j++){
			int mx = mat[i+1][j];
			if(j-1 >= 0)
				mx = max(mx, mat[i+1][j-1]);
			if(j+1 < n)
				mx = max(mx, mat[i+1][j+1]);
			mat[i][j] += mx;
		}
    }
    int mx = mat[0][0];
    for(int j=0; j<n; j++)
        if(mat[0][j] > mx)
			mx = mat[0][j];
	return mx;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			for(int j=0; j<n; j++)
				cin>>mat[i][j];
		cout<<f()<<endl;
	}
	return 0;
}


