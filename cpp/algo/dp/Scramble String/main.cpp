#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl


using namespace std;



////////////////////////////////////////////////////

string s1, s2;
vector<vector<vector<int>>> dp;

int f(int i, int j, int l){
    if(dp[i][j][l] != -1){
		return dp[i][j][l];
    }
    if(l == 1){
		if(s1[i] == s2[j]){
			dp[i][j][1] = 1;
		}
		else{
			dp[i][j][1] = 0;
		}
		return dp[i][j][1];
    }
    dp[i][j][l] = 0;
    for(int n=1; n<l; n++){
		dp[i][j][l] |= f(i, j, n) && f(i+n, j+n, l-n);
		dp[i][j][l] |= f(i, j+l-n, n) && f(i+n, j, l-n);
		if(dp[i][j][l] == 1){
			return 1;
		}
    }
    return 0;
}




int isScramble(const string s1, const string s2) {
	::s1 = s1;
	::s2 = s2;
    dp.clear();
    int n = s1.size();
    dp.resize(n+1, vector<vector<int>>(n+1, vector<int>(n+1, -1)));
    return f(0, 0, n);
}

///////////////////////////////////////////////////


int main(){
	cout<<isScramble("a","a")<<endl;
	return 0;
}




















