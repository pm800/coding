#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n, val[105], wt[105], w;
int mat[105][105];

int f(){
	for(int c = 1; c<n+1; c++){
		int x = wt[c-1];
		for(int r = 1; r<w+1; r++){
			if(r<x){
				mat[r][c] = mat[r][c-1];
			}
			else{
				mat[r][c] = max(  mat[r][c-1], val[c-1] + mat[r-x][c-1] );
			}
		}
	}
	return mat[w][n];
}


int main(){
	for(int i=0; i<105; i++)
		mat[i][0] = 0;
	for(int i=0; i<105; i++)
		mat[0][i] = 0;
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		cin>>w;
		for(int i=0; i<n; i++)
			cin>>val[i];
		for(int i=0; i<n; i++)
			cin>>wt[i];
		cout<<f()<<endl;
	}
	return 0;
}


















//#include <iostream>
//#include <vector>
//#include <climits>
//#include <stack>
//#include <string>
//#include <algorithm>
//#include <list>
//#include <unordered_map>
//#include <unordered_set>
//
//using namespace std;
//
//int n, val[105], wt[105];
//
//int f(int i, int w){
//	if(w == 0)
//		return 0;
//	if(w < 0)
//		return -1;
//	if(i < 0)
//		return 0;
//	int x =0, y=0;
//	int a, b;
//	a = f(i-1, w-wt[i]);
//	b = f(i-1, w);
//	if(a != -1)
//		x = a + val[i];
//	if(b != -1)
//		y = b;
//	if(a == -1 && b== -1)
//		return -1;
//	return max(x, y);
//}
//
//
//int main(){
//	int tcs;
//	cin >> tcs;
//	while(tcs--){
//		int w;
//		cin>>n;
//		cin>>w;
//		for(int i=0; i<n; i++)
//			cin>>val[i];
//		for(int i=0; i<n; i++)
//			cin>>wt[i];
//		cout<<f(n-1, w)<<endl;
//	}
//	return 0;
//}
//
//
