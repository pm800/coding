#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

string s1, s2;
int m, n;
int mat[103][1003];

int f(){
    if(n<m)
		return 0;
	for(int i=1; i<m+1; i++){
		for(int j=1; j<n+1; j++){
			mat[i][j] = mat[i][j-1];
			if(s1[j-1] == s2[i-1])
				mat[i][j] += mat[i-1][j-1];
		}
	}
	return mat[m][n];
}


int main(){
	for(int j=0; j<1003; j++)
		mat[0][j] = 1;
	for(int i=1; i<103; i++)
		mat[i][0] = 0;
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>s1>>s2;
        n = s1.size();
        m = s2.size();
        cout<<f()<<endl;
	}
	return 0;
}


