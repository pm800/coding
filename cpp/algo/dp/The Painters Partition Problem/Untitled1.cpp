
#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n;
int arr[55];
int mm;


int sm(int i, int j){
	int t = 0;
	while(i<=j){
		t += arr[i];
		i++;
	}
	return t;
}

int getmx(int i){
	int mx = INT_MIN;
    while(i<n){
		if(arr[i]>mx)
			mx = arr[i];
		i++;
    }
    return mx;
}

int f(int i, int k){
    if(i>=n)
		return 0;
	if(k >= (n-1)-i+1)
		return getmx(i);
	if(k == 1)
		return sm(i, n-1);
	for(int j=i+1; j<n; j++){
		int l=0, r=0;
		l = sm(i, j);
		r = f(j+1, k-1);
		int curr = max(l, r);
		mm = min(mm, curr);
	}
	return mm;
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		int k;
		mm = INT_MAX;
		cin>>k>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		cout<<f(0,k)<<endl;
	}
	return 0;
}


