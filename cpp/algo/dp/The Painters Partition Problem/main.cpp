#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n;
int arr[55];

int sm[55][55];
int getmx[55];
int dp[55][55];

int f(int i, int k){
    if(i>=n)
		return 0;
	if(k >= (n-1)-i+1)
		return getmx[i];
	if(k == 1)
		return sm[i][n-1];
	int minm = INT_MAX;
	for(int j=i; j<n; j++){
		int l=0, r=0;
		l = sm[i][j];
		if(dp[k-1][j+1] != -1){
			r = dp[k-1][j+1];
		}
		else
			r = f(j+1, k-1);
		int curr = max(l, r);
		minm = min(minm, curr);
	}
	dp[k][i] = minm;
	return dp[k][i];
}

int g(int k){
    for(int i=0; i<n; i++){
		sm[i][i] = arr[i];
		for(int j=i+1; j<n; j++){
			sm[i][j] = sm[i][j-1] + arr[j];
		}
    }

    int mx = INT_MIN;
    for(int i=n-1; i>=0; i--){
		mx = max(arr[i], mx);
		getmx[i] = mx;
    }

    for(int i=0; i<55; i++){
		for(int j=0; j<55; j++){
			dp[i][j] = -1;
		}
    }

	return f(0, k);
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		int k;
		cin>>k>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		cout<<g(k)<<endl;
	}
	return 0;
}


