#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n, val[100005], wt[100005], w;
int dp[100005];

int f(){
	fill(dp, dp+w+1, 0);
	for(int j = 0; j < n; j++){
		int wj = wt[j], vj = val[j];
		for(int i = w; i >= wj; i--){
            dp[i] = max(dp[i], vj + dp[i-wj]);
		}
	}
	return dp[w];
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		cin>>w;
		for(int i=0; i<n; i++)
			cin>>val[i];
		for(int i=0; i<n; i++)
			cin>>wt[i];
		cout<<f()<<endl;
	}
	return 0;
}

