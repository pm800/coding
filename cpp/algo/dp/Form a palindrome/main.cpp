#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

string s;
int mat[44][44];

int f(int i, int j){
    if(i >= j){
		mat[i][j] = 0;
		return 0;
    }
    if(s[i] == s[j]){
		if(mat[i+1][j-1] != -1)
			mat[i][j] = mat[i+1][j-1];
		else
			mat[i][j] = f(i+1, j-1);
    }
    else{
        int l, r;
        if(mat[i][j-1] != -1)
			l = 1 + mat[i][j-1];
		else
			l = 1 + f(i, j-1);
		if(mat[i+1][j] != -1)
			r = 1+ mat[i+1][j];
		else
			r = 1+f(i+1, j);
		mat[i][j] = min(l, r);
    }
    return mat[i][j];
}

int g(){
	int n = s.size();
	for(int i=0; i<n+3; i++)
		for(int j=0; j<n+3; j++)
			mat[i][j] = -1;
	int ans = f(0, n-1);
//	for(int i=0; i<n+3; i++){
//		for(int j=0; j<n+3; j++)
//			cout<<mat[i][j]<<" ";
//		cout<<endl;
//	}
	return ans;
}

int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>s;
		cout<<g()<<endl;
	}
	return 0;
}


