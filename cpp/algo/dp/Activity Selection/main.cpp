#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>

using namespace std;

int n, st[1003], ft[1003];

struct S{
	int s, f;
};

S arr[1003];

bool comp(S s1, S s2){
	return s1.f <= s2.f;
}

int g(){
    for(int i=0; i<n; i++)
		arr[i] = {st[i], ft[i]};
	sort(arr, arr+n, comp);
    int j= 0, cnt = 0;
    for(int i=0; i<n; i++){
		int s, f;
		s = arr[i].s;
		f = arr[i].f;
		if(s >= j){
			cnt++;
			j = f;
		}
    }
    return cnt;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin >>st[i];
		for(int i=0; i<n ;i++)
			cin>>ft[i];
		cout<<g()<<endl;
	}
	return 0;
}


