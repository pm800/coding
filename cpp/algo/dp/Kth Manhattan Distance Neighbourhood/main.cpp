#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset

#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl


using namespace std;

class Solution{
public:
	vector<vector<int> > solve(int A, vector<vector<int> > &B);
};

vector<vector<int> > Solution::solve(int k, vector<vector<int> > &ov) {
	vector<vector<int>> mat = ov;
	vector<vector<int>> tmat = ov;
	int m = mat.size(), n = mat[0].size();
	while(k--){
		for(int i=0; i<m; i++){
			for(int j=0; j<n; j++){
                int c = mat[i][j];
                if(i-1 >= 0){
					c = max(c, mat[i-1][j]);
                }
                if(i+1 < m){
					c = max(c, mat[i+1][j]);
                }
                if(j-1 >= 0){
					c = max(c, mat[i][j-1]);
                }
                if(j+1 < n){
					c = max(c, mat[i][j+1]);
                }
                tmat[i][j] = c;
			}
		}
		mat = tmat;
	}
	return mat;
}



int main(){

	return 0;
}


