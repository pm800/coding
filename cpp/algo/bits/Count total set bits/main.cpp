#include <iostream>
#include <cmath>
#define uli unsigned long int
using namespace std;

int main()
{
    int tcs;
    cin >> tcs;
    while(tcs--){
        int n, cnt = 0;
        cin >>n;

        bool flag = 0, a=1;
        int l = -1;
        for(int i=30; i>=0; i--){
            if(n& (1<<i)){
                if(flag==0){
                    l = i; flag = 1;
                }

            }
            else{
                if(flag == 1) a = false;
            }
        }
        if(n==0){ cout<<0; continue;  }
        if(a==1){ int b = l+1; cout<<b*pow(2, b-1)<<endl; continue; }
        int x = n - pow(2, l) + 1;
        int b = l;
        int sm = b*pow(2, b-1);
        for(int i=0; i<=l-1; i++){
            int s = x / (int)pow(2, i+1);
            sm += s*pow(2, i);
            int b = x % (int)pow(2, i+1);
            int eo = b - pow(2, i);
            if(eo > 0){
                sm += eo;
            }
        }
        sm += x;


        cout<<sm<<endl;
    }
    return 0;
}

//int main()
//{
//    int tcs;
//    cin >> tcs;
//    while(tcs--){
//        int n, cnt = 0;
//        cin >>n;
//        for(int i=1; i<=n; i++){
//            for(int j=0; j<10; j++){
//                if( (uli)i & (uli)1<<j )
//                    cnt++;
//            }
//        }
//        cout<<cnt<<endl;
//    }
//    return 0;
//}
