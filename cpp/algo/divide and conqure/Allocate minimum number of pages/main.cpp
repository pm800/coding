#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring>

using namespace std;

int n, arr[55];

int dp[55][55];

int f(int i, int k){
    if( (n-i) < k || k <= 0 || i >= n )
		return -1;
	if(dp[i][k] != -1)
		return dp[i][k];
	if(k == 1){
		int sm = 0;
        for(int j=i; j<n; j++)
			sm += arr[j];
		return sm;
	}
	int sm = 0, ans = INT_MAX;
    for(int j = i; j <n; j++){
		int x = f(j+1, k-1);
		if(x == -1)
			break;
		sm += arr[j];
		int y = max(sm, x);
		ans = min(ans, y);

    }
    dp[i][k] = ans;
    return ans;
}


int main(){
	int tcs;
	cin >> tcs;
	while(tcs--){
		cin>>n;
		for(int i=0; i<n; i++)
			cin>>arr[i];
		int k;
		cin>>k;
		memset(dp, -1, sizeof dp);
		cout<<f(0, k)<<endl;
	}
	return 0;
}


