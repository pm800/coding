#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset
#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define p5(x, y, a, b, c) cout << x << " "<<y<<  " "<<a<<" "<<b<< " "<<c<<endl
#define p6(x, y, a, b, c, d) cout << x << " "<<y<<  " "<<a<<" "<<b<< " "<<c<<" "<<d<<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl

using namespace std;

int n, k;
string gs;

int lpsf(){
    int lps[n];
    lps[0] = 0;
    int i=1, j = 0;
    while(i<n && j<n){
		if(gs[i] != gs[j]){
			if(j == 0){
				lps[i] = 0;
				i++;
			}
			else{
				j = lps[j-1];
			}
		}
		else{
			lps[i] = j+1;
			i++;
			j++;
		}
    }
    return lps[n-1];
}

void f(){
    int x = lpsf();
	cout<<gs;
    k--;
    while(k-- > 0){
		cout<<gs.substr(x);
    }
    cout<<endl;
    return;
}

int main(){
	cin>>n>>k;
	cin>>gs;
	f();
	return 0;
}


















