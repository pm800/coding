#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cstring> // memset
#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define p5(x, y, a, b, c) cout << x << " "<<y<<  " "<<a<<" "<<b<< " "<<c<<endl
#define p6(x, y, a, b, c, d) cout << x << " "<<y<<  " "<<a<<" "<<b<< " "<<c<<" "<<d<<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl
#define f first
#define s second
using namespace std;

int n;

int ff(pair<int,int> arr[]){
	pair<int,int> la[n];
	pair<int,int> ra[n];
	la[0] = arr[0];
	int a, b;
	for(int i=1; i<n; i++){
		a = max(arr[i].f, la[i-1].f);
		b = min(arr[i].s, la[i-1].s);
        la[i] = {a,b};
	}
	ra[n-1] = arr[n-1];
	a = ra[n-1].f;
	b = ra[n-1].s;
	for(int i=n-2; i>=0; i--){
		a = max(a, arr[i].f);
		b = min(b, arr[i].s);
        ra[i] = {a,b};
	}
	int mx = 0;
    if(n>1){
		a = la[n-2].f;
		b = la[n-2].s;
		if(mx < b-a){
			mx = b-a;
		}
		a = ra[1].f;
		b = ra[1].s;
		if(mx < b-a){
			mx = b-a;
		}
    }
	for(int i=1; i<n-1; i++){
		a = max(la[i-1].f, ra[i+1].f);
		b = min(la[i-1].s, ra[i+1].s);
		if(mx < b-a){
			mx = b-a;
		}
	}
	return mx;
}

int main(){
	cin>>n;
	pair<int,int> arr[n];
	int a,b;
	for(int i=0; i<n; i++){
		cin>>a>>b;
		arr[i] = {a,b};
	};
	cout<<ff(arr)<<endl;
	return 0;
}


















