#include <iostream>
#include <vector>
#include <climits>
#include <stack>
#include <queue> //priortity_queue
#include <string>
#include <algorithm>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <cmath>
#include <cstring> // memset
#define p2(x, y) cout << x << " "<<y<<endl
#define p4(x, y, a, b) cout << x << " "<<y<<  " "<<a<<" "<<b<< endl
#define p3(x, y, a) cout << x << " "<<y<<  " "<<a <<endl
#define p1(x) cout << x <<endl
#define p5(x, y, a, b, c) cout << x << " "<<y<<  " "<<a<<" "<<b<< " "<<c<<endl
#define p6(x, y, a, b, c, d) cout << x << " "<<y<<  " "<<a<<" "<<b<< " "<<c<<" "<<d<<endl
#define parr(arr, n)  for(int i=0; i<n; i++) cout<<arr[i]<<" "; cout<<endl
#define pv(v) for(int i=0; i<v.size(); i++) cout<<v[i]<<" "; cout<<endl

using namespace std;
#define ll long long int
int n, k;

int df(int x){
	int cnt = 1;
	x /= 10;
	while(x){
		x /= 10;
		cnt++;
	}
	return cnt;
}

ll f(int arr[]){
	map<int, map<int, int>> m;
	int x, d;
	int mod;
	for(int i=0; i<n; i++){
		x = arr[i];
		mod = x%k;
		d = df(x);
		m[d][mod]++;
	}
	ll r, t, dx, fr;
	ll ans = 0;
	for(ll i=0; i<n; i++){
		x = arr[i];
		dx = df(x);
		ll mul = 10;
		for(ll d=1; d<=10; d++){
			t = (x * (mul % k)) % k;
			mod = (k - t) % k;
			fr = 0;
			if(m.find(d) != m.end()){
				if(m[d].find(mod) != m[d].end()){
					fr = m[d][mod];
				}
			}
			if(dx == d){
				if(x%k == mod){
					fr--;
				}
			}
			ans += fr;
			mul *= 10;
		}
	}
	return ans;
}

int main(){
	cin>>n>>k;
	int arr[n];
	for(int i=0; i<n; i++){
		cin>>arr[i];
	}
	cout<<f(arr)<<endl;
	return 0;
}


















